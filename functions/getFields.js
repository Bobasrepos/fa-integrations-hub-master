const express = require('express');
const cors = require('cors');
const { findFieldsByEntity, getEntities } = require('./faApiFunctions');

const getFields = express();
// Automatically allow cross-origin requests
getFields.use(cors({origin: true}));

getFields.get('/', async (req, res) => {

    res.headers = {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST",
        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
        "Content-Type": "application/json"
    };

    let entities = await getEntities();

    let allFields = [];
    for (const entity of entities) {
        let fields = await findFieldsByEntity(entity.name);
        allFields = [...allFields, {[entity.name] : [...fields]}]
    }

    res.send(allFields);
});

module.exports = getFields;
