const functions = require('firebase-functions');

const getFields = require('./getFields');
const getEntityRecords = require('./getEntityRecords');
const createEntity = require('./createEntity');
const updateEntity = require('./updateEntity');
const createLine = require('./createLine');
const processForm = require('./processFormData');

exports.getFields = functions.https.onRequest(getFields);
exports.processForm = functions.https.onRequest(processForm);
exports.getEntityRecords = functions.https.onRequest(getEntityRecords);
exports.createEntity = functions.https.onRequest(createEntity);
exports.updateEntity = functions.https.onRequest(updateEntity);
exports.createLine = functions.https.onRequest(createLine);
