const express = require('express');
const cors = require('cors');
const { listCustomEntities } = require('./faApiFunctions');

const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "POST",
    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    "Content-Type": "application/json"
};

const getEntitiesApp = express();

// Automatically allow cross-origin requests
getEntitiesApp.use(cors({origin: true}));

getEntitiesApp.get('/', async (req, res) => {
    res.headers = headers;
    let { entity='app_test', trigger='updated', limit=10, searchf='app_test_field0', searchv='7b236193-d455-4e41-be61-bf2d46cdb173', getchanges=true } = req.query;
    res.send(await listCustomEntities(entity,trigger,limit, true, searchf, searchv, null,  getchanges));
});

getEntitiesApp.get('/contact', async (req, res) => {
    res.headers = headers;
    let { trigger='created',limit=10, searchf=null, searchv=null } = req.query;
    res.send(await listCustomEntities('contact',trigger,limit, true, searchf, searchv ));
});

getEntitiesApp.get('/logo', async (req, res) => {
    res.headers = headers;
    let { trigger='created',limit=10, searchf=null, searchv=null } = req.query;
    res.send(await listCustomEntities('logo',trigger,limit, true, searchf, searchv ));
});

getEntitiesApp.get('/deal', async (req, res) => {
    res.headers = headers;
    let { trigger='created',limit=10, searchf=null, searchv=null } = req.query;
    res.send(await listCustomEntities('deal',trigger,limit, true, searchf, searchv ));
});

getEntitiesApp.get('/suggested', async (req, res) => {
    res.headers = headers;
    let { trigger='created',limit=10, searchf=null, searchv=null } = req.query;
    res.send(await listCustomEntities('suggested_contact',trigger,limit, true, searchf, searchv ));
});

getEntitiesApp.get('/changes', async (req, res) => {
    res.headers = headers;
    let { entity='app_test', trigger='updated', limit=10, searchf='app_test_field0', searchv='7b236193-d455-4e41-be61-bf2d46cdb173', getchanges=true } = req.query;
    res.send(await listCustomEntities(entity,trigger,limit, false, searchf, searchv, null,  getchanges));
});

module.exports = getEntitiesApp;

