const axios = require('axios').default;

//var clientId = "789A5A64-84D8-488B-BABC-FD2937DB12F3";
//var clientSecret = "fa-secret-27CC723B1169620F4C5538";


var clientId = "259A5A64-84D8-488B-B567-8D2937DB1633";
var clientSecret = "fa-secret-71CC733B1169620F435289";


// functions to get records from FreeAgent

var uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
var idFieldsTypes = ['reference', 'manyReferences', 'reference_array', 'remoteReference', 'reference_joinCatalog', 'catalogArray', 'catalogArray', 'immutableReference'];


async function getToken(clientId, clientSecret) {
    const oAuthOptions = {
        method: 'POST',
        headers: {'content-type': 'application/json'},
        data: `{"grant_type":"client_credentials","client_id": "${clientId}","client_secret": "${clientSecret}"}`,
        url: 'https://freeagent.network/oauth/token'
    };
    return axios(oAuthOptions)
}

async function postQuery(queryStr, token) {
    const queryOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', "Authorization" : "Bearer " + token},
        data: JSON.stringify({query: queryStr}),
        url: 'https://freeagent.network/api/graphql'
    };
    try {
        return await axios(queryOptions);
    } catch (e) {
        return e;
    }
}

var token = null;
var serverDate = null;

async function fetchQuery(queryStr) {
    if (!token) {
        var res = await getToken(clientId, clientSecret);
        token = res.data.access_token;
        serverDate = res.headers.date;
    }
    var queryRes = await postQuery(queryStr, token);
    if (queryRes && queryRes.data && queryRes.data.data) {
        return queryRes.data.data;
    } else {
        return queryRes;
    }

}



String.prototype.format = function() {
    a = this;
    for (k in arguments) {
        a = a.replace("{" + k + "}", arguments[k])
    }
    return a
};

var faEntities = null;

var agentBodyMin = `id first_name last_name teamId email_address full_name`;
var entityBodyMin = `entity_id entity_name logo_name logo_id`;
var leadBodyMin = `id first_name last_name work_email full_name`;
var leadBody = `
id
first_name
last_name
full_name
current_position
buying_power
buying_center
buying_center_id
portrait_url
mobile_phone
home_phone
work_phone
personal_email
work_email
email_addresses
location_name
home_address
work_address
linkedIn
facebook
twitter
lead_status_id
lead_source_id
industry_id
logo_id
location_id
lead_score
lead_owner_id
owner_id
created_at
updated_at
custom_fields {
  field_name
  value
  values
  display_value
  display_values
}`;

var logoBodyMin = `id name`;
var logoBody = `
id
name
logo_src
hq_location
industry
industry_catalog_id
revenue
num_employees
website
summary
owner_id
business_phone
created_at
custom_fields {
  field_name
  value
  values
  display_value
  display_values
}`;
var dealBodyMin = `id name logo { id name }`;
var dealBody = `
id
__typename
name
status
status_id
closeDate: close_date
createDate: created_at
updateDate: updated_at
close_date: close_date
created_at: created_at
updated_at: updated_at
amount
dealState: deal_state
winProbability: win_probability
win_probability: win_probability
sales_stage_id
weighted_forecast
sales_cycle
type_id
dtype_id
lead_source_id
logo {
  id
  name
}
forecast_category_id
fiscal_period
stage_changed_at
owner_id
modified_id
last_activity
custom_fields {
  field_name
  value
  values
  display_value
  display_values
}`;
var customEntityBodyMin = `id seq_id`;
var customEntityBody = `
id
seq_id
custom_fields {
  field_name
  value
  values
  display_value
  display_values
}
created_at
updated_at
created_by {
  id
  full_name
  email_address
}
updated_by {
  id
  full_name
}`;
var entityValuesBody = `
count
values {
  id
  seq_id
  custom_fields {
    field_name
    value
    values
    display_value
    display_values
  }
  created_at
  updated_at
  last_activity
}
`;
var entityValuesChangesBody = `
      changes 
         { 
           id 
           seq_id 
           updated_at 
           created_at  
           is_create 
           field_values 
         }`;
var entityValuesBodyMin = `values { id seq_id }`;
var getFieldItemsBody = `id  name`;
var getEntitiesBodyMin = `id name is_custom parent_id entity_id`;
var getFieldsBodyMin = `id is_custom main_type is_calculated name entity name_label is_required render_type`;


var entities = null;

async function getEntities(config={}) {
    if (!entities) {
        var isCustom = config && config.is_custom ? config.is_custom : false;
        const graphqlQuery =  `query { getEntities(show_hidden: false, only_referenceable: false, is_custom: ${isCustom}, include_non_primary: false) { ${getEntitiesBodyMin}  } }`;
        var res = await fetchQuery(graphqlQuery);
        entities = res.getEntities;
        return entities;
    } else {
        return entities;
    }
}

async function getEntityRecord (entityId, searchFieldName, searchFieldValue, isCustom=null, config=null) {
    let systemEntities = ['contact','lead','logo','account','deal'];
    if (!isCustom) {
        isCustom = !systemEntities.includes(entityId);
    }
    let graphqlQuery = '';
    if (!isCustom) {
        if(entityId === 'contact' || entityId === 'lead') graphqlQuery = getLeadsQuery(1, searchFieldName, searchFieldValue);
        if(entityId === 'logo' || entityId === 'account') graphqlQuery = getAccountsFilteredQuery(1,searchFieldName, searchFieldValue);
        if(entityId === 'deal') graphqlQuery = getDealsFilteredQuery(1,searchFieldName, searchFieldValue);
    } else {
        graphqlQuery = getEntityValuesFilteredQuery(entityId, searchFieldName, searchFieldValue);
    }
    const response = await fetchQuery(graphqlQuery);

    const responseProperty = getEntityResponseProperty(isCustom ? 'custom' : entityId);
    let record = [];
    let r = response;
    if ( r  && r[responseProperty.name] && r[responseProperty.name][responseProperty.plural].length > 0) {
        record = r[responseProperty.name][responseProperty.plural]
    }
    if (record[0] && config && config.formatResponseData) {
        var originalItems = await findFieldsByEntity(entityId,{ getItems: false, onlyCustoms: isCustom});
        return formatResponse(record[0], originalItems)
    } else {
        return record
    }
}

const getEquivalentType = (renderType) => {
    let type;
    let fieldType;
    switch(renderType) {
        case 'text':
            type = "text";
            fieldType = "input";
            break;
        case 'number':
            type = "number";
            fieldType = "input";
            break;
        case 'percent':
            type = "number";
            fieldType = "input";
            break;
        case 'currency':
            type = "number";
            fieldType = "input";
            break;
        case 'datetime':
            type = 'date';
            fieldType = "input";
            break;
        case 'email':
            type = "text";
            fieldType = "input";
            break;
        case 'reference':
            type = "text";
            fieldType = "select";
            break;
        case 'reference_array':
            type = "text";
            fieldType = "checkboxes";
            break;
        case 'immutableReference':
            type = "text";
            fieldType = "select";
            break;
        case 'buyingpower':
            type = "number";
            fieldType = "input";
            break;
        default:
            type = "text";
            fieldType = "input";
            break;
    }
    return {type, fieldType};
};

const parseResponse = (data, config = {  }) => {
    return data.map(({ is_required, name, render_type, name_label, entity, choices = null }) => {
        let translation = name_label;
        if (translation) {
            translation = translation.includes('.') && translation.split('.')[1].includes('_') ? translation.split('.')[1].split('_').join(' ') : translation;
            translation = translation.includes('.') && translation.split('.')[1].indexOf('_') === -1 ? translation.split('.')[1] : translation;
            translation = translation.split(' ').map(word => word.replace(/^\w/, c => c.toUpperCase())).join(' ');
        }
        const {type, fieldType} = getEquivalentType(render_type);
        let isRequire = config.fieldsNotRequired ? false : is_required;
        const fieldZappier = { key: name, required: isRequire, type, fieldType, label: translation, entity };
        if (choices) fieldZappier.choices = choices;
        return fieldZappier;
    });
};


async function findFieldsByEntity (entityId, config = {getItems: true}) {
    let fieldsRequired = config.fieldsNotRequired ? { fieldsNotRequired: true } : {};
    if (entityId) {
        let entities = config.entities;
        if (!entities) entities = await getEntities();
        const entitySelected = entities.find(entity => entity.name === entityId);
        if (entitySelected) {
            const graphqlQuery = `query {
                getFields(entity: "${entityId}") {
                    ${ getFieldsBodyMin }
                }
            }`;

            const response = await fetchQuery(graphqlQuery);
            let fields = response.getFields;
            fields = fields.filter(field => field.main_type !== "reference_join");
            fields = fields.filter(field => field.main_type !== "ago");
            fields = fields.filter(field => !field.is_calculated);
            if ((config && config.onlyCustoms) || entitySelected.is_custom) {
                fields = fields.filter(field => field.is_custom);
            }
            if (config.getItems) {
                const promises = fields.map(async (field) => {
                    if (field.render_type === "reference" || field.render_type === "immutableReference") {
                        const fieldItems = await getFieldItems(field.id);
                        if (fieldItems) {
                            return Object.assign({}, field, { choices: fieldItems });
                        }
                    }
                    return field;
                });
                return Promise.all(promises)
                    .then((fieldsToIterate) =>  {
                        fieldsToIterate = parseResponse(fieldsToIterate, fieldsRequired);
                        return fieldsToIterate;
                    });
            } else {
                return fields;
            }
        }
    } else {
        return [];
    }
}


async function createCustomEntity (inputData, entityId,  config= {formatResponseData: false}) {
    var onlyCustoms = false;
    var entityName = entityId || 'contact';
    var contactNames = ["contact", "lead"];
    var logoNames = ["logo", "account"];
    var graphqlQuery;
    if (entityName) {
        if (!entities) {
            entities = await getEntities({});
        }
        var responseProperty;
        var entitySelected = entities.find(entity => entity.name === entityName);
        onlyCustoms = entitySelected ? entitySelected.is_custom : false;
        var originalItems = await findFieldsByEntity(entityName,{ getItems: false, onlyCustoms: onlyCustoms, entities: entities });
        var params = formatCustomFields(inputData, originalItems);
        if (!entitySelected.is_custom) {
            params = stringifyForGraphQLParams(params);
            if (contactNames.indexOf(entityName) !== -1) {
                graphqlQuery = `mutation { addLead(${params}) {  lead {  ${leadBody}  }  }  }`;
                responseProperty = "addLead";
            } else if (logoNames.indexOf(entityName) !== -1) {
                graphqlQuery = `mutation { addLogo(${params}) { ${logoBody} } }`;
                responseProperty = "addLogo";
            } else {
                graphqlQuery = `mutation { addDeal(${params}) { ${dealBody} } }`;
                responseProperty = "addDeal";
            }
        } else {
            graphqlQuery = `mutation {  addEntityValue(entity: "${entityName}", custom_fields: ${JSON.stringify(params.custom_fields)}) { ${customEntityBody} } }`;
            responseProperty = "addEntityValue";
        }
        var res = await fetchQuery(graphqlQuery)
        var r = res[responseProperty];
        if (responseProperty === "addLead" && r.lead) {
            r = r.lead;
        }
        if (config && config.formatResponseData) {
            return formatResponse(r, originalItems)
        } else {
            return r
        }
    }
}


async function updateEntity(inputData,entityName,searchFieldName=null,searchFieldValue=null, recordId=null, config=null) {
    var onlyCustoms = false;
    if (entityName) {
        if (!entities) {
            entities = await getEntities();
        }
        var selectedEntity = entities.find(entity => entity.name === entityName);
        if (selectedEntity.is_custom) onlyCustoms = selectedEntity.is_custom;
        var originalItems = await findFieldsByEntity(entityName,{ getItems: false, onlyCustoms: onlyCustoms, entityName:entityName, entities: entities });
        var params = formatForUpdateFields(inputData, originalItems, onlyCustoms);
        if (!recordId) {
            var record = await getEntityRecord(entityName, searchFieldName, searchFieldValue, onlyCustoms);
            if (record[0] && record[0].id) {
                recordId = record[0].id;
            }
        }
        if (recordId) {
            var entityNameMapping = onlyCustoms ? 'custom' : entityName;
            var queryProperties = createUpdateQuery(entityName, entityNameMapping, recordId, params);
            var response = await fetchQuery(queryProperties.graphqlQuery);
            var r = response[queryProperties.responseProperty];
            if (config && config.formatResponseData) {
                return formatResponse(r, originalItems)
            } else {
                return r
            }
        } else {
            return null;
        }
    } else {
        return null;
    }
}

async function getEntityChanges(entity, id, fromDate) {
    var query = getEntityValuesChangesQuery(entity, id, fromDate);
    var response = await fetchQuery(query);
    console.log(query, response)
    return { query, response: response.getChangesOnEntity.changes }
}

async function listCustomEntities (entityName='app_test',
                                   trigger='created',
                                   limit=10,
                                   formatResponseData= false,
                                   searchF = null,
                                   searchV = null,
                                   parentId=null,
                                   getChanges = false
) {
    var onlyCustoms = false;

    var sendQueriesAndRes = {};

    if (entityName) {
        if (!entities) {
            entities = await getEntities();
        }
        var selectedEntity = entities.find(entity => entity.name === entityName);
        var query = '';
        var responseProperty = '';
        if (selectedEntity && selectedEntity.is_custom) {
            onlyCustoms = selectedEntity.is_custom;
        }
        var originalItems = await findFieldsByEntity(entityName,{getItems: false, onlyCustoms });
        var entityNameMapping = entityName;
        if (onlyCustoms) {
            entityNameMapping = 'custom';
        }
        if (entityNameMapping === 'custom') {
            query = `query { getEntityValuesWithCount(entity: "${entityName}",limit: ${limit},filters: [{field_name:"${searchF}",values:["${searchV}"]}],order: [["${trigger}_at", "DESC"]], parent_entity_reference_id:${parentId}) { ${entityValuesBody} } }`
            responseProperty = 'getEntityValuesWithCount';
        } else {
            if (entityName === 'contact' || entityName === 'lead') {
                query = `query { getLeadsWithCount(limit: ${limit},filters: [{field_name:"${searchF}",values:["${searchV}"]}], order: [["${trigger}_at", "DESC"]]){ leads { ${leadBody} } } }`;
                responseProperty = 'getLeadsWithCount';
            }
            if (entityName === 'logo' || entityName === 'account') {
                query = `query { logosWithCount(limit: "${limit}",filters: [{field_name:"${searchF}",values:["${searchV}"]}], order: [["${trigger}_at", "DESC"]]) { logos { ${logoBodyMin} } } }`;
                responseProperty = 'logosWithCount';
            }
            if (entityName === 'deal') {
                query = `query { getDealsWithCount(limit: ${limit},filters: [{field_name:"${searchF}",values:["${searchV}"]}], order: [["${trigger}_at", "DESC"]]){ deals { ${dealBody} } } }`;
                responseProperty = 'getDealsWithCount';
            }
        }

        sendQueriesAndRes.getRecorsQuery = query;

        console.log(query);

        var response = await fetchQuery(query);

        var entitiesList = response[responseProperty];

        console.log(entitiesList);

        entitiesList = getEntitiesData (responseProperty, entitiesList)

       /* if (trigger == 'updated' && entitiesList && entitiesList.length > 0) {
            entitiesList = entitiesList.filter(entity => entity.created_at != entity.updated_at);
        }
        */

        console.log(entitiesList);

        var formattedEntities = [];

        if (getChanges) {
            var tempList = [];
            var changesQueries = [];
            var fromDate = Date.now() - 31556952000;
            fromDate = new Date(fromDate);
            fromDate = fromDate.toISOString();
            for (var entityObj of entitiesList) {
                var changes = await getEntityChanges(entityName, entityObj.id, fromDate);
                changesQueries.push(changes.query);
                if (changes.response.length > 0) {
                    var changesFiltered = changes.response.map(c => {
                        if (c.field_values[searchF]) {
                           return {[searchF]: c.field_values[searchF]}
                        }
                    });
                    if (changesFiltered.length > 0) {
                        entityObj = {...entityObj, changes: changesFiltered };
                        tempList.push(entityObj);
                    }
                }
            }

            if (changesQueries.length > 0) {
                sendQueriesAndRes.changesQueries = changesQueries;
            }

            console.log(serverDate);
            //console.log('tempList', tempList);
            entitiesList = tempList;
        }


        if (formatResponseData) {
            for (var entity of entitiesList) {
                formattedEntities.push(formatResponse(entity, originalItems));
            }
            return formattedEntities;
        } else {
            return entitiesList;
        }
    } else {
        return {Response: 'we could\'t find a record to update'}
    }
}

let formatChildFields = (childrenFields) => {
    let childrenFieldsStr = '[';
    childrenFields.map(c => {
        let comma = childrenFieldsStr === '[' ? '' : ',';
        return childrenFieldsStr = `${childrenFieldsStr}${comma}["${c.field}","${c.value}"]`
    });
    return `custom_fields:${childrenFieldsStr}]`
}


let formatChildren = (children) => {
    let childrenStr = '[';
    children.map(child => {
        let comma = childrenStr === '[' ? '' : ',';
        let { entity, fields } = child;
        return childrenStr = `${childrenStr}${comma}{${formatChildFields(fields)}, entity_id: "${entity}"}`
    });
    return `children:${childrenStr}]`
};


const createLine = async (parentEntityId, parentEntity,  parentFields, children, parentInstanceId) => {
    let results = [];
    if (!entities) {
        entities = await getEntities();
    }
    if (!parentEntityId && parentEntity) {
        var parentEntityObj = entities.find(e => e.name === parentEntity);
        if (parentEntityObj) {
            parentEntityId = parentEntityObj.parent_id;
        }
    }
    var originalItems = await findFieldsByEntity(parentEntity);
    let customFields = await formatCustomFields({...parentFields} ,originalItems, true );
    customFields = stringifyForGraphQLParams(customFields);
    let parentFieldsParsed = customFields.replace('custom_fields:', 'parent_fields:');
    const query = `mutation { saveCompositeEntity(parent_entity_id: "${parentEntityId}", ${formatChildren(children)} , instance_id: "${parentInstanceId}", ${parentFieldsParsed} ) {  parent_id } }`;
    const res = await fetchQuery(query);
    return [...results, {[parentEntity] : res.saveCompositeEntity.parent_id }];
};



function getLeadsQuery(limit, name, value) {
    return `query {  getLeadsWithCount(limit: ${limit},  filters: [{field_name:"${name}",values:["${value}"]}]) { leads { ${leadBodyMin}  }  }  }`;
}

function getAccountsFilteredQuery(limit, name, value) {
    return `query {  logosWithCount(limit: "${limit}", filters: [{field_name:"${name}",values:["${value}"]}]) { logos { ${logoBodyMin}  }  }   }`;
}

function getDealsFilteredQuery(limit, name, value) {
    return `query {  getDealsWithCount(limit: ${limit}, filters: [{field_name:"${name}",values:["${value}"]}]) {  deals { ${dealBodyMin} }  }   }`;
}

function getEntityValuesFilteredQuery(entity, name, value){
    return `query {  getEntityValuesWithCount(entity: "${entity}", limit: 1, filters: [{field_name:"${name}",values:["${value}"]}]) {  ${entityValuesBody}   }  }`;
}

function getEntityValuesChangesQuery(entity,id, fromDate){
    return `query {  getChangesOnEntity(entity: "${entity}", id: "${id}", from_date: "${fromDate}") {  ${entityValuesChangesBody}   }  }`;
}

function updateEntityFieldsQuery(recordId, params) {
    return `mutation {  updateEntityValueFields(recordId: "${recordId}",  values: [${params}]) {  ${customEntityBodyMin}  }   }`;
}

function updateLeadFieldsQuery(recordId, params) {
    return `mutation {  updateLeadFields(recordId: "${recordId}",  values: [${params}]) {  ${leadBodyMin}  }   }`;
}

function updateLogoFieldsQuery(recordId, params) {
    return `mutation {  updateLogoFields(recordId: "${recordId}",  values: [${params}]) {  ${logoBodyMin}  }   }`;
}

function updateDealFieldsQuery(recordId, params) {
    return `mutation {  updateDealFields(recordId: "${recordId}",  values: [${params}]) {  ${dealBodyMin}  }   }`;
}

function createUpdateQuery(entityId, entity, recordId, params) {
    var map = {
        'custom': { graphqlQuery: updateEntityFieldsQuery(recordId,params), responseProperty: 'updateEntityValueFields' },
        'contact': { graphqlQuery: updateLeadFieldsQuery(recordId, params), responseProperty: 'updateLeadFields' },
        'lead': { graphqlQuery: updateLeadFieldsQuery(recordId, params), responseProperty: 'updateLeadFields' },
        'logo': { graphqlQuery: updateLogoFieldsQuery(recordId, params), responseProperty: 'updateLogoFields' },
        'account': { graphqlQuery: updateLogoFieldsQuery(recordId, params), responseProperty: 'updateLogoFields' },
        'deal': { graphqlQuery: updateDealFieldsQuery(recordId, params), responseProperty: 'updateDealFields' }
    };
    return map[entity];
}



function checkRenderType(bundleInput, originalItems) {
    let dataCopy = { ...bundleInput };
    let inputKeys = Object.keys(dataCopy);
    inputKeys.forEach((key) => {
        let field = originalItems.find(item => item.name == key);
        if (field && field.render_type) {
            let typeObj = getEquivalentType(field.render_type);
            if (typeObj.type) {
                if (typeObj.type === "number") {
                    dataCopy[key] = parseInt(dataCopy[key], 10);
                }
                if (typeObj.type === "text") {
                    dataCopy[key] = dataCopy[key].toString();
                }
            }
        }
    });
    return dataCopy;
}


function getJson(value) {
    return JSON.stringify(JSON.stringify(value)).slice(1, -1)
}

function stringifyForGraphQLParams(data, isCustom= false) {
    if (!isCustom) {
        let json = JSON.stringify(data);
        json = json.replace(/"([^(")"]+)":/g,"$1:");
        json = json.substring(1, json.length - 1);
        return json;
    } else {
        let params = '';
        data.map(p => {
            let comma = params !== '' ? ',' : '';
            params = `${params}${comma}[${getJson(p[0])},${getJson(p[1])}]`;
        });
        return params;
    }
}

function formatCustomFields (data , originalItems, onlyCustoms = false, onlyStrings=false) {
    let customFields = [];
    let inputData =  !onlyStrings ? checkRenderType(data, originalItems) : data;
    let keys = Object.keys(inputData);
    let params = {};
    for (const key of keys) {
        const fieldItem = originalItems.find(originalItem => originalItem.name === key);
        if(fieldItem) {
            let value = inputData[key];
            if (fieldItem.is_custom || onlyCustoms) {
                customFields.push([key,value]);
                delete params[key];
            } else {
                params[key] = value;
            }
        }
    }
    if(onlyCustoms || customFields.length > 0) {
        params.custom_fields = [...customFields];
    }
    return params;
}



function getEntityResponseProperty (entityName) {
    const map = {
        'contact': {name: 'getLeadsWithCount', plural: 'leads'},
        'lead':  {name: 'getLeadsWithCount', plural: 'leads'},
        'logo':  {name: 'logosWithCount', plural: 'logos'},
        'account':  {name: 'logosWithCount', plural: 'logos'},
        'deal': {name: 'getDealsWithCount', plural: 'deals'},
        'custom': {name: 'getEntityValuesWithCount', plural: 'values'}
    };
    return map[entityName];
}



function getEntitiesData (responseProperty, entitiesList) {
    if (responseProperty == 'getLeadsWithCount') {
        entitiesList = entitiesList.leads;
    }
    if (responseProperty == 'logosWithCount') {
        entitiesList = entitiesList.logos;
    }
    if (responseProperty == 'getDealsWithCount') {
        entitiesList = entitiesList.deals;
    }
    if (responseProperty == 'getEntityValuesWithCount') {
        entitiesList = entitiesList.values;
    }
    return entitiesList;
}


async function getFieldItems(id) {
    var graphqlQuery = `query {
    getFieldItems(fa_field_config_id: "${id}") {
      ${ getFieldItemsBody }
    }
  }`;
    var response = await fetchQuery(graphqlQuery);
    var fieldItems = response.getFieldItems;
    fieldItems = fieldItems.map(field => {
        return { value: field.id, label: field.name };
    });
    return fieldItems;
}



async function mapIdFromTextChoice(fieldId, valueToMap) {
    let fieldItems = await getFieldItems(fieldId);
    if (fieldItems && fieldItems.length > 0) {
        let searchedChoice = fieldItems.find(choice => choice.label === valueToMap);
        return searchedChoice && searchedChoice.value ? searchedChoice.value : valueToMap;
    } else {
        return valueToMap;
    }
}

function formatForUpdateFields(data , originalItems, onlyCustoms = false) {
    var customFields = '';
    var inputData = checkRenderType(data, originalItems);
    var keys = Object.keys(inputData);
    var paramsString = '';
    for (const key of keys) {
        const fieldItem = originalItems.find(originalItem => originalItem.name === key);
        if(fieldItem) {
            let value = inputData[key];
            if (idFieldsTypes.includes(fieldItem.main_type) && !value.match(uuidRegex)) {
                value = mapIdFromTextChoice(fieldItem.id, value);
            }
            if (fieldItem.is_custom || onlyCustoms) {
                var customComma = ',';
                if (customFields === '') {
                    customComma = '';
                }
                customFields = `${customFields}${customComma}[${getJson(key)},${getJson(value)}]`;
            } else {
                var newFiled = `{ 
          field_name: "${key}" 
          value: "${value}"
        }`;
                var comma = ',';
                if (paramsString === '') {
                    comma = '';
                }
                paramsString = `${paramsString}${comma}${newFiled}`;
            }
        }
    }
    if(onlyCustoms) {
        paramsString = `{
      field_name: "custom_fields"
      value: "[${customFields}]"
    }`;
    } else {
        if (customFields !== '') {
            let customParams = `{
        field_name: "custom_fields"
        value: "[${customFields}]"
      }`;
            let comma = paramsString === '' ? '' : ',';
            paramsString = `${paramsString}${comma}${customParams}`;
        }
    }
    return paramsString;
}


function formatResponse(data, originalItems = null) {
    if (data) {
        let r = { ...data };
        let keys = Object.keys(r);
        for (const key of keys) {
            if (key === 'custom_fields' && r[key] && r[key].length > 0) {
                for (const f of r.custom_fields) {
                    if (f.field_name) {
                        if (f.value && f.display_value && f.value !== f.display_value) {
                            r[f.field_name] = {value: f.value, label: f.display_value};
                        } else {
                            if (originalItems) {
                                let field = originalItems.find(originalItem => originalItem.name == f.field_name);
                                if (field) {
                                    if (f.value) {
                                        if (f.value.match(uuidRegex)) {
                                            let fieldItems = getFieldItems(field.id);
                                            if (fieldItems && fieldItems.length > 0) {
                                                r[f.field_name] = fieldItems.find(choice => choice.value === f.value);
                                                if (f.field_name !== key) {
                                                    delete r[key];
                                                }
                                            }
                                        } else {
                                            r[f.field_name] = f.display_value || f.value;
                                        }
                                    }
                                } else {
                                    r[f.field_name] = f.display_value || f.value;
                                }
                            } else {
                                r[f.field_name] = f.display_value || f.value;
                            }
                        }
                    }
                }
            } else {
                if (originalItems) {
                    let field = originalItems.find(originalItem => originalItem.name == key);
                    let value = r[key];
                    if (field) {
                        if (value) {
                            if (idFieldsTypes.includes(field.main_type) && value.match(uuidRegex)) {
                                let fieldItems = getFieldItems(field.id);
                                if (fieldItems && fieldItems.length > 0) {
                                    value = fieldItems.find(choice => choice.value === value);
                                    r[key] = value;
                                }
                            } else {
                                r[key] = value;
                            }
                        }
                    }
                }
            }
        }
        return r;
    } else {
        return {Data: 'There is no data available'};
    }
}


module.exports = {
    createCustomEntity,
    listCustomEntities,
    updateEntity,
    getEntityRecord,
    findFieldsByEntity,
    fetchQuery,
    getEntities,
    getEntityChanges,
    createLine
};






