const express = require('express');
const cors = require('cors');
let bodyParser = require('body-parser');
const {parse} = require('querystring');
const { createLine } = require('./faApiFunctions');

const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "POST",
    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    "Content-Type": "application/json"
};

const createLineApp = express();

createLineApp.use(bodyParser.json());
// Automatically allow cross-origin requests
createLineApp.use(cors({origin: true}));

createLineApp.use(express.urlencoded({ extended: true }));
// Parse JSON bodies (as sent by API clients)
createLineApp.use(express.json());

createLineApp.post('/', async (req, res) => {
    res.headers = headers;
    let bodyData = typeof req.body === "object" ? req.body : parse(req.body);
    // let children = [{ custom_field1: 'Value Text 1', custom_field2: 'Value Text 2', entity: "custom_name"}]
    let {parentEntityId, parentEntity,  parentFields, children, parentInstanceId } = bodyData;
    let response =  await createLine(parentEntityId, parentEntity,  parentFields, children, parentInstanceId);
    res.send(response);
});


module.exports = createLineApp;

