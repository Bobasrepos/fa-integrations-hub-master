const express = require('express');
const cors = require('cors');
let bodyParser = require('body-parser');
const {parse} = require('querystring');

const { getEntities, createCustomEntity, listCustomEntities , updateEntity, createLine } = require('./faApiFunctions');

const processForm = express();

processForm.use(bodyParser.json());
// Automatically allow cross-origin requests
processForm.use(cors({origin: true}));

processForm.use(express.urlencoded({ extended: true }));

// Parse JSON bodies (as sent by API clients)
processForm.use(express.json());

processForm.post('/', async (req, res) => {

    res.headers = {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST",
        "Access-Control-Allow-Headers": "*",
        "Content-Type": "application/json"
    };

    let data = typeof req.body === "object" ? req.body : parse(req.body);

    let { relations, search, parent, ...entitiesData } = data;


    let entities = Object.keys(entitiesData);

    let createdEntities = {searches: []};
    if (search && search.length > 0) {
        for (const searchInfo of search) {
            let { key, value, entity } = searchInfo;
            let searchResult = await listCustomEntities(entity, 'created', 1, true,key, value);
            if (searchResult && searchResult.length > 0) {
                if (!createdEntities[entity]) {
                    createdEntities[entity] = searchResult[0];
                }
                createdEntities.searches = [...createdEntities.searches, {[entity]: searchResult}];
            }
        }
    }

    if (parent) {
        let entities = await getEntities();

        let inputData = [];

        for (const [parentEntity, children] of Object.entries(parent)) {
            let parentIdObj = entities.find(e => e.name === parentEntity);
            let parentId = parentIdObj ? parentIdObj.entity_id : null;
            let parentFields = data[parentEntity] ? data[parentEntity] : [];
            createdEntities[parentEntity] = {parentFields}
            if (parentId) {
                let childrenArr = [];
                for (const [childEntity, childFields] of Object.entries(children.children)) {
                    let childIdObj = entities.find(e => e.name === childEntity);
                    let childId = childIdObj ? childIdObj.entity_id : null;
                    childrenArr = [...childrenArr, {custom_fields: childFields, entity_id: childId, entity: childEntity}];
                    createdEntities[parentEntity][childEntity] = childFields;
                }
                inputData = [...inputData, {parentEntityId: parentId, children: childrenArr, instanceId: "", parentFields, parentEntity }];
            }
        }

        let results = await createLine(inputData, entities);

        results.map(r => {
            let entityName = Object.keys(r)[0];
            if (createdEntities[entityName]) {
                createdEntities[entityName] = {...createdEntities[entityName], id: r[entityName]};
            } else {
                createdEntities[entityName] = {id: r[entityName]};
            }
        });
    }

    let generateRelation = async (entityFromPrevious ,firstEntityData , firstEntityId,  addIdToEntityId) => {
        if (!entityFromPrevious) {
            let dataCopy = {...data[addIdToEntityId]};
            dataCopy[`${firstEntityId}_id`] = firstEntityData.id;
            let secondEntityReturned = await createCustomEntity({...dataCopy}, addIdToEntityId);
            createdEntities[addIdToEntityId] = {...secondEntityReturned};
        } else {
            let updateResponse = await updateEntity({[`${firstEntityId}_id`]:firstEntityData.id},addIdToEntityId, entityFromPrevious.id);
            createdEntities[addIdToEntityId] = {...updateResponse};
        }
    };

    for (const entityId of entities) {
        let inputData = {...data[entityId],entityId};
        if (relations && relations.length > 0) {
            let relationData = relations.filter(relation => {
                if (relation[0].entity === entityId || relation[1].entity === entityId) {
                    return relation;
                }
            });
            if (relationData && relationData.length > 0) {
                for (const relation of relationData) {
                    let firstElEntity = relation[0].entity;
                    let secondElEntity = relation[1].entity;
                    let firstElKey = relation[0].key;
                    let secondElKey = relation[1].key;

                    if ((firstElEntity === entityId && secondElKey === `${entityId}_id`) ||
                        (secondElEntity === entityId && firstElKey === `${entityId}_id`)
                    ) {
                        let firstEntityCreatedId = firstElEntity === entityId && secondElKey === `${entityId}_id` ? firstElEntity : secondElEntity;
                        let addIdToEntityId = firstEntityCreatedId === firstElEntity ?  secondElEntity : firstElEntity;
                        let firstEntityData = {};
                        if (!createdEntities[firstEntityCreatedId]) {
                            firstEntityData = await createCustomEntity( {...data[firstEntityCreatedId]}, firstEntityCreatedId, {formatResponseData: true});
                            createdEntities[entityId] = {...firstEntityData};
                        } else {
                            firstEntityData = createdEntities[firstEntityCreatedId];
                        }
                        await generateRelation(createdEntities[addIdToEntityId],firstEntityData , firstEntityCreatedId ,  addIdToEntityId);
                    }
                }
            } else {
                let dataReturned =  await createCustomEntity(inputData, entityId);
                createdEntities[entityId] = {...dataReturned};
            }
        } else {
            if (!createdEntities[entityId]) {
                let dataReturned =  await createCustomEntity(inputData, entityId);
                createdEntities[entityId] = {...dataReturned};
            }
        }
    }

    res.send(createdEntities);
});

module.exports = processForm;

