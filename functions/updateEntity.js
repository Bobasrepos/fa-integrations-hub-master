const express = require('express');
const cors = require('cors');
let bodyParser = require('body-parser');
const {parse} = require('querystring');
const { updateEntity } = require('./faApiFunctions');

const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "POST",
    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    "Content-Type": "application/json"
};

const updateEntityApp = express();

updateEntityApp.use(bodyParser.json());
// Automatically allow cross-origin requests
updateEntityApp.use(cors({origin: true}));

updateEntityApp.use(express.urlencoded({ extended: true }));
// Parse JSON bodies (as sent by API clients)
updateEntityApp.use(express.json());

updateEntityApp.post('/contact', async (req, res) => {
    res.headers = headers;
    let bodyData = typeof req.body === "object" ? req.body : parse(req.body);
    let { searchf, searchv, recordid, ...inputData } = bodyData;
    let response =  await updateEntity(inputData,'contact', searchf,searchv,recordid);
    res.send(response);
});

updateEntityApp.post('/logo', async (req, res) => {
    res.headers = headers;
    let bodyData = typeof req.body === "object" ? req.body : parse(req.body);
    let { searchf, searchv, recordid, ...inputData } = bodyData;
    let response =  await updateEntity(inputData,'logo', searchf,searchv,recordid);
    res.send(response);
});

updateEntityApp.post('/deal', async (req, res) => {
    res.headers = headers;
    let bodyData = typeof req.body === "object" ? req.body : parse(req.body);
    let { searchf, searchv, recordid, ...inputData } = bodyData;
    let response =  await updateEntity(inputData,'deal',searchf,searchv,recordid);
    res.send(response);
});

updateEntityApp.post('/', async (req, res) => {
    res.headers = headers;
    let bodyData = typeof req.body === "object" ? req.body : parse(req.body);
    let { entity, searchf, searchv, recordid, ...inputData } = bodyData;
    console.log(entity, searchf, searchv, recordid, inputData);
    let response =  await updateEntity(inputData,entity,searchf,searchv,recordid);
    res.send(response);
});


module.exports = updateEntityApp;

