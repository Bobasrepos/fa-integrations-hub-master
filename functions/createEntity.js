const express = require('express');
const cors = require('cors');
let bodyParser = require('body-parser');
const {parse} = require('querystring');
const { createCustomEntity } = require('./faApiFunctions');

const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "POST",
    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    "Content-Type": "application/json"
};

const creatEntity = express();

creatEntity.use(bodyParser.json());
// Automatically allow cross-origin requests
creatEntity.use(cors({origin: true}));

creatEntity.use(express.urlencoded({ extended: true }));

// Parse JSON bodies (as sent by API clients)
creatEntity.use(express.json());

creatEntity.post('/contact', async (req, res) => {
    res.headers = headers
    let inputData = typeof req.body === "object" ? req.body : parse(req.body);
    let response =  await createCustomEntity(inputData, 'contact');
    res.send(response);
});

creatEntity.post('/logo', async (req, res) => {
    res.headers = headers
    let inputData = typeof req.body === "object" ? req.body : parse(req.body);
    let response =  await createCustomEntity(inputData, 'logo');
    res.send(response);
});

creatEntity.post('/deal', async (req, res) => {
    res.headers = headers
    let inputData = typeof req.body === "object" ? req.body : parse(req.body);
    let response =  await createCustomEntity(inputData, 'deal');
    res.send(response);
});

creatEntity.post('/', async (req, res) => {
    res.headers = headers
    let bodyData = typeof req.body === "object" ? req.body : parse(req.body);
    let { entity, ...inputData } = bodyData;
    let response =  await createCustomEntity(inputData, entity);
    res.send(response);
});


module.exports = creatEntity;

