import {formStylesCheckboxElementLabel} from './formStylesObjects';

let styles = localStorage.getItem('formStylesCheckboxElementLabel') ? JSON.parse(localStorage.getItem('formStylesCheckboxElementLabel')) : formStylesCheckboxElementLabel;


export default (state = {
    ...styles
}, action) => {
    if (action.type === 'FORM_CHECKBOX_ELEMENT_LABEL_CHANGED') {
        if (action.styleObj.property && action.styleObj.value) {
            return {
                ...state,
                [action.styleObj.property]: action.styleObj.value
            };
        } else {
            return action.styleObj;
        }
    } else {
        return state;
    }
};
