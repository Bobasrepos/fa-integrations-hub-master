let settingsFont = { min: 6, max: 32, step: 1, extension: 'px', marks: null };
let settingsPercent = { min: 5, max: 100, step: 5, extension: '%', marks: null };
let settingsPercentSmallSteps = { min: 0, max: 100, step: 1, extension: '%', marks: null };
let settingsPx = { min: 50, max: 1500, step: 10, extension: 'px', marks: null };
let settingsPxSmall = { min: 0, max: 100, step: 1, extension: 'px', marks: null };
let settingsEmSmall = { min: 0, max: 2, step: .001, extension: 'em', marks: null };
let settingsFonWight = { min: 200, max: 1000, step: 100, extension: '', marks: null };
let verticalPositionOptions = [{label: 'Left', value: 'flex-start'}, {label: 'Center', value: 'center'},{label: 'Right', value: 'flex-end'}];
let horizontalPositionOptions = [{label: 'Top', value: 'flex-start'}, {label: 'Center', value: 'center'},{label: 'Bottom', value: 'flex-end'}];
let backgroundSizeOptions = [{label: 'Cover', value: 'cover'}, {label: 'Contain', value: 'contain'},{label: 'Auto', value: 'auto'}];
let containerWidthOptions = [{label: 'Parent Width (100%)', value: '100%'}, {label: 'Form Width (auto)', value: 'auto'}];
let containerHeightOptions = [{label: 'Parent Height (100%)', value: '100%'}, {label: 'View Port Height (100vh)', value: '100vh'},{label: 'Form Height (auto)', value: 'auto'}];


let backgroundPositionXOptions = [
    {label:'Left', value: 'left'},
    {label:'Center', value: 'center'},
    {label:'Right', value: 'right'},
    {label:'0%', value: '0%'},
    {label:'5%', value: '5%'},
    {label:'10%', value: '10%'},
    {label:'20%', value: '20%'},
    {label:'30%', value: '30%'},
    {label:'35%', value: '35%'},
    {label:'40%', value: '40%'},
    {label:'45%', value: '45%'},
    {label:'50%', value: '50%'},
    {label:'55%', value: '55%'},
    {label:'60%', value: '60%'},
    {label:'70%', value: '70%'},
    {label:'80%', value: '80%'},
    {label:'90%', value: '90%'},
    {label:'95%', value: '95%'},
    {label:'100%', value: '100%'},
];

let backgroundPositionYOptions = [
    {label:'Top', value: 'top'},
    {label:'Center', value: 'center'},
    {label:'Bottom', value: 'Bottom'},
    {label:'0%', value: '0%'},
    {label:'5%', value: '5%'},
    {label:'10%', value: '10%'},
    {label:'20%', value: '20%'},
    {label:'30%', value: '30%'},
    {label:'35%', value: '35%'},
    {label:'40%', value: '40%'},
    {label:'45%', value: '45%'},
    {label:'50%', value: '50%'},
    {label:'55%', value: '55%'},
    {label:'60%', value: '60%'},
    {label:'70%', value: '70%'},
    {label:'80%', value: '80%'},
    {label:'90%', value: '90%'},
    {label:'95%', value: '95%'},
    {label:'100%', value: '100%'},
];






let sliderFieldsArray = [
    { label:'Vertical Alignment', property:'justifyContent', element:'formContainerStyles', defaultValue: 'center' , isPosition: true, horizontalPositionOptions},
    { label:'Horizontal Alignment', property:'alignItems', element:'formContainerStyles', defaultValue: 'center' ,isPosition: true,  verticalPositionOptions},
    { label:'Background Size', property:'backgroundSize', element:'formContainerStyles', defaultValue: 'contain' ,isTextSelect: true,  options: backgroundSizeOptions},
    { label:'Background Position X', property:'backgroundPositionX', element:'formContainerStyles', defaultValue: 'center' ,isTextSelect: true,  options:backgroundPositionXOptions},
    { label:'Background Position Y', property:'backgroundPositionY', element:'formContainerStyles', defaultValue: 'top' ,isTextSelect: true,  options:backgroundPositionYOptions},
    { label:'Container Width', property:'width', element:'formContainerStyles', defaultValue: '100%' ,isTextSelect: true,  options:containerWidthOptions},
    { label:'Container Height', property:'height', element:'formContainerStyles', defaultValue: '100%' ,isTextSelect: true,  options:containerHeightOptions},
    { label:'Border Radius', property:'borderRadius', element:'formStyles', defaultValue: 4 , ...settingsPxSmall},
    { label:'Border Width', property:'borderWidth', element:'formStyles', defaultValue: 0 , ...settingsPxSmall},
    { label:'Padding', property:'padding', element:'formStyles', defaultValue: 15 , ...settingsPxSmall},
    { label:'Position Top', property:'top', element:'formStyles', defaultValue: 0 , ...settingsPercentSmallSteps},
    { label:'Position Left', property:'left', element:'formStyles', defaultValue: 0 , ...settingsPercentSmallSteps},
    { label:'Font Size', property:'fontSize', element:'formStylesLabel', defaultValue: 16 , ...settingsFont},
    { label:'Font Weight', property:'fontWeight', element:'formStylesLabel', defaultValue: 400, ...settingsFonWight },
    { label:'Letter Spacing', property:'letterSpacing', element:'formStylesLabel', defaultValue: 0, ...settingsEmSmall},
    { label:'Margin Left', property:'marginLeft', element:'formStylesLabel', defaultValue: 3, ...settingsPxSmall },
    { label:'Margin Bottom', property:'marginBottom', element:'formStylesLabel', defaultValue: 2, ...settingsPxSmall },
    { label:'Font Size', property:'fontSize', element:'formStylesInputSelect', defaultValue: 16 , ...settingsFont },
    { label:'Width', property:'width', element:'formStyles' , defaultValue: 100, ...settingsPercent },
    { label:'Max Width', property:'maxWidth', element:'formStyles', defaultValue: 500 , ...settingsPx},
    { label:'Min Width', property:'minWidth', element:'formStyles', defaultValue: 400 , ...settingsPx},
    { label:'Height', property:'height', element:'formStylesInputSelect', defaultValue: 30, ...settingsPxSmall },
    { label:'Padding Left', property:'paddingLeft', element:'formStylesInputSelect', defaultValue: 5, ...settingsPxSmall },
    { label:'Border Radius', property:'borderRadius', element:'formStylesInputSelect', defaultValue: 3, ...settingsPxSmall },
    { label:'Border Width', property:'borderWidth', element:'formStylesInputSelect', defaultValue: 1, ...settingsPxSmall },
    { label:'Margin Bottom', property:'marginBottom', element:'formStylesInputSelect', defaultValue: 10, ...settingsPxSmall },
    { label:'Font Size', property:'fontSize', element:'formStylesSubmitButton', defaultValue: 16, ...settingsFont },
    { label:'Letter Spacing', property:'letterSpacing', element:'formStylesSubmitButton', defaultValue: 0, ...settingsEmSmall},
    { label:'Width', property:'width', element:'formStylesSubmitButton', defaultValue: 100, ...settingsPercent },
    { label:'Height', property:'padding', element:'formStylesSubmitButton', defaultValue: 10, ...settingsPxSmall },
    { label:'Border Radius', property:'borderRadius', element:'formStylesSubmitButton', defaultValue: 3, ...settingsPxSmall },
    { label:'Border Width', property:'borderWidth', element:'formStylesSubmitButton', defaultValue: 1, ...settingsPxSmall },
    { label:'Font Size', property:'fontSize', element:'formStylesTextBlock', defaultValue: 18, ...settingsPxSmall },
    { label:'Font Weight', property:'fontWeight', element:'formStylesTextBlock', defaultValue: 700, ...settingsFonWight },
    { label:'Margin Bottom', property:'marginBottom', element:'formStylesTextBlock', defaultValue: 0, ...settingsPxSmall },
    { label:'Margin Left', property:'marginLeft', element:'formStylesTextBlock', defaultValue: 0, ...settingsPxSmall },
    { label:'Margin Right', property:'marginTop', element:'formStylesTextBlock', defaultValue: 0, ...settingsPxSmall },
    { label:'Margin Top', property:'marginTop', element:'formStylesTextBlock', defaultValue: 0, ...settingsPxSmall },
    { label:'Width', property:'width', element:'formStylesTextBlock', defaultValue: 100, ...settingsPercent },
    { label:'Letter Spacing', property:'letterSpacing', element:'formStylesTextBlock', defaultValue: 0, ...settingsEmSmall },
    { label:'Letter Spacing', property:'letterSpacing', element:'formStylesCheckboxElementLabel', defaultValue: 0, ...settingsEmSmall },
    { label:'Font Size', property:'fontSize', element:'formStylesCheckboxElementLabel', defaultValue: 12, ...settingsPxSmall },
    { label:'Font Weight', property:'fontWeight', element:'formStylesCheckboxElementLabel', defaultValue: 400, ...settingsFonWight },
];

let formContainerStyles = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    padding: '15px',
    boxSizing: 'border-box',
    overflowY: 'scroll',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain'
};

let formStyles = {
    display: 'grid',
    justifyContent: 'flex-start',
    alignItems: 'center',
    gridTemplateColumns: '1fr 1fr',
    gridGap: '10px',
    width: '100%',
    height: 'auto',
    maxWidth: '500px',
    minWidth: '400px',
    padding: '15px',
    overflowY: 'auto',
    fontFamily: 'Roboto',
    fontSize: '14px',
    boxSizing: 'border-box',
    borderRadius: '4px',
    borderStyle: 'solid',
    borderWidth: '0px',
    position: 'relative'
};

let formStylesDiv = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    fontFamily: 'Roboto',
    fontSize: '14px',
    boxSizing: 'border-box'
};

let formStylesInputSelect = {
    width: '100%',
    borderColor: 'rgba(183,183,183,1)',
    borderWidth: '1px',
    borderStyle: 'solid',
    height: "46px",
    paddingLeft: "16px",
    minHeight: '30px',
    fontFamily: 'Open Sans',
    fontSize: '16px',
    WebkitAppearance: 'none',
    MozAppearance: 'none',
    appearance: 'none',
    boxSizing: 'border-box',
    backgroundImage: 'url(data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUiIHdpZHRoPSI3IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0yLjgyOCA0LjM2NEwwIDEuNTM2LjcwNy44MjhsMi44MjkgMi44MjlMNi4zNjQuODI4bC43MDcuNzA4TDMuNTM2IDUuMDd6IiBmaWxsPSIjQkJCRUJGIi8+PC9zdmc+)',
    backgroundPosition: 'right 10px center',
    backgroundRepeat: 'no-repeat',
    color: 'rgba(102,106,110,1)',
    borderRadius: "6px",
    backgroundColor: "white",
    marginBottom: "5px"
};

let formStylesLabel = {
    marginLeft: '3px',
    marginBottom: '2px',
    fontWeight: 400,
    fontFamily: "Open Sans, sans serif",
    fontSize: "15px",
    color: "rgba(106,106,107,1)",
    letterSpacing: "0.03em"
};

let formSubmitContainerStyles = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    gridColumn: '1/-1',
    fontFamily:  "Open Sans, sans serif",
    boxSizing: 'border-box',
    marginBottom: "10px"
};


let formStylesSubmitButton = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'rgba(183,183,183,1)',
    borderWidth: '1px',
    borderStyle: 'solid',
    height: '100%',
    marginTop: '10px',
    fontFamily: "Open Sans, sans serif",
    fontSize: '16px',
    boxSizing: 'border-box',
    color: 'RGBA(54, 61, 70,1)',
    width: "35%",
    borderRadius: "6px",
    padding: "13px"
};

let formStylesCheckboxContainer = {
    display: 'grid',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    gridTemplateColumns: '1fr 1fr 1fr 1fr',
    width: '100%',
    marginBottom: '5px',
    marginTop: '20px',
    fontFamily: 'Open Sans, sans serif',
    boxSizing: 'border-box'
};

let formStylesCheckboxElementContainer = {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'column',
    width: '100%',
    marginBottom: '10px',
    fontFamily: 'Open Sans, sans serif',
    boxSizing: 'border-box'
};

let formStylesCheckboxElementLabel = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    fontFamily: 'Open Sans, sans serif',
    fontSize: '12px',
    boxSizing: 'border-box',
    fontWeight: 400,
    color: 'rgb(106, 106, 107)'
};

let formStylesTextBlock = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 700,
    fontFamily: 'Open Sans, sans serif',
    fontSize: "1",
    color: "rgba(106,106,107,1)",
    letterSpacing: "0em"
};

let fontsList = [
    'Arial', 'Times New Roman', 'Helvetica', 'Times', 'Courier New', 'Verdana',
    'Courier', 'Arial Narrow', 'Candara', 'Geneva', 'Calibri', 'Optima', 'Cambria','Garamond',
    'Perpetua', 'Didot', 'Lucida Bright', 'Copperplate' , 'Droid Sans', 'Inter', 'Droid Serif', 'Roboto',
    "Segoe UI", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Helvetica Neue",
    "Open Sans", "Lato", "Old Standard TT","Abril Fatface", "PT Serif" ,"Vollkorn",
    "PT Mono","Gravitas One"
];

let googleFontList = [
    { font: 'Arial', subFont: 'sans-serif' },
    { font: 'Arial Black', subFont: 'sans-serif' },
    { font: 'Times New Roman', subFont: 'sans-serif' },
    { font: 'Times', subFont: 'sans-serif' },
    { font: 'Courier New', subFont: 'sans-serif' },
    { font: 'Verdana', subFont: 'sans-serif' },
    { font: 'Georgia', subFont: 'sans-serif' },
    { font: 'Palatino', subFont: 'sans-serif' },
    { font: 'Garamond', subFont: 'sans-serif' },
    { font: 'Bookman', subFont: 'sans-serif' },
    { font: 'Candara', subFont: 'sans-serif' },
    { font: 'Impact', subFont: 'sans-serif' },
    { font: 'Roboto', subFont: 'sans-serif' },
    { font: 'Open Sans', subFont: 'sans-serif' },
    { font: 'Inter', subFont: 'sans-serif' },
    { font: 'Ubuntu', subFont: 'sans-serif' },
    { font: 'EB Garamond', subFont: 'serif' },
    { font: 'PT Mono', subFont: 'sans-serif' },
    { font: 'Vollkorn', subFont: 'serif' },
    { font: 'Courier Prime', subFont: 'monospace' },
    { font: 'Fira Sans', subFont: 'sans-serif' },
    { font: 'Oxygen', subFont: 'sans-serif' },
    { font: 'Old Standard TT', subFont: 'serif' },
    { font: 'Gravitas One', subFont: 'cursive' },
    { font: 'PT Serif', subFont: 'serif' },
    { font: 'Montserrat', subFont: 'serif' },
    { font: 'Source Sans Pro', subFont: 'serif' },
    { font: 'Roboto Mono', subFont: 'serif' },
    { font: 'Raleway', subFont: 'serif' }
];


export { sliderFieldsArray,
    formContainerStyles,
    formStyles,
    formStylesDiv ,
    formStylesInputSelect ,
    formStylesLabel ,
    formStylesSubmitButton,
    fontsList,
    googleFontList,
    formStylesCheckboxContainer,
    formStylesCheckboxElementContainer,
    formStylesCheckboxElementLabel,
    formSubmitContainerStyles,
    formStylesTextBlock
};
