import {formStylesLabel} from './formStylesObjects';

let styles = localStorage.getItem('formStylesLabel') ? JSON.parse(localStorage.getItem('formStylesLabel')) : formStylesLabel;


export default (state = {
    ...styles
}, action) => {
    if (action.type === 'FORM_LABEL_CHANGED') {
        if (action.styleObj.property && action.styleObj.value) {
            return {
                ...state,
                [action.styleObj.property]: action.styleObj.value
            };
        } else {
            return action.styleObj;
        }
    } else {
        return state;
    }
};
