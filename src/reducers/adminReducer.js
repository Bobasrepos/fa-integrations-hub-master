import { sliderFieldsArray, fontsList, googleFontList } from './formStylesObjects';
import response from '../response';
let submitButtonField = {label:"Submit",  key:"submit_button", type:"text", fieldType:"button", entity :"textbox",  width:"100%"};
let activeEditor = sessionStorage.getItem('activeEditor') ? sessionStorage.getItem('activeEditor') : 'data';
let creds = localStorage.getItem('creds') ? JSON.parse(localStorage.getItem('creds')) : null;
let fieldsFromStorage = localStorage.getItem('fieldsFromFa') ? JSON.parse(localStorage.getItem('fieldsFromFa')) : [];
let editedFieldsFromStorage = localStorage.getItem('editedFields') ? JSON.parse(localStorage.getItem('editedFields')) : [];
let editedSelectionFromStorage = localStorage.getItem('selectionList') ? JSON.parse(localStorage.getItem('selectionList')) : [];
let formFieldsFromStorage = localStorage.getItem('formFields') ? JSON.parse(localStorage.getItem('formFields')) : [];
let activeDataFromStorage = localStorage.getItem('activeData') ? localStorage.getItem('activeData') : 'contact';
let backupsFromStorage = localStorage.getItem('backups') ? JSON.parse(localStorage.getItem('backups')) : null;

editedSelectionFromStorage = editedSelectionFromStorage.length === 0 ? fieldsFromStorage : editedSelectionFromStorage;

let entitiesMenu = [];
if (fieldsFromStorage && fieldsFromStorage.length > 0) {
    fieldsFromStorage.map((entityData,i) => {
        entitiesMenu.push({text: Object.keys(entityData)[0], index: i});
    });
}


export default (state = {
    sliderFieldsArray,
    fontsList,
    googleFontList,
    activeEditor: activeEditor,
    creds,
    fieldsFromFa: fieldsFromStorage,
    editedFields: editedFieldsFromStorage,
    selectionList:editedSelectionFromStorage,
    formFields:formFieldsFromStorage,
    activeData: activeDataFromStorage,
    entitiesMenu: entitiesMenu,
    tempRelation: null,
    relationsList: [],
    submitButton: [submitButtonField],
    editingIsOn: false,
    refresh: false,
    optionsModalIsOpen: false,
    selectedStyle: 'labels',
    allBackups: backupsFromStorage
}, action) => {
    switch (action.type) {
        case 'ADD_USER_INFO':
            return {
                ...state,
                user: action.user
            };
        case 'LOAD_PAGE':
            return {
                ...state,
                isLoggedIn: true
            };
        case 'LOAD_FIELDS':
            let selectionList = [];
            let key = Object.keys(action.fields[0])[0];
            let entitiesMenu = [];
            action.fields.map((entityData,i) => {
                entitiesMenu.push({text: Object.keys(entityData)[0], index: i});
            });

            selectionList = [...selectionList, ...action.fields[0][key]];

            localStorage.setItem('fieldsFromFa', JSON.stringify(action.fields));
            localStorage.setItem('editedFields', JSON.stringify(action.fields));
            localStorage.setItem('selectionList', JSON.stringify(action.fields[0][key]));
            localStorage.setItem('formFields', JSON.stringify([]));
            return {
                ...state,
                fieldsFromFa: action.fields,
                editedFields: action.fields,
                selectionList: selectionList,
                formFields: [],
                activeData: key,
                entitiesMenu
            };
        case 'LOAD_FIELDS_FROM_FA':
            localStorage.setItem('fieldsFromFa', JSON.stringify(action.fields));
            return {
                ...state,
                fieldsFromFa: action.fields,
            };
        case 'LOAD_ALL_BACKUPS':
            localStorage.setItem('backups', JSON.stringify(action.backups));
            return {
                ...state,
                allBackups: action.backups,
            };
        case 'LOAD_FIELDS_FROM_BACKUP':
            let entitiesMenuBackup = [];
            let keyBackup = Object.keys(action.backup.editedFields[0])[0];
            action.backup.editedFields.map((entityData,i) => {
                entitiesMenuBackup.push({text: Object.keys(entityData)[0], index: i});
            });
            localStorage.setItem('editedFields', JSON.stringify(action.backup.editedFields));
            localStorage.setItem('selectionList', JSON.stringify(action.backup.selectionList));
            localStorage.setItem('formFields', JSON.stringify(action.backup.formFields));
            return {
                ...state,
                editedFields: action.backup.editedFields,
                selectionList: action.backup.selectionList,
                formFields: action.backup.formFields,
                activeData: keyBackup,
                entitiesMenu: entitiesMenuBackup
            };
        case 'TOGGLE_REQUIRED':
            let formFieldsCopy = [...state.formFields];
            let selectionListArray = state.selectionList.map(field => {
                if (field.key === action.key) {
                    if (field.selected) {
                        field.selected = !field.selected;
                        formFieldsCopy = formFieldsCopy.filter(f => f.key !== action.key);
                    } else {
                        field.selected = true;
                        formFieldsCopy = [...formFieldsCopy, field]
                    }
                }
                return field
            });
            let editedFieldsToStorage = [...state.editedFields];
            editedFieldsToStorage[state.activeData] = selectionListArray;
            localStorage.setItem('editedFields', JSON.stringify(editedFieldsToStorage));
            localStorage.setItem('selectionList', JSON.stringify(selectionListArray));
            localStorage.setItem('formFields', JSON.stringify(formFieldsCopy));
            return {
                ...state,
                selectionList: selectionListArray,
                formFields : formFieldsCopy,
            };
        case 'LOAD_ENTITY':
            localStorage.setItem('selectionList', JSON.stringify(state.editedFields[action.entityInfo.index][action.entityInfo.text]));
            localStorage.setItem('activeData', action.entityInfo.text);
            return {
                ...state,
                selectionList: state.editedFields[action.entityInfo.index][action.entityInfo.text],
                activeData: action.entityInfo.text,
                activeEditor: 'data'
            };
        case 'CHANGED_FORM_HTML':
            return {
                ...state,
                formOuterHtml: action.htmlString,
            };

        case 'CREDS_ADDED':
            return {
                ...state,
                creds: action.creds,
            };
        case 'STYLE_CHANGED':
            let stateCopy = {...state};
            stateCopy[action.styleObj.element][action.styleObj.property] = action.styleObj.value;
            stateCopy.refresh = !state.refresh;
            return stateCopy;
        case 'EDITOR_CHANGED':
            return {
                ...state,
                activeEditor: action.name
            };
        case 'LOAD_SETTINGS_RELATION':
            return {
                ...state,
                activeData: action.data,
                activeEditor: 'data'
            };
        case 'MAKE_RELATION':
            let tempCopy = state.tempRelation;
            let relationsListCopy = [...state.relationsList];
            if (!state.tempRelation) {
                tempCopy = action.relationInfo;
            } else {
                if (tempCopy.entity !== action.relationInfo.entity) {
                    relationsListCopy = [...relationsListCopy, [tempCopy, action.relationInfo]];
                    tempCopy = null;
                }
            }
            return {
                ...state,
                tempRelation:tempCopy,
                relationsList:relationsListCopy
            };
        case 'TOGGLE_EDIT':
            return {
                ...state,
                editingIsOn: !state.editingIsOn
            };
        case 'DATA_EDITED':
            let editedFieldsCopy = [...state.editedFields];
            let editedSelection = [...state.selectionList];
            let formCopy = [...state.formFields];
            let newChoices = [];
            if (action.changeInfo.entity !== 'textbox') {
                let {entity, key, name, value, editingLabel, optionValue} = action.changeInfo;
                editedFieldsCopy = editedFieldsCopy.map(entityFields => {
                    if (entityFields[entity]) {
                        entityFields[entity] = entityFields[entity].map(field => {
                            if (field.key === key) {
                                field[name] = value
                            }
                            return field;
                        });
                        editedSelection = entityFields[entity];
                        return { [entity] : entityFields[entity]}
                    } else {
                        return entityFields
                    }
                });
            }
            formCopy = formCopy.map(formField => {
                if (formField.entity === action.changeInfo.entity && formField.key === action.changeInfo.key) {
                    formField[action.changeInfo.name] = action.changeInfo.value;
                }
                return formField;
            });
            localStorage.setItem('editedFields', JSON.stringify(editedFieldsCopy));
            localStorage.setItem('selectionList', JSON.stringify(editedSelection));
            localStorage.setItem('formFields', JSON.stringify(formCopy));
            localStorage.setItem('optionsModalChoices', JSON.stringify(newChoices));
            return {
                ...state,
                editedFields: editedFieldsCopy,
                selectionList: editedSelection,
                formFields: formCopy,
                optionsModalChoices: newChoices,
                refresh: !state.refresh
            };
        case 'OPTION_EDITED':
            let editedFieldsCopy1 = [...state.editedFields];
            let editedSelection1 = [...state.selectionList];
            let formCopy1 = [...state.formFields];
            let newChoices1 = [];
            editedFieldsCopy1 = editedFieldsCopy1.map(entityFields => {
                if (entityFields[action.changeInfo.entity]) {
                    entityFields[action.changeInfo.entity] = entityFields[action.changeInfo.entity].map(field => {
                        if (field.key === action.changeInfo.key) {
                            if (action.changeInfo.name === 'choices') {
                                if (field.choices) {
                                    if (!action.changeInfo.editingLabel) {
                                        field.choices = field.choices.filter(choice => choice.value !== action.changeInfo.value);
                                    } else {
                                        field.choices = field.choices.map(choice => choice.value === action.changeInfo.optionValue ? {
                                            label: action.changeInfo.value,
                                            value: choice.value
                                        } : choice);
                                    }
                                    newChoices1 = field.choices;
                                }
                            }
                        }
                        return field;
                    });
                    editedSelection1 = entityFields[action.changeInfo.entity];
                    return { [action.changeInfo.entity] : entityFields[action.changeInfo.entity]}
                } else {
                    return entityFields
                }
            });
            formCopy1 = formCopy1.map(formField => {
                if (formField.key === action.changeInfo.key) {
                    if (action.changeInfo.name === 'choices') {
                        if (formField.choices) {
                            if (!action.changeInfo.editingLabel) {
                                formField.choices = formField.choices.filter(choice => choice.value !== action.changeInfo.value);
                            } else {
                                formField.choices = formField.choices.map(choice => {
                                    if (choice.value === action.changeInfo.optionValue) {
                                        return {
                                            label: action.changeInfo.value,
                                            value: choice.value
                                        }
                                    }   else {
                                        return choice;
                                    }
                                });
                                newChoices1 = formField.choices;
                            }
                        }
                    }
                    return formField;
                } else {
                    return formField;
                }
            });
            return {
                ...state,
                editedFields: editedFieldsCopy1,
                selectionList: editedSelection1,
                formFields: formCopy1,
                refresh: !state.refresh,
                optionsModalChoices: newChoices1
            };
        case 'OPTION_ADDED':
            let editedFieldsCopy2 = [...state.editedFields];
            let editedSelection2 = [...state.selectionList];
            let formCopy2 = [...state.formFields];
            let newChoices2 = [];
            editedFieldsCopy2 = editedFieldsCopy2.map(entityFields => {
                if (entityFields[action.changeInfo.entity]) {
                    entityFields[action.changeInfo.entity] = entityFields[action.changeInfo.entity].map(field => {
                        if (field.entity === action.changeInfo.entity && field.key === action.changeInfo.key) {
                            if (field.choices) {
                                let lastOption1 = field.choices.filter(choice => choice.value && choice.value.includes('new_option'));
                                let optionId = lastOption1 && lastOption1.length > 0 ? lastOption1.length + 1 : 1;
                                field.choices = [...field.choices, {label: 'New Option', value: `new_option_${optionId}`}];
                            } else {
                                field = {...field, choices: [{label: 'New Option', value: `new_option_1`}]}
                            }
                            newChoices2 = field.choices;
                        }
                        return field;
                    });
                    editedSelection2 = entityFields[action.changeInfo.entity];
                    return {[action.changeInfo.entity]: entityFields[action.changeInfo.entity]}
                } else {
                    return entityFields
                }
            });

            formCopy2 = formCopy2.map(formField => {
                if (formField.entity === action.changeInfo.entity && formField.key === action.changeInfo.key) {
                    if (formField.choices) {
                        let lastOption1 = [];
                        lastOption1 = formField.choices.filter(choice => choice.value && choice.value.includes('new_option'));
                        let optionId2 = lastOption1 && lastOption1.length > 0 ? lastOption1.length + 1 : 1;
                        formField.choices = [...formField.choices, {label: 'New Option', value: `new_option_${optionId2}`}];
                    } else {
                        formField.choices = [{label: 'New Option', value: `new_option_1`}];
                    }
                }
                return formField;
            });

            return {
                ...state,
                editedFields: editedFieldsCopy2,
                selectionList: editedSelection2,
                formFields: formCopy2,
                optionsModalChoices: newChoices2
            };
        case 'LOAD_FORM_FIELDS':
            return {
                ...state,
                formFields: action.newOrderFields
            };
        case 'ADD_TEXT_BLOCK_TO_FORM':
            let textBlockId = 1;
            if (state.textBlockList) {
                let lastKey = state.textBlockList[state.textBlockList.length - 1].key;
                textBlockId = parseFloat(lastKey[lastKey.length - 1]) + 1;
            }
            let newTextObject = {label: `Text ${textBlockId} goes here`, key: `text_block_${textBlockId}`, type: 'text', fieldType: 'div', entity: 'textbox'};
            let textBlockListCopy = state.textBlockList ? [...state.textBlockList, newTextObject] : [newTextObject];
            localStorage.setItem('formFields', JSON.stringify([...state.formFields, newTextObject]));
            return {
                ...state,
                formFields: [...state.formFields, newTextObject],
                textBlockList: textBlockListCopy
            };
        case 'ADD_NOTE_FIELD_TO_FORM':
            let noteFieldId = 1;
            if (state.noteFieldsList) {
                let lastKey = state.noteFieldsList[state.noteFieldsList.length - 1].key;
                noteFieldId = parseFloat(lastKey[lastKey.length - 1]) + 1;
            }
            let newNoteObject = {label: `Add a note`, key: `note_field_${noteFieldId}`, type: 'text', fieldType: 'textarea', entity: 'contact'};
            let noteFieldsListCopy = state.noteFieldsList ? [...state.noteFieldsList, newNoteObject] : [newNoteObject];
            localStorage.setItem('formFields', JSON.stringify( [...state.formFields, newNoteObject]));
            return {
                ...state,
                formFields: [...state.formFields, newNoteObject],
                noteFieldsList: noteFieldsListCopy
            };
        case 'OPTION_MODAL_OPEN':
            return {
                ...state,
                optionsModalIsOpen: true,
                optionsModalData: action.modalData,
                optionsModalChoices: action.modalData.choices,

            };
        case 'CLOSE_OPTION_MODAL':
            return {
                ...state,
                optionsModalIsOpen: false,
                optionsModalData: null,
                optionsModalChoices: null
            };
        case 'LOAD_SELECTED_STYLE':
            return {
                ...state,
                selectedStyle: action.styleInfo,
                activeEditor: 'styles'
            };
        case 'REMOVE_BACKUP_ITEM':
            let allBackupsCopy = [...state.allBackups];
            allBackupsCopy = allBackupsCopy.filter(backup => Object.keys(backup)[0] !== action.backupId);
            if (allBackupsCopy && allBackupsCopy.length === 0) {
                allBackupsCopy = null;
            }
            localStorage.setItem('backups', JSON.stringify(allBackupsCopy));
            return {
                ...state,
                allBackups:allBackupsCopy
            };
        case 'ADD_BACKUP_ITEM':
            let allBackupsCopyAdded = [];
            if (state.allBackups) {
                allBackupsCopyAdded = [...state.allBackups, action.backup];
            } else {
                allBackupsCopyAdded = [action.backup]
            }

            localStorage.setItem('backups', JSON.stringify(allBackupsCopyAdded));
            return {
                ...state,
                allBackups:allBackupsCopyAdded
            };
        case 'DEFAULT_CHANGED':
            let allBackupDefault = [...state.allBackups];
            allBackupDefault = allBackupDefault.map(backup => {
                let key = Object.keys(backup)[0];
                if (key === action.backupInfo.id) {
                    return {[action.backupInfo.id]: action.backupInfo.data}
                } else {
                    if (action.backupInfo.type === 'plugin') {
                         backup[key].isPlugInDefault = 'No';
                    }
                    if (action.backupInfo.type === 'iframe') {
                        backup[key].isIframeDefault = 'No';
                    }
                    return backup
                }
            });
            localStorage.setItem('backups', JSON.stringify(allBackupDefault))
            return {
                ...state,
                allBackups: allBackupDefault
            };
        case 'PARENT_CHANGED':
            let formCopyParent = [...state.formFields];
            formCopyParent = formCopyParent.map(formField => {
                if (formField.entity === action.changeData.entity && formField.key === action.changeData.key) {
                    formField.parent = action.changeData.value;
                }
                return formField;
            });
            localStorage.setItem('formFields', JSON.stringify(formCopyParent));
            return {
                ...state,
                formFields: formCopyParent,
            };

        default:
            return state;
    }
};
