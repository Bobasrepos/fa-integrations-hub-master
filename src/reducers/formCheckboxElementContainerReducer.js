import {formStylesCheckboxElementContainer} from './formStylesObjects';

let styles = localStorage.getItem('formStylesCheckboxElementContainer') ? JSON.parse(localStorage.getItem('formStylesCheckboxElementContainer')) : formStylesCheckboxElementContainer;


export default (state = {
    ...styles
}, action) => {
    if (action.type === 'FORM_CHECKBOX_ELEMENT_CONTAINER_CHANGED') {
        if (action.styleObj.property && action.styleObj.value) {
            return {
                ...state,
                [action.styleObj.property]: action.styleObj.value
            };
        } else {
            return action.styleObj;
        }
    } else {
        return state;
    }
};
