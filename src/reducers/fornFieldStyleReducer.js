import {formStylesDiv} from './formStylesObjects';

let styles = localStorage.getItem('formStylesDiv') ? JSON.parse(localStorage.getItem('formStylesDiv')) : formStylesDiv;

export default (state = {
    ...styles
}, action) => {
    if (action.type === 'FORM_FIELD_CHANGED') {
        if (action.styleObj.property && action.styleObj.value) {
            return {
                ...state,
                [action.styleObj.property]: action.styleObj.value
            };
        } else {
            return action.styleObj;
        }
    } else {
        return state;
    }
};
