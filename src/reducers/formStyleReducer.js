import {formStyles} from './formStylesObjects';

let styles = localStorage.getItem('formStyles') ? JSON.parse(localStorage.getItem('formStyles')) : formStyles;

export default (state = {
    ...styles
}, action) => {
    if (action.type === 'FORM_CHANGED') {
        if (action.styleObj.property && action.styleObj.value) {
            return {
                ...state,
                [action.styleObj.property]: action.styleObj.value
            };
        } else {
            return action.styleObj;
        }
    } else {
        return state;
    }
};
