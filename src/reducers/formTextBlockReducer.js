import {formStylesTextBlock} from './formStylesObjects';

let styles = localStorage.getItem('formStylesTextBlock') ? JSON.parse(localStorage.getItem('formStylesTextBlock')) : formStylesTextBlock;


export default (state = {
    ...styles
}, action) => {
    if (action.type === 'FORM_TEXT_BLOCK_CHANGED') {
        if (action.styleObj.property && action.styleObj.value) {
            return {
                ...state,
                [action.styleObj.property]: action.styleObj.value
            };
        } else {
            return action.styleObj;
        }
    } else {
        return state;
    }
};
