import { formStylesSubmitButton } from './formStylesObjects';

let submitStyles = localStorage.getItem('formStylesSubmitButton') ? JSON.parse(localStorage.getItem('formStylesSubmitButton')) : formStylesSubmitButton;

export default (state = {
    ...submitStyles
}, action) => {
    if (action.type === 'FORM_SUBMIT_CHANGED') {
        if (action.styleObj.property && action.styleObj.value) {
            return {
                ...state,
                [action.styleObj.property]: action.styleObj.value
            };
        } else {
            return action.styleObj;
        }
    } else {
        return state;
    }
};
