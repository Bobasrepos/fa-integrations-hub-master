import { formSubmitContainerStyles } from './formStylesObjects';

let sessionStyles = localStorage.getItem('formSubmitContainerStyles') ? JSON.parse(localStorage.getItem('formSubmitContainerStyles')) : formSubmitContainerStyles;

export default (state = {
    ...sessionStyles
}, action) => {
    if (action.type === 'FORM_SUBMIT_CONTAINER_CHANGED') {
        if (action.styleObj.property && action.styleObj.value) {
            return {
                ...state,
                [action.styleObj.property]: action.styleObj.value
            };
        } else {
            return action.styleObj;
        }
    } else {
        return state;
    }
};
