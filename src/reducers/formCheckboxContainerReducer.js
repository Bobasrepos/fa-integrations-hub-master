import {formStylesCheckboxContainer} from './formStylesObjects';

let styles = localStorage.getItem('formStylesCheckboxContainer') ? JSON.parse(localStorage.getItem('formStylesCheckboxContainer')) : formStylesCheckboxContainer;


export default (state = {
    ...styles
}, action) => {
    if (action.type === 'FORM_CHECKBOX_CONTAINER_CHANGED') {
        if (action.styleObj.property && action.styleObj.value) {
            return {
                ...state,
                [action.styleObj.property]: action.styleObj.value
            };
        } else {
            return action.styleObj;
        }
    } else {
        return state;
    }
};
