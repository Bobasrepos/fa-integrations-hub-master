import {formStylesInputSelect} from './formStylesObjects';

let styles = localStorage.getItem('formStylesInputSelect') ? JSON.parse(localStorage.getItem('formStylesInputSelect')) : formStylesInputSelect;

export default (state = {
    ...styles
}, action) => {
    if (action.type === 'INPUT_SELECT_CHANGED') {
        if (action.styleObj.property && action.styleObj.value) {
            return {
                ...state,
                [action.styleObj.property]: action.styleObj.value
            };
        } else {
            return action.styleObj;
        }
    } else {
        return state;
    }
};
