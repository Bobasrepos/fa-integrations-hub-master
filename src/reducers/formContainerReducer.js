import {formContainerStyles} from './formStylesObjects';

let styles = localStorage.getItem('formContainerStyles') ? JSON.parse(localStorage.getItem('formContainerStyles')) : formContainerStyles;


export default (state = {
    ...styles
}, action) => {
    if (action.type === 'FORM_CONTAINER_CHANGED') {
        if (action.styleObj.property && action.styleObj.value) {
            return {
                ...state,
                [action.styleObj.property]: action.styleObj.value
            };
        } else {
            return action.styleObj;
        }
    } else {
        return state;
    }
};
