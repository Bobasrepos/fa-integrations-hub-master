import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import selectValues from './reducers/adminReducer';
import formFieldStyles from './reducers/fornFieldStyleReducer';
import inputSelectStyles from './reducers/formInputSlectReducer';
import formContainerStyles from './reducers/formContainerReducer';
import formStyles from './reducers/formStyleReducer';
import labelStyle from './reducers/formLabelReducer';
import submitStyle from './reducers/formSubmitReducer';
import formSubmitContainerStyles from './reducers/formSubmitContainerReducer';
import formCheckboxContainerReducer from "./reducers/formCheckboxContainerReducer";
import formCheckboxElementLabelReducer from "./reducers/formCheckboxElementLabelReducer";
import formTextBlockReducer from "./reducers/formTextBlockReducer";
import formCheckboxElementContainerReducer from "./reducers/formCheckboxElementContainerReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    return createStore(
        combineReducers({
            values: selectValues,
            field: formFieldStyles,
            inputselect: inputSelectStyles,
            container: formContainerStyles,
            form: formStyles,
            label: labelStyle,
            submit: submitStyle,
            submitContainer: formSubmitContainerStyles,
            textBlock: formTextBlockReducer,
            checkboxContainer: formCheckboxContainerReducer,
            checkboxElementLabel: formCheckboxElementLabelReducer,
            checkboxElementContainer: formCheckboxElementContainerReducer
        }),
        composeEnhancers(applyMiddleware(thunk))
    );
};
