import React, { Component, useEffect} from 'react';
import { connect } from 'react-redux';
import { getFaFields, getHashedId } from '../utils';
import { loadFields, getFieldsFromBackup, getFieldsFromFaOnly } from '../actions/actions';
import Menu from '../components/MenuBlock/Menu';
import DataStyleCode from '../components/MainEditor';
import FormFieldGenerator from '../components/FormBlock/FormFieldGenerator';
import FormOptionsListModal from "../components/AppsBlock/FormOptionsListModal";
import db from '../DB/db';

let AdminPage = props => {
    useEffect(() => {
        let clientId = props.clientId || '';
        let id = getHashedId(clientId);
        let clientSecret = props.secret || '';
        if (!props.fieldsFromFa || (props.fieldsFromFa && props.fieldsFromFa.length === 0)) {
            let docRef = db.collection("formBackups").doc(id);
            docRef.get().then(async function(doc) {
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    props.getFieldsFromBackup(doc.data());
                    getFaFields(clientId, clientSecret).then(res => {
                        if (res.status === 200) {
                            localStorage.setItem('fieldsFromFa', JSON.stringify(res.data));
                            props.getFieldsFromFaOnly(res.data);
                        } else {
                            console.log('We couldn\'t get the fields list from FA');
                        }
                    })
                        .catch(e => console.error(e))
                } else {
                    getFaFields(clientId, clientSecret).then(res => {
                            if (res.status === 200) {
                                localStorage.setItem('fieldsFromFa', JSON.stringify(res.data));
                                props.getFieldsFromFa(res.data);
                            } else {
                                console.log('We couldn\'t get the fields list from FA');
                            }
                        })
                        .catch(e => console.error(e))
                }
            }).catch(function(error) {
                console.log("Error getting document:", error);
            });

        }
    }, []);


        return (
            <div className="admin_page_container">
                { props.selectionList && props.selectionList.length > 0 && <>
                    <Menu />
                    <DataStyleCode />
                    <FormFieldGenerator />
                    {props.optionsModalIsOpen && <FormOptionsListModal />}
                </> }
            </div>
        )
};

const mapStateToProps = (state) => ({
    user: state.values.user,
    fieldsFromFa: state.values.fieldsFromFa,
    selectionList: state.values.selectionList,
    clientId: state.values.creds.clientIdValue,
    secret: state.values.creds.secretValue,
    optionsModalIsOpen: state.values.optionsModalIsOpen,
});

const mapDispatchToProps = (dispatch) => ({
    getFieldsFromFa: (fields) => dispatch(loadFields(fields)),
    getFieldsFromBackup: (backup) => dispatch(getFieldsFromBackup(backup)),
    getFieldsFromFaOnly: (fields) => dispatch(getFieldsFromFaOnly(fields))
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminPage);

