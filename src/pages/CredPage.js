import React, {Component} from 'react';
import {connect} from 'react-redux';
import { addCreds } from '../actions/actions';


class FieldsList extends Component {

    saveCreds(e) {
        e.preventDefault();
        let clientId = document.querySelector('#fa_client_id');
        let clientIdValue = '';
        let secret = document.querySelector('#fa_secret');
        let secretValue = '';
        if (clientId) {
            clientIdValue = clientId.value;
        }
        if (secret) {
            secretValue = secret.value;
        }

        if (clientIdValue !== '' && secretValue !== '') {
            localStorage.setItem("creds", JSON.stringify({ clientIdValue,secretValue }));
            this.props.addCreds({ clientIdValue,secretValue })
        }
    };

    render() {
        return (
            <div className="creds_page_container">
                <form className="cred_form_container">
                    <label htmlFor="">FA Client Id</label>
                    <input id="fa_client_id"/>
                    <label htmlFor="fa_secret">FA Secret</label>
                    <input id="fa_secret"/>
                    <button onClick={(e) => this.saveCreds(e)}>Save</button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({
    addCreds: (creds) => dispatch(addCreds(creds)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FieldsList);
