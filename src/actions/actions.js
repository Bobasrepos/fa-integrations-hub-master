export const userInfo = (user) => ({
    type: 'ADD_USER_INFO',
    user
});

export const loadPage = () => ({
    type: 'LOAD_PAGE',
});

export const loadFields = (fields) => ({
    type: 'LOAD_FIELDS',
    fields
});

export const getFieldsFromFaOnly = (fields) => ({
    type: 'LOAD_FIELDS_FROM_FA',
    fields
});

export const getFieldsFromBackup = (backup) => ({
    type: 'LOAD_FIELDS_FROM_BACKUP',
    backup
});

export const getAllBackupsList = (backups) => ({
    type: 'LOAD_ALL_BACKUPS',
    backups
});

export const toggleRequired = (key) => ({
    type: 'TOGGLE_REQUIRED',
    key
});

export const saveFormHtml = (htmlString) => ({
    type: 'CHANGED_FORM_HTML',
    htmlString
});

export const addCreds = (creds) => ({
    type: 'CREDS_ADDED',
    creds
});

export const changeFormStyle = (styleObj) => ({
    type: 'FORM_CHANGED',
    styleObj
});

export const changeFieldStyle = (styleObj) => ({
    type: 'FORM_FIELD_CHANGED',
    styleObj
});

export const changeLabelStyle = (styleObj) => ({
    type: 'FORM_LABEL_CHANGED',
    styleObj
});

export const changeContainerStyle = (styleObj) => ({
    type: 'FORM_CONTAINER_CHANGED',
    styleObj
});

export const changeSubmitStyle = (styleObj) => ({
    type: 'FORM_SUBMIT_CHANGED',
    styleObj
});

export const changeSubmitContainerStyle = (styleObj) => ({
    type: 'FORM_SUBMIT_CONTAINER_CHANGED',
    styleObj
});

export const changeInputSelectStyle = (styleObj) => ({
    type: 'INPUT_SELECT_CHANGED',
    styleObj
});

export const changeTextBlockStyle = (styleObj) => ({
    type: 'FORM_TEXT_BLOCK_CHANGED',
    styleObj
});

export const changeCheckBoxContainerStyle = (styleObj) => ({
    type: 'FORM_CHECKBOX_CONTAINER_CHANGED',
    styleObj
});

export const changeCheckBoxElementContainerStyle = (styleObj) => ({
    type: 'FORM_CHECKBOX_ELEMENT_CONTAINER_CHANGED',
    styleObj
});

export const changeCheckBoxElementLabelStyle = (styleObj) => ({
    type: 'FORM_CHECKBOX_ELEMENT_LABEL_CHANGED',
    styleObj
});


export const editorChange = (name) => ({
    type: 'EDITOR_CHANGED',
    name
});

export const loadEntityFields = (entityInfo) => ({
    type: 'LOAD_ENTITY',
    entityInfo
});

export const loadSettingsRelations = (data) => ({
    type: 'LOAD_SETTINGS_RELATION',
    data
});

export const relationElementClicked = (relationInfo) => ({
    type: 'MAKE_RELATION',
    relationInfo
});

export const toggleEdit = () => ({
    type: 'TOGGLE_EDIT',
});

export const dataEdited = (changeInfo) => ({
    type: 'DATA_EDITED',
    changeInfo
});

export const loadFormFields = (newOrderFields) => ({
    type: 'LOAD_FORM_FIELDS',
    newOrderFields
});

export const addTextBlock = () => ({
    type: 'ADD_TEXT_BLOCK_TO_FORM',
});

export const addNoteField = () => ({
    type: 'ADD_NOTE_FIELD_TO_FORM',
});

export const removeAnOption = (data) => ({
    type: 'REMOVE_AN_OPTION',
    data
});

export const optionEdited = (changeInfo) => ({
    type: 'OPTION_EDITED',
    changeInfo
});

export const optionAdded = (changeInfo) => ({
    type: 'OPTION_ADDED',
    changeInfo
});

export const openOptionsModal = (modalData) => ({
    type: 'OPTION_MODAL_OPEN',
    modalData
});

export const closeOptionsModal = () => ({
    type: 'CLOSE_OPTION_MODAL'
});

export const loadSelectedStyle = (styleInfo) => ({
    type: 'LOAD_SELECTED_STYLE',
    styleInfo
});

export const addBackupItem = (backup) => ({
    type: 'ADD_BACKUP_ITEM',
    backup
});

export const removeBackupItem = (backupId) => ({
    type: 'REMOVE_BACKUP_ITEM',
    backupId
});

export const defaultChanged = (backupInfo) => ({
    type: 'DEFAULT_CHANGED',
    backupInfo
});

export const onParentChange = (changeData) => ({
    type: 'PARENT_CHANGED',
    changeData
});











