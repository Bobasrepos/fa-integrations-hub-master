import axios from "axios";
import { GraphQLClient } from "graphql-request";
import Hashids from "hashids";
import db, { storageRef } from "./DB/db";
import url from './constants';

let getFaFields = async ( clientId, clientSecret, entityName = null, queryString = null) => {
    const options = {
        method: 'GET',
        headers: {'content-type': 'application/json'},
        data: `{"clientId":"${clientId}","clientSecret":"${clientSecret}"}`,
        url: `${url}/getFields`
    };
    return axios(options)
};



const hashIds = new Hashids('FreeAgentRocks');

let getHashedId = (clientId) => {
    return hashIds.encodeHex(clientId.split('-').join(''));
};

let recreateClientId = (encodedHex) => {
    let decodeHex = hashIds.decodeHex(encodedHex);
    let array = decodeHex.split('');
    array.splice(8, 0, '-');
    array.splice(13, 0, '-');
    array.splice(18, 0, '-');
    array.splice(23, 0, '-');
    return array.join('').toUpperCase();
};

let getSavedVersionsDocs = async (clientId) => {
    let id = getHashedId(clientId);
    let docsArray = [];
    let snapshotData = await db.collection('formBackups').doc(id).collection('savedVersions').get();
    await snapshotData.forEach(doc => {
        docsArray = [...docsArray, {[doc.id]:doc.data()}];
    });
    return docsArray;
};

let getForm = async (clientId, path='formBackups') => {
    let id = getHashedId(clientId);
    let doc = await db.doc(path).doc(id).get();
    return doc.data();
};

let getCollectionDocs = async (collectionRef) => {
    let docsArray = [];
    let snapshotData = await db.collection(collectionRef).get();
    await snapshotData.forEach(doc => {
        docsArray = [...docsArray, {[doc.id]:doc.data()}];
    });
    return docsArray;
};

let getDoc = async (docRef) => {
    let doc = await db.doc(docRef).get();
    return doc.data();
};

let deleteDoc = (docRef) => {
     db.doc(docRef).delete().then(r => r).catch(e => console.error(e));
};

let download = (filename, finalHtml) => {
    let element = document.createElement('a');
    element.setAttribute('href', 'data:html/plain;charset=utf-8,' + encodeURIComponent(finalHtml));
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
};

let downloadCode = (formCode, docId = 'index') => {
    let finalHtml = `
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <title>Form</title>
                </head>
                <body style="padding: 0; margin: 0; width: 100vw; height: 100vh; box-sizing: border-box">
                    ${formCode}
                </body>
                </html>`;
    let filename = `${docId}.html`;
    download(filename, finalHtml);
};

let copyToClipboard = (str) => {
    const el = document.createElement('textarea');  // Create a <textarea> element
    el.value = str;                                 // Set its value to the string that you want copied
    el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
    el.style.position = 'absolute';
    el.style.left = '-9999px';                      // Move outside the screen to make it invisible
    document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
    const selected =
        document.getSelection().rangeCount > 0        // Check if there is any content selected previously
            ? document.getSelection().getRangeAt(0)     // Store selection if found
            : false;                                    // Mark as false to know no selection existed before
    el.select();                                    // Select the <textarea> content
    document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
    document.body.removeChild(el);                  // Remove the <textarea> element
    if (selected) {                                 // If a selection existed before copying
        document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
        document.getSelection().addRange(selected);   // Restore the original selection
    }
};


let saveAs = (blob, fileName) => {
    if (typeof navigator.msSaveOrOpenBlob !== 'undefined') {
        return navigator.msSaveOrOpenBlob(blob, fileName);
    } else if (typeof navigator.msSaveBlob !== 'undefined') {
        return navigator.msSaveBlob(blob, fileName);
    } else {
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = fileName;
        elem.style = 'display:none;opacity:0;color:transparent;';
        (document.body || document.documentElement).appendChild(elem);
        if (typeof elem.click === 'function') {
            elem.click();
        } else {
            elem.target = '_blank';
            elem.dispatchEvent(new MouseEvent('click', {
                view: window,
                bubbles: true,
                cancelable: true
            }));
        }
        URL.revokeObjectURL(elem.href);
    }
}


let exportToCsv = (filename, rows) => {
    var processRow = function (row) {
        var finalVal = '';
        for (var j = 0; j < row.length; j++) {
            var innerValue = row[j] === null ? '' : row[j].toString();
            if (row[j] instanceof Date) {
                innerValue = row[j].toLocaleString();
            };
            var result = innerValue.replace(/"/g, '""');
            if (result.search(/("|,|\n)/g) >= 0)
                result = '"' + result + '"';
            if (j > 0)
                finalVal += ',';
            finalVal += result;
        }
        return finalVal + '\n';
    };

    var csvFile = '';
    for (var i = 0; i < rows.length; i++) {
        csvFile += processRow(rows[i]);
    }

    var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}


export {
    getFaFields,
    getHashedId,
    recreateClientId,
    getSavedVersionsDocs ,
    getForm,
    getCollectionDocs,
    getDoc,
    deleteDoc,
    downloadCode,
    saveAs,
    copyToClipboard,
    exportToCsv
};

