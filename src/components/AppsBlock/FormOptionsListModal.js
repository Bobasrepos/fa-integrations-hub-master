import React, { FC, useState, useEffect } from "react";
import {connect} from 'react-redux';
import {optionAdded, optionEdited, closeOptionsModal} from "../../actions/actions";


export const FormOptionsListModal = props => {
   let [choices, setChoices] = useState([]);

    let optionsRendered = [];
    let modalData = props.optionsModalData || {};
    let {entity,key, label, fieldType, leftOffset,topOffset} = modalData;

    useEffect(() => {
        setChoices(props.optionsModalChoices)
    });


    let addOption = (e) => {
        props.optionAdded({entity, key, value: 'New Option' })
    };

    let removeOption = (e) => {
        let value = e.target.dataset.value;
        let name = 'choices';
        props.optionEdited({entity, key , name, value })
    };

    let onOptionLabelChange = (e) => {
        let name = 'choices';
        let value = e.target.value;
        let optionValue = e.target.dataset.value;
        props.optionEdited({name, value, key, entity, optionValue, editingLabel: true})
    };

    let closeModal = (e) => {
        console.log(e.target.dataset.name);
       if (e.target.dataset.name === "container") {
           props.closeOptionsModal()
       }
    };

    if (props && choices) {
        optionsRendered = choices.map((choice,i) => {
            return (<span key={`${key}${i}options`} className="sortableHelper" data-value={choice.value}>
                <input value={choice.label} data-value={choice.value} onChange={e => onOptionLabelChange(e)}/>
                <span className="options-editor-remove-option"
                      data-value={choice.value}
                      onClick={e => removeOption(e)}
                >x</span>
            </span>)
        })
    }



    return (
        <div className='options-modal-all-page-container' data-name="container" onClick={e => closeModal(e)}>
            <div className="options-editor-absolute-position" style={{left: leftOffset, top: (topOffset + 40)}}>
                {optionsRendered}
                <span key={`add-option-button`} className="options-editor-add-option-button" onClick={e => addOption(e)}>+</span>
            </div>
        </div>);
    };

const mapStateToProps = (state) => ({
    optionsModalIsOpen: state.values.optionsModalIsOpen,
    optionsModalData: state.values.optionsModalData,
    optionsModalChoices: state.values.optionsModalChoices,
});

const mapDispatchToProps = (dispatch) => ({
    optionEdited:(changeInfo) => dispatch(optionEdited(changeInfo)),
    optionAdded:(changeInfo) => dispatch(optionAdded(changeInfo)),
    closeOptionsModal:() => dispatch(closeOptionsModal()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormOptionsListModal);
