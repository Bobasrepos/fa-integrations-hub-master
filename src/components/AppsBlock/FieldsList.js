import React, {Component} from 'react';
import {connect} from 'react-redux';
import FieldsListRow from './FieldsListRow';
import FieldsEditingRow from './FieldsEditingRow';
import EntityRelations from './EntityRelations';
import FormEditing from './FormEditing';
import {saveFormHtml, toggleRequired, loadEntityFields, loadSettingsRelations} from '../../actions/actions';

class FieldsList extends Component {

    componentDidMount() {
        this.props.saveFormHtml(document.querySelector('.generated-form-container').outerHTML);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.formOuterHtml !== this.props.formOuterHtml ||
            prevProps.selectionList !== this.props.selectionList) {
            this.props.saveFormHtml(document.querySelector('.generated-form-container').outerHTML);
        }
    }

    toggleField(e) {
        e.preventDefault();
        this.props.toggleRequired(e.currentTarget.dataset.key);
        let form = document.querySelector('.generated-form');
        if (form) {
            this.props.saveFormHtml(document.querySelector('.generated-form').outerHTML);
        }
    }

    loadEntityFields(e) {
        e.preventDefault();
        let text = e.currentTarget.dataset.text;
        let index = e.currentTarget.dataset.index;
        this.props.loadEntityFields({text, index});
    }

    loadSettingsRelationsClicked(e) {
        e.preventDefault();
        this.props.loadSettingsRelations(e.target.dataset.name);
    }


     render() {
        let list = this.props.selectionList;
        let renderedList = [];
        if (list && list.length > 0) {
            if (!this.props.editingIsOn) {
            renderedList = list.map((fieldInfo,i) => {
                return ( <li onClick={(e) => this.toggleField(e)}
                             data-key={fieldInfo.key}
                             data-label={fieldInfo.label}
                             data-eltype={fieldInfo.type}
                             data-entity={fieldInfo.entity}
                             data-userreq={fieldInfo.userRequired}
                             data-order={i}
                             className={fieldInfo.selected ? 'field_selected' : 'field_not_selected'}
                             key={`${fieldInfo.key}${fieldInfo.entity}selectionli`}
                >
                    <FieldsListRow field={fieldInfo} index={i} key={`${fieldInfo.key}${fieldInfo.entity}row`}/>
                </li>)
            })
            } else {
                renderedList = list.map((fieldInfo,i) => {
                    return ( <li className={'field_not_selected'}
                                 key={fieldInfo.key}
                    >
                        <FieldsEditingRow field={fieldInfo} index={i} key={`${fieldInfo.key}${fieldInfo.entity}editing`}/>
                    </li>)
                })
            }
        }

        let entitiesMenu = [];
        if (this.props.entitiesMenu) {
            entitiesMenu = this.props.entitiesMenu.map(entity => (<span
                data-index={entity.index}
                data-text={entity.text}
                key={entity.index}
                onClick={e => this.loadEntityFields(e)}
                className={this.props.activeData && entity.text === this.props.activeData ? "selected-data" : ""}
            >{entity.text}</span>))
        }

         let activeData = this.props && this.props.activeData ? this.props.activeData : 'contact';



        return (
            <>
                { (activeData !== 'relations' && activeData !== 'editing' && activeData !== 'settings') && <ul id="fieldsList">
                    <li className="column-labels">
                        <span>#</span>
                        <span>Field</span>
                        <span>Type</span>
                        <span>FA Req</span>
                    </li>
                    { renderedList }
                </ul>
                }
                { activeData === 'relations' && <EntityRelations /> }
                { activeData === 'editing' && <FormEditing /> }
            </>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.values.user,
    fieldsFromFa: state.values.fieldsFromFa,
    editedFields: state.values.editedFields,
    selectionList: state.values.selectionList,
    entitiesMenu: state.values.entitiesMenu,
    activeData: state.values.activeData,
    completeRelations: state.values.completeRelations,
    editingIsOn: state.values.editingIsOn
});

const mapDispatchToProps = (dispatch) => ({
    toggleRequired: (key) => dispatch(toggleRequired(key)),
    saveFormHtml: (htmlString) => dispatch(saveFormHtml(htmlString)),
    loadEntityFields: (entityInfo) => dispatch(loadEntityFields(entityInfo)),
    loadSettingsRelations: (data) => dispatch(loadSettingsRelations(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(FieldsList);
