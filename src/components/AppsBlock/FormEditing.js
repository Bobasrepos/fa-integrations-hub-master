import React, { FC, useState, useEffect } from "react";
import {connect} from 'react-redux';
import styled from 'styled-components';
import  { ReactSortable } from 'react-sortablejs'
import FormEditingRow from './FormEditingRow';

import { loadFormFields,dataEdited,addTextBlock, addNoteField } from '../../actions/actions';


const FormSettings = props => {
    let LabelRow = styled.li`
        display: grid;
        grid-template-columns: 2fr 3fr 2fr 3fr 2fr 2fr 2fr;
        grid-template-rows: minmax(45px, 50px);
        justify-content: center;
        align-items: center;
        width: 98%;
        height: auto;
        font-size: .9em;
        font-weight: 500;
        padding: 5px 10px;
        margin-bottom: 2px;
        margin-top: 2px;
        margin-left: 1%;
        color: #3d4754;
        border-bottom: 1px solid rgba(0,0,0,0.1);
        span {
            border-right: 1px solid rgba(0,0,0,0.1);
            color: #3d4754;
            font-size: 1.1em;
            font-weight: 500;
            &:last-child {
                border-right: 0;
            }
        }
`;

    let addTextBlock = () => {
        props.addTextBlock();
    };

    let addNoteField = () => {
        props.addNoteField();
    };


    let formFields = [];
    let submitField = props.submitButton[0];
    if (props.formFields && props.formFields.length > 0) {
        formFields = props.formFields.map((field,i) => (<FormEditingRow field={field} index={i} key={`${field.entity}${field.key}settings`} />));
    }


    return (<ul className="form-field-settings-container">
                <LabelRow className="column-labels">
                    <span>Parent</span>
                    <span>Label</span>
                    <span>Type</span>
                    <span>Options</span>
                    <span>User Req</span>
                    <span>Find</span>
                    <span>Position</span>
                </LabelRow>
                <ReactSortable list={props.formFields ||[]} setList={props.loadFormFields}>
                    {formFields}
                </ReactSortable>
                <FormEditingRow field={submitField} index={999} key={`${submitField.entity}${submitField.key}settings`} />
        <li className="settings-add-text-block">
            <span data-name="text" onClick={() => addTextBlock()}>Add Text Block</span>
        </li>
        </ul>)
};



const mapStateToProps = (state) => ({
    user: state.values.user,
    fieldsFromFa: state.values.fieldsFromFa,
    selectionList: state.values.selectionList,
    formFields: state.values.formFields,
    submitButton: state.values.submitButton,
    entitiesMenu: state.values.entitiesMenu,
    activeData: state.values.activeData,
    relationsList: state.values.relationsList
});

const mapDispatchToProps = (dispatch) => ({
    loadFormFields: (newOrderFields) => dispatch(loadFormFields(newOrderFields)),
    dataEdited:(changeInfo) => dispatch(dataEdited(changeInfo)),
    addTextBlock: () => dispatch(addTextBlock()),
    addNoteField: () => dispatch(addNoteField())
});

export default connect(mapStateToProps, mapDispatchToProps)(FormSettings);
