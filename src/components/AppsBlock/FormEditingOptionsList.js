import React, { FC, useState, useEffect } from "react";
import {connect} from 'react-redux';
import {optionEdited, optionAdded, dataEdited, removeAnOption} from '../../actions/actions';


export const FormSettingsOptionsList = props => {

    let { entity , choices } = props.field;
    let fieldKey = props.fieldKey;


    let numberOfChoices = 0;

    if (props && choices) {
        numberOfChoices = choices.length;
    }


    return ( <div key={`${fieldKey}${entity}options-container`} className="options-editor-container">
            <div className="options-editor-label"
                 onClick={(e) => props.toggleEditOptionsModal(e)}>
                {numberOfChoices} options</div>
    </div>);
};

const mapStateToProps = (state) => ({
    user: state.values.user,
    fieldsFromFa: state.values.fieldsFromFa,
    selectionList: state.values.selectionList,
    formFields: state.values.formFields,
    entitiesMenu: state.values.entitiesMenu,
    activeData: state.values.activeData,
    relationsList: state.values.relationsList,
    refersh: state.values.refresh,
});

const mapDispatchToProps = (dispatch) => ({
    optionEdited:(changeInfo) => dispatch(optionEdited(changeInfo)),
    optionAdded:(changeInfo) => dispatch(optionAdded(changeInfo)),
    dataEdited:(changeInfo) => dispatch(dataEdited(changeInfo)),
    removeAnOption:(data) => dispatch(removeAnOption(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormSettingsOptionsList);
