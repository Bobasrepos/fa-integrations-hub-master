import React, {Component} from 'react';
import {connect} from 'react-redux';
import { relationElementClicked } from '../../actions/actions';


class EntityColumn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            key: '',
            keydown: false
        };
    }

    relationsElementClick(e) {
        let { key,label ,entity } = e.currentTarget.dataset;
        if (!this.props.tempRelation || (this.props.tempRelation && this.props.tempRelation.entity !== entity)) {
            let parentContainer = document.querySelector('.relations-editor-columns-container');
            function offset(el) {
                let rect = el.getBoundingClientRect(),
                    scrollLeft = window.pageXOffset || parentContainer.scrollLeft,
                    scrollTop = window.pageYOffset || parentContainer.scrollTop;
                return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
            }
            function parentOffset(el) {
                let rect = el.getBoundingClientRect(),
                    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
                    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
                return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
            }
            if (e.shiftKey || e.altKey || e.ctrlKey) {

                let divOffset = offset(e.currentTarget);
                let parentDoc = parentOffset(parentContainer);
                let leftOffset = divOffset.left - parentDoc.left;
                let topOffset = divOffset.top - parentDoc.top;
                let computedStyle = window.getComputedStyle(e.currentTarget);
                let width = parseFloat(computedStyle.getPropertyValue('width'));
                let height = parseFloat(computedStyle.getPropertyValue('height'));

                let relationElement = {leftOffset, topOffset, width, height, key,label , entity };
                this.props.relationElementClicked(relationElement);
                if (!e.currentTarget.classList.contains('relation-block-element-selected')) {
                    e.currentTarget.classList.add('relation-block-element-selected')
                }
            } else {
                alert('If you wish to make relation between entities, please hold shift key and click on two elements, that you would like to relate')
            }
        }
    }

    render() {
        let columnList = [];
        if (this.props.entityFields) {
            columnList = this.props.entityFields.map((fieldInfo, i) => {
                let className = '';
                if (this.props && this.props.relationsList && this.props.relationsList.length > 0) {
                    this.props.relationsList.map(relation => {
                        relation.map(relEl => {
                            if (relEl.entity === fieldInfo.entity && relEl.key === fieldInfo.key) {
                                className = 'relation-block-element-selected';
                            }
                        })
                    });
                }

                return (<li className={`${className}`}
                            data-key={fieldInfo.key}
                            data-label={fieldInfo.label}
                            data-entity={fieldInfo.entity}
                            key={`${fieldInfo.key}relations`}
                            onClick={e => this.relationsElementClick(e)}
                >
                    <div className="relation-editor-field-block">
                        <div className="relation-editor-field-label">{fieldInfo.label}</div>
                        <div className="relation-editor-field-key">{fieldInfo.key}</div>
                    </div>
                </li>)
            })
        }

        return (
            <ul className="relations-editor-column">
                {columnList}
            </ul>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.values.user,
    fieldsFromFa: state.values.fieldsFromFa,
    selectionList: state.values.selectionList,
    entitiesMenu: state.values.entitiesMenu,
    activeData: state.values.activeData,
    relationsList: state.values.relationsList,
    tempRelation: state.values.tempRelation
});

const mapDispatchToProps = (dispatch) => ({
    relationElementClicked: (relationInfo) => dispatch(relationElementClicked(relationInfo))
});

export default connect(mapStateToProps, mapDispatchToProps)(EntityColumn);
