import React, { FC, useState, useEffect } from "react";
import {connect} from 'react-redux';
import FormEditingOptionsList from './FormEditingOptionsList';
import { loadFormFields,dataEdited,removeAnOption, openOptionsModal, onParentChange } from '../../actions/actions';
import EntitiesList from "./EntitiesList";
import styled from "styled-components";


export const FormEditingRow = props => {
    let [editOption, setEditOptions] = useState(false);
    let [elLeft, setLeft] = useState(0);
    let [elTop, setTop] = useState(0);

    let field = props.field;
    let { entity,key, label, fieldType, userRequired, choices} = field;
    let fieldWidth = field.width || '100%';

    let onInputChange = (e) => {
        let name = e.target.name || e.target.dataset.name;
        let value = e.target.value;
        let key = field.key;
        let entity = field.entity;
        props.dataEdited({name, value, key, entity})
    };

    let onParentChange = (e) => {
        let name = e.target.name || e.target.dataset.name;
        let value = e.target.value;
        let key = field.key;
        let entity = field.entity;
        props.onParentChange({name, value, key, entity})
    };

    let toggleEditOptionsModal = (e, data) => {
        function offset(el) {
            let rect = el.getBoundingClientRect(),
                scrollLeft = window.pageXOffset || el.parentElement.scrollLeft,
                scrollTop = window.pageYOffset || el.parentElement.scrollTop;
            return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
        }

            let divOffset = offset(e.target);

            let leftOffset = divOffset.left;
            let topOffset = divOffset.top;

            let modalData = {entity,key, label, fieldType, choices, leftOffset,topOffset};

           props.openOptionsModal(modalData);
    };


      let position = {elLeft,elTop};
    return (<li data-entity={entity}
                data-key={key}
                key={`${entity}${key}settings-row`}
    >
        <EntitiesList field={field} onParentChange={onParentChange}/>
        <input className="fields-list-name-label" value={label || ''} name="label" data-key={key} data-entity={entity}  onChange={e => onInputChange(e)}/>
        <select value={fieldType} data-key={key} data-entity={entity} name="fieldType"
                onChange={e => onInputChange(e)}>
            <option value='input'>Input</option>
            <option value='select'>Select</option>
            <option value='checkboxes'>Checkbox</option>
            <option value='div'>Div</option>
            <option value='textarea'>Textarea</option>
            <option value='button'>Button</option>
        </select>
        <FormEditingOptionsList choices={choices} position={position} toggleEditOptionsModal={toggleEditOptionsModal} editOption={editOption} fieldKey={field.key} field={field}/>
        <select value={userRequired} data-key={key} data-entity={entity} name="userRequired"
                onChange={e => onInputChange(e)}>
            <option value='no'>No</option>
            <option value='yes'>Yes</option>
        </select>
        <select data-key={key} data-entity={entity} name="searchFor"
                onChange={e => onInputChange(e)}>
            <option value='no'>No</option>
            <option value='yes'>Yes</option>
        </select>
        <select value={fieldWidth} data-key={key} data-entity={entity} name="width"
                onChange={e => onInputChange(e)}>
            <option value='100%'>100%</option>
            <option value='50%_Left'>50% Left</option>
            <option value='50%_Right'>50% Right</option>
        </select>
    </li>);

};

const mapStateToProps = (state) => ({
    user: state.values.user,
    fieldsFromFa: state.values.fieldsFromFa,
    selectionList: state.values.selectionList,
    formFields: state.values.formFields,
    entitiesMenu: state.values.entitiesMenu,
    activeData: state.values.activeData,
    relationsList: state.values.relationsList
});

const mapDispatchToProps = (dispatch) => ({
    loadFormFields: (newOrderFields) => dispatch(loadFormFields(newOrderFields)),
    dataEdited:(changeInfo) => dispatch(dataEdited(changeInfo)),
    removeAnOption:(data) => dispatch(removeAnOption(data)),
    openOptionsModal:(modalData) => dispatch(openOptionsModal(modalData)),
    onParentChange: (changeData) => dispatch(onParentChange(changeData))

});

export default connect(mapStateToProps, mapDispatchToProps)(FormEditingRow);
