import React from "react";
import {connect} from "react-redux";
import styled from 'styled-components';
import {} from "../../actions/actions";


let Container = styled.select`
     display: flex;
     justify-content: flex-start;
     align-items: flex-start;
      text-transform: capitalize;
`;

let Option = styled.option`
        text-transform: capitalize;
`;

let EntitiesList = props => {

    let entitiesMenu = [];
    if (props.entitiesMenu) {
        entitiesMenu = props.entitiesMenu.map(entity => (<Option
            data-index={entity.index}
            data-text={entity.text}
            key={entity.index}
        >{entity.text}</Option>))
    }

    return (<Container onChange={e => props.onParentChange(e)}
                       name="entity"
                       value={props.field.parent}>
        <Option>None</Option>
        {entitiesMenu}
    </Container>)
};

const mapStateToProps = (state) => ({
    entitiesMenu: state.values.entitiesMenu,
    activeData: state.values.activeData,
    activeEditor: state.values.activeEditor,
    creds: state.values.creds,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(EntitiesList);
