import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Line} from "react-lineto";
import EntityColumn from "./EntityColumn";

class EntityRelations extends Component {
    constructor(props) {
        super(props);
        this.state = {
            domReady: false
        };
    }

    componentDidMount() {
        this.setState({domReady : true})
    }

    render() {
        let relationsLines = [];

        let props = this.props;
        if (this.state.domReady && props && props.relationsList && props.relationsList.length > 0) {
            relationsLines = props.relationsList.map(relation => {
                let relElOneLeft = relation[0].leftOffset < relation[1].leftOffset ? relation[0].leftOffset + relation[0].width - 2 : relation[0].leftOffset + 2;
                let relElOneTop = relation[0].topOffset + ( relation[0].height / 2 );
                let relELTwoLeft = relation[0].leftOffset < relation[1].leftOffset ? relation[1].leftOffset + 2 : relation[1].leftOffset + relation[1].width - 2;
                let relELTwoTop = relation[1].topOffset + ( relation[1].height / 2 );

                return (<Line key={`${relELTwoLeft}${relElOneTop}`} borderWidth={3} within="relations-editor-columns-container" borderColor="RGBA(61, 76, 96, .4)" x0={relElOneLeft} y0={relElOneTop} x1={relELTwoLeft} y1={relELTwoTop} />);
            });
        }


        let activeData = this.props && this.props.activeData ? this.props.activeData : 'contact';

        let relationsList = [];
        if (activeData === 'relations') {
            relationsList = this.props.fieldsFromFa.map(entityFields => {
                let key = Object.keys(entityFields)[0];
                return (<EntityColumn entityFields={entityFields[key]} key={key} />);
            });
        }
        let entitiesMenu = [];

        if (this.props.entitiesMenu) {
            entitiesMenu = this.props.entitiesMenu.map(entity => (<span
                data-index={entity.index}
                data-text={entity.text}
                key={entity.index}
                className={this.props.activeData && entity.text === this.props.activeData ? "selected-data" : ""}
            >{entity.text}</span>))
        }

        return (<div className="relations-editor-page-container">
                <div className="relations-editor-entities-menu">{entitiesMenu}</div>
                <div className="relations-editor-columns-container">
                    { relationsList }
                    { relationsLines }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.values.user,
    fieldsFromFa: state.values.fieldsFromFa,
    selectionList: state.values.selectionList,
    entitiesMenu: state.values.entitiesMenu,
    activeData: state.values.activeData,
    relationsList: state.values.relationsList
});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(EntityRelations);
