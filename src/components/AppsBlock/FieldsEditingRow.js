import React, {Component} from 'react';
import {connect} from 'react-redux';
import { dataEdited } from "../../actions/actions";



class FieldsEditingRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            key: '',
            label: '',
            type: '',
            required: false,
            userRequired: false
        };
    }

    componentDidMount() {
        this.setState({...this.props.field})
    }

    onInputChange(e) {
        let name = e.target.name;
        let value = e.target.value;
        let key = this.props.field.key;
        let entity = this.props.activeData;
        this.setState({[e.target.name]: e.target.value});
        this.props.dataEdited({name, value, key, entity})
    }

    render() {
        let fieldInfo = this.props.field;
        console.log(fieldInfo);
        let key = fieldInfo && fieldInfo.key ? fieldInfo.key : '';
        let label = fieldInfo && fieldInfo.label ? fieldInfo.label : '';
        let type = fieldInfo && fieldInfo.type ? fieldInfo.type : '';

        let required = fieldInfo && fieldInfo.required ? 'Yes' : 'No';
        return (
           <>
                <span>{this.props.index + 1}</span>
                <div className="fields-list-name">
                    <input className="fields-list-name-label" value={label} name="label" onChange={e => this.onInputChange(e)}/>
                     <span className="fields-list-name-key" >{key}</span>
                </div>
                <span>{type}</span>
               <span>{required}</span>
            </>
        );
    }
}

const mapStateToProps = (state) => ({
    activeData: state.values.activeData,
});

const mapDispatchToProps = (dispatch) => ({
    dataEdited:(changeInfo) => dispatch(dataEdited(changeInfo))
});

export default connect(mapStateToProps, mapDispatchToProps)(FieldsEditingRow);
