import React, {Component} from 'react';
import {connect} from 'react-redux';
import { toggleRequired } from "../../actions/actions";


class FieldsListRow extends Component {
    render() {
        let fieldInfo = this.props.field;
        let key = fieldInfo ? fieldInfo.key : '';
        let label = fieldInfo ? fieldInfo.label : '';
        let required = fieldInfo && fieldInfo.required ? 'Yes' : 'No';

        let type = fieldInfo ? fieldInfo.type : '';

        return (
           <>
                <span>{this.props.index + 1}</span>
                <div className="fields-list-name">
                    <span className="fields-list-name-label">{label}</span>
                    <span className="fields-list-name-key">{key}</span>
                </div>
                <span className="text-capitalize">{type}</span>
                <span className="text-capitalize">{required}</span>
            </>
        );
    }
}

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(FieldsListRow);
