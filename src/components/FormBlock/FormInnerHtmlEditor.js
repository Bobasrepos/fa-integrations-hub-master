import React, {Component} from 'react';
import {connect} from 'react-redux';
import { saveFormHtml } from "../../actions/actions";


class FormFieldGenerator extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    onTextAreaChange(e) {
        this.props.saveFormHtml(e.target.value);
        document.querySelector('.generated-form').innerHTML = e.target.value;
    }

    render() {
        return <textarea className="code-editor" value={this.props.formOuterHtml} onChange={(e) => this.onTextAreaChange(e)}/>
    }
}

const mapStateToProps = (state) => ({
    selectionList: state.values.selectionList,
    formOuterHtml: state.values.formOuterHtml
});

const mapDispatchToProps = (dispatch) => ({
    saveFormHtml: (htmlString) => dispatch(saveFormHtml(htmlString))
});

export default connect(mapStateToProps, mapDispatchToProps)(FormFieldGenerator);
