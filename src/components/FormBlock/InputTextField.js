import React from 'react';
import {createMuiTheme, makeStyles, ThemeProvider} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import {blue, red} from "@material-ui/core/colors";


let InputTextField = (props) => {
    let field = props.field;
    let isRequired = !!(field.required || (field.userRequired && field.userRequired === 'yes'));
    let gridColumn = '1/-1';
    if (field && field.width && field.width.split('_')[0] === '50%') {
        gridColumn = field.width.split('_')[1] === 'Left' ? '1/2' : '2/-1';
    }
    let placeholderText = field.label || null;
    let searchFor = field.searchFor || false;

    let dataProps = {
        placeholder: placeholderText,
        'data-search': searchFor,
        'data-required': isRequired,
        'data-key': field.key,
        'data-label': field.label,
        'data-entity': field.entity,
        'data-form': 'value',
        type:field.type
    };

    const useStyles = makeStyles((theme) => ({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
            gridColumn,
            width:'100%',
            '& label': {
                fontSize: '1.1em',
                backgroundColor: 'white',
                marginLeft: 0,
                paddingRight: '7px',
                paddingLeft: '2px'
            },
            '& input': {
                ...props.styles,
                paddingBottom: '10px',
                height: '100%'
            }
        }
    }));

    const classes = useStyles();



    const theme = createMuiTheme({
        palette: {
            primary: blue,
        },
    });

    return ( <ThemeProvider theme={theme}>
                <TextField
                    className={classes.root}
                    label={placeholderText}
                    variant="outlined"
                    required={isRequired}
                    type={field.type}
                    id="mui-theme-provider-outlined-input"
                    inputProps={dataProps}
                />
        </ThemeProvider>
    )
};

export default InputTextField;
