import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';



let SelectField = (props) => {
    const [value, setValue] = React.useState('');

    let field = props.field;

    let gridColumn = '1/-1';
    if (field && field.width && field.width.split('_')[0] === '50%') {
        gridColumn = field.width.split('_')[1] === 'Left' ? '1/2' : '2/-1';
    }

    const useStyles = makeStyles((theme) => ({
        formControl: {
            display: "flex",
            flexWrap: "wrap",
            minWidth: 120,
            width: '100%',
            height: '100%',
            gridColumn,
            '& label': {
                fontSize: '1.1em',
                backgroundColor: 'white',
                marginLeft: 0,
                paddingRight: '7px',
                paddingLeft: '2px'
            },
            '& select': {
                fontSize: '1.4em',
                backgroundColor: 'white',
                color: 'rgba(100,100,100,1)',
            },
        },
    }));

    const classes = useStyles();

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    let options = field.choices.map(choice => (
        <option value={choice.value} key={choice.value}>{choice.label}</option>));
    return (

            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="outlined-age-native-simple">{field.label}</InputLabel>
                <Select
                    native
                    value={value}
                    onChange={handleChange}
                    label={field.label}
                    inputProps={{
                        name: field.key,
                        id: 'outlined-age-native-simple',
                    }}
                >
                    {options}
                </Select>
            </FormControl>

    );
}

export default SelectField;
