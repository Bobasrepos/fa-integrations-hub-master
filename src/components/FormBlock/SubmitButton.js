import React from "react";
import {connect} from "react-redux";
import url from '../../constants';


let SubmitButton = props => {
    let field = props.submitButton[0];
    let gridColumn = '1/-1';
    if (field && field.width && field.width.split('_')[0] === '50%') {
        gridColumn = field.width.split('_')[1] === 'Left' ? '1/2' : '2/-1';
    }

    let submitFormFunc = (e, field, props) => {
        let localFuncUrl = `${url}/processForm`;
        let relations = props && props.relationsList ? props.relationsList : null;
        e.preventDefault();
        let fieldValueFields = document.querySelectorAll('[data-form="value"]');
        let data = {relations: relations};
        let formIsValid = true;
        fieldValueFields.forEach(field => {
            let {key, entity, search, required, parent} = field.dataset;
            let requiredField = JSON.parse(required);
            let value = field.value;
            if (parent && parent !== 'None') {
                if (!data.parent || !data.parent[parent]) {
                    data.parent = {[parent]: {children: {[entity]: [[key,value]]}}}
                } else {
                        data.parent = {
                            [parent]: {children: {[entity]: [...data.parent[parent].children[entity], [key, value]]}}
                        }
                }
            } else {
                if (!requiredField || (requiredField && value !== '')) {
                    if (field.type === 'date') {
                        let date = new Date(value);
                        value = date.toISOString();
                    }
                    if (search === 'yes') {
                        if (!data.search) {
                            data.search = [{key, value, entity}];
                        } else {
                            data.search = [...data.search, {key, value, entity}];
                        }
                    }
                    if (!data[entity]) {
                        data[entity] = {[key]: value};
                    } else {
                        if (field.type === 'checkbox' && field.checked) {
                            if (data[entity][key]) {
                                data[entity][key] = `${data[entity][key]},${value}`;
                            } else {
                                data[entity][key] = value;
                            }
                        } else {
                            data[entity][key] = value;
                        }
                    }
                } else {
                    if (requiredField && value === '') {
                        field.style.border = "1px solid rgba(255,0,0,0.5)";
                        field.style.boxShadow = "0px 0px 5px 2px rgba(255,0,0,0.3)";
                        formIsValid = false;
                    }
                }
            }
        });
        console.log({data});
        if (formIsValid) {
            let url = localFuncUrl;
            let fetchOptions = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'x-client-id': props.clientId,
                    'x-secret': props.secret
                },
                body: JSON.stringify(data)
            };
            fetch(url, fetchOptions)
                .then(response => {
                    if (response.ok) {
                        fieldValueFields.forEach(field => {
                            field.value = '';
                        });
                    } else {
                        alert('Error')
                    }
                    return  response.json()
                })
                .then(data => console.log(data))
                .catch(e => console.error(e));
        }
    };

    return(<div style={{...props.formStylesSubmitContainer}}>
            <button onClick={e => submitFormFunc(e, field, props)} type="button" id="submit_generated_form_button" style={{...props.formStylesSubmitButton, gridColumn}}>{field.label}</button>
        </div>);
};

const mapStateToProps = (state) => ({
    formFields: state.values.formFields,
    submitButton: state.values.submitButton,
    formContainerStyles: state.container,
    formCheckboxContainerStyles: state.checkbox,
    formCheckboxElementLabelStyles: state.checkboxElementLabel,
    formStyles: state.form,
    formStylesDiv: state.field,
    formStylesInputSelect: state.inputselect,
    formStylesLabel: state.label,
    formStylesSubmitButton: state.submit,
    formStylesSubmitContainer: state.submitContainer,
    clientId: state.values.creds.clientIdValue,
    secret: state.values.creds.secretValue,
    relationsList: state.values.relationsList,
    selectedStyle:state.values.selectedStyle,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(SubmitButton);
