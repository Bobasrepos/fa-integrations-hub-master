import React, {Component} from 'react';
import {connect} from 'react-redux';
import GeneratedLabelFields from './GeneratedLabelFields';
import GeneratedPlaceholderFields from './GeneratedPlaceholderFields';
import GeneratedModernFields from './GeneratedModernFields';
import SubmitButton from './SubmitButton';
import { saveFormHtml } from '../../actions/actions';
import url from '../../constants';


class FormFieldGenerator extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        this.addFuncToScript = this.addFuncToScript.bind(this);
    }


    addFuncToScript() {
        let production =  `${url}/createEntitiesHook`;
        let relations = this.props && this.props.relationsList && this.props.relationsList.length > 0 ?  this.props.relationsList : null;
        let data = relations ? JSON.stringify({relations: relations}) : JSON.stringify({relations : null});
        let func = `document.querySelector('#submit_generated_form_button').addEventListener('click', (e) => {
                e.preventDefault();
            let fieldValueFields = document.querySelectorAll('[data-form="value"]');
            let data = ${data};
            let formIsValid = true;
            fieldValueFields.forEach(field => {
                let {key, entity, search, required} = field.dataset;
                let requiredField = JSON.parse(required);
                let value = field.value;
                if (!requiredField || (requiredField && value !== '')) {
                    if (field.type === 'date') {
                        let date = new Date(value);
                        value = date.toISOString();
                    }
                    if (search === 'yes') {
                        if (!data.search) {
                            data.search = [{key, value, entity}];
                        } else {
                            data.search = [...data.search, {key, value, entity}];
                        }
                    }
                    if (!data[entity]) {
                        data[entity] = {[key]: value};
                    } else {
                        if (field.type === 'checkbox' && field.checked) {
                            if (data[entity][key]) {
                                data[entity][key] = data[entity][key] + ',' + value;
                            } else {
                                data[entity][key] = value;
                            }
                        } else {
                            data[entity][key] = value;
                        }
                    }
                } else {
                    if (requiredField && value === '') {
                        field.style.border = "1px solid rgba(255,0,0,0.5)";
                        field.style.boxShadow = "0px 0px 5px 2px rgba(255,0,0,0.3)";
                        formIsValid = false;
                    }
                }
            });
            if (formIsValid) {
                let url = "${production}";
                let fetchOptions = {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json', 'x-client-id': '${this.props.clientId}', 'x-secret': '${this.props.secret}' },
                    body: JSON.stringify(data)
                };
                fetch(url, fetchOptions)
                    .then(response => {
                        if (response.ok) {
                            fieldValueFields.forEach(field => {
                                field.value = '';
                            });
                        } else {
                            alert('Error')
                        }
                        return  response.json()
                    })
                    .then(data => console.log(data))
                    .catch(e => console.error(e));
            }
            })`;

        let scriptEl = document.querySelector('#submit_form_func');
        if (scriptEl) {
            scriptEl.innerHTML = func;
        }
        let importScript = document.querySelector('#font-import');
        if (importScript) {
            importScript.innerHTML= `WebFont.load({
                 google: {
                     families: [  'Arial', 'Times New Roman', 'Helvetica', 'Times', 'Courier New', 'Verdana',
                    'Courier', 'Arial Narrow', 'Candara', 'Geneva', 'Calibri', 'Optima', 'Cambria','Garamond',
                    'Perpetua', 'Didot', 'Lucida Bright', 'Copperplate' , 'Droid Sans', 'Inter', 'Droid Serif', 'Roboto',
                    "Segoe UI", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Helvetica Neue",
                    "Open Sans", "Lato", "Old Standard TT","Abril Fatface", "PT Serif" ,"Vollkorn",
                    "PT Mono","Gravitas One"]
                 }
             });`
        let removeBoxScript = document.querySelector('#remove_box');
            removeBoxScript.innerHTML= `
                let fieldValueFields = document.querySelectorAll('[data-form="value"]');
                fieldValueFields.forEach(field => {
                     field.addEventListener('click', e => {
                            if (e.target.style.boxShadow && e.target.style.boxShadow !== '0px 0px 0px 0px') {
                                e.target.style.boxShadow = "0px 0px 0px 0px";
                                e.target.style.border = "1px solid rgba(183,183,183,1)";
                            }  
                     });
                     field.addEventListener('change', e => {
                           if (e.target.style.boxShadow && e.target.style.boxShadow !== '0px 0px 0px 0px') {
                                e.target.style.boxShadow = "0px 0px 0px 0px";
                                e.target.style.border = "1px solid rgba(183,183,183,1)";
                            }  
                     });
                });  
            `;
        }
    };

    componentDidMount() {
        this.addFuncToScript();
        this.props.saveFormHtml(document.querySelector('.generated-form-container').outerHTML);
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        if ((prevProps.formContainerStyles !== this.props.formContainerStyles) ||
            (prevProps.formStyles !== this.props.formStyles) ||
            (prevProps.formStylesDiv !== this.props.formStylesDiv) ||
            (prevProps.formStylesInputSelect !== this.props.formStylesInputSelect) ||
            (prevProps.formStylesLabel !== this.props.formStylesLabel) ||
            (prevProps.formStylesSubmitButton !== this.props.formStylesSubmitButton) ||
            (prevProps.formContainerStyles !== this.props.formContainerStyles) ||
            (prevProps.relationsList !== this.props.relationsList)) {
            this.addFuncToScript();
            this.props.saveFormHtml(document.querySelector('.generated-form-container').outerHTML);
        }
    }



    render() {
        let props = this.props;
        let formFieldsList = [];
        let activeStyle = this.props.selectedStyle || 'labels';
        if (props.formFields) {
            formFieldsList = props.formFields.map(field => {
                if (activeStyle === 'labels') {
                    return (<GeneratedLabelFields field={field} key={`${field.label}${field.key}field`}/>)
                }
                if (activeStyle === 'placeholders') {
                    return (<GeneratedPlaceholderFields field={field} key={`${field.label}${field.key}field`}/>)
                }
                if (activeStyle === 'modern') {
                    return (<GeneratedModernFields field={field} styles={{color:'#2196f3'}} key={`${field.label}${field.key}field`}/>)
                }
            });
        }

        return (<div className="generated-form-container" style={this.props.formContainerStyles}>
            <form className="generated-form" style={this.props.formStyles}>
                {formFieldsList}
                <SubmitButton />
                <script id="submit_form_func" />
                <script id="remove_box" />
                <script src="http://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js"></script>
                <script id="font-import" />
            </form>
        </div>)
    }
}

const mapStateToProps = (state) => ({
    formFields: state.values.formFields,
    submitButton: state.values.submitButton,
    formContainerStyles: state.container,
    formCheckboxContainerStyles: state.checkbox,
    formCheckboxElementLabelStyles: state.checkboxElementLabel,
    formStyles: state.form,
    formStylesDiv: state.field,
    formStylesInputSelect: state.inputselect,
    formStylesLabel: state.label,
    formStylesSubmitButton: state.submit,
    formStylesSubmitContainer: state.submitContainer,
    clientId: state.values.creds.clientIdValue,
    secret: state.values.creds.secretValue,
    relationsList: state.values.relationsList,
    selectedStyle:state.values.selectedStyle,
});

const mapDispatchToProps = (dispatch) => ({
    saveFormHtml: (htmlString) => dispatch(saveFormHtml(htmlString))
});

export default connect(mapStateToProps, mapDispatchToProps)(FormFieldGenerator);
