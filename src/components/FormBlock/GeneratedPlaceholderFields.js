import React, {Component} from 'react';
import {connect} from 'react-redux';

class GeneratedPlaceholderFields extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    removeBoxShadow(e) {
        e.target.style.boxShadow = "0px 0px 0px 0px";
        e.target.style.border = "1px solid rgba(183,183,183,1)";
    };

    generateField(field) {
        let isRequired = !!(field.required || (field.userRequired && field.userRequired === 'yes'));
        let fieldType = field && field.fieldType ? field.fieldType : '';
        let gridColumn = '1/-1';
        if (field && field.width && field.width.split('_')[0] === '50%') {
            gridColumn = field.width.split('_')[1] === 'Left' ? '1/2' : '2/-1';
        }
        let { backgroundImage , backgroundPosition, ...inputStyles } = this.props.formStylesInputSelect;
        let formStylesDiv = {...this.props.formStylesDiv, gridColumn };
        let placeholderText = field.label || null;
        let searchFor = field.searchFor || false;
        switch(fieldType) {
            case 'input':
                return (<span key={field.key} style={formStylesDiv}>
                    <input id={`${field.key}-${field.entity}-input-field`}
                           onClick={e => this.removeBoxShadow(e)}
                           required={isRequired}
                           data-required={isRequired}
                           data-key={field.key}
                           data-label={field.label}
                           data-entity={field.entity}
                           data-parent={field.parent || null}
                           data-search={searchFor}
                           data-form="value"
                           placeholder={placeholderText}
                           type={field.type}
                           style={inputStyles}
                    />
                </span>);
            case 'select':
                if (field.choices) {
                    let options = field.choices.map(choice => (
                        <option value={choice.value} key={choice.value}>{choice.label}</option>));
                    return (<span key={field.key} style={formStylesDiv}>
                        <select id={`${field.key}-${field.entity}-select-field`}
                                onClick={e => this.removeBoxShadow(e)}
                                data-required={isRequired}
                                data-key={field.key}
                                data-label={field.label}
                                data-entity={field.entity}
                                data-parent={field.parent || null}
                                data-search={searchFor}
                                data-form="value"
                                style={this.props.formStylesInputSelect}
                        >${options}</select>
                    </span>);
                } else {
                    return (<span key={field.key} style={formStylesDiv}>
                        <input id={`${field.key}-${field.entity}-input-field`}
                               onClick={e => this.removeBoxShadow(e)}
                               required={isRequired}
                               data-required={isRequired}
                               data-key={field.key}
                               data-label={field.label}
                               data-entity={field.entity}
                               data-parent={field.parent || null}
                               data-search={searchFor}
                               data-form="value"
                               placeholder={placeholderText}
                               type={field.type}
                               style={inputStyles}
                        />
                    </span>);
                }
            case 'checkboxes':
                let choices = field.choices || [{label: 'test1', value: 'test1'},{label: 'test2', value: 'test2'}];
                let checkBoxOptions = choices.map((choice,i) => {
                    let value = choice.value ||'';
                    return (<div key={`${choice.label}`} style={this.props.formCheckboxElementContainer}>
                        <input type="checkbox"
                               id={`${choice.label}${choice.value}`}
                               name={choice.label}
                               data-key={field.key}
                               data-label={field.label}
                               data-entity={field.entity}
                               data-parent={field.parent || null}
                               data-search={searchFor}
                               data-form="value"
                               value={value}/>
                    </div>);
                });
                return (<span key={`${field.label}${field.key}checkboxes`}  style={formStylesDiv}>
                    <div style={this.props.formCheckboxContainerStyles}>
                        {checkBoxOptions}
                    </div>
                </span>);
            case 'div':
                return(
                    <div style={{...this.props.formStylesTextBlock, gridColumn}}>{field.label}</div>
                );
            case 'textarea':
                return(<span key={field.key} style={formStylesDiv}>
                    <textarea id={`${field.key}-${field.entity}-textarea-field`}
                              required={isRequired}
                              data-required={isRequired}
                              data-key={field.key}
                              data-label={field.label}
                              data-entity={field.entity}
                              data-parent={field.parent || null}
                              data-search={searchFor}
                              data-form="value"
                              placeholder={placeholderText}
                              style={inputStyles}
                    />
                </span>)
        }
    }


    render() {
        let props = this.props;
        let generatedField = '';
        if (props && props.field) {
            generatedField  = this.generateField(props.field);
        }
        return (<>{generatedField}</>)
    }
}

const mapStateToProps = (state) => ({
    formFields: state.values.formFields,
    formContainerStyles: state.container,
    formCheckboxContainerStyles: state.checkboxContainer,
    formCheckboxElementContainer: state.checkboxElementContainer,
    formCheckboxElementLabelStyles: state.checkboxElementLabel,
    formStyles: state.form,
    formStylesDiv: state.field,
    formStylesInputSelect: state.inputselect,
    formStylesLabel: state.label,
    formStylesSubmitButton: state.submit,
    formStylesSubmitContainer: state.submitContainer,
    formStylesTextBlock: state.textBlock,
    clientId: state.values.creds.clientIdValue,
    secret: state.values.creds.secretValue,
    relationsList: state.values.relationsList,
    selectedStyle:state.values.selectedStyle,
});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(GeneratedPlaceholderFields);
