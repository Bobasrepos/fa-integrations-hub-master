import React, {Component} from 'react';
import {connect} from 'react-redux';
import url from '../../constants';


class FormGeneratedField extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    submitForm(e) {
        let localFuncUrl = `${url}/createEntitiesHook`;
        let production =  `${url}/createEntitiesHook`;
        let relations = this.props && this.props.relationsList ? this.props.relationsList : null;
        e.preventDefault();
        let fieldValueFields = document.querySelectorAll('[data-form="value"]');
        let data = {relations: relations};
        let formIsValid = true;
        fieldValueFields.forEach(field => {
            let {key, entity, search, required} = field.dataset;
            let requiredField = JSON.parse(required);
            let value = field.value;
            if (!requiredField || (requiredField && value !== '')) {
                if (requiredField.type === 'date') {
                    let date = new Date(value);
                    value = date.toISOString();
                }
                if (search === 'yes') {
                    if (!data.search) {
                        data.search = [{key, value, entity}];
                    } else {
                        data.search = [...data.search, {key, value, entity}];
                    }
                }
                if (!data[entity]) {
                    data[entity] = {[key]: value};
                } else {
                    data[entity][key] = value;
                }
            } else {
                if (requiredField && value === '') {
                    field.style.border = "1px solid rgba(255,0,0,0.5)";
                    field.style.boxShadow = "0px 0px 5px 2px rgba(255,0,0,0.3)";
                    formIsValid = false;
                }
            }
        });
        if (formIsValid) {
            let url = localFuncUrl;
            let fetchOptions = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'x-client-id': this.props.clientId,
                    'x-secret': this.props.secret
                },
                body: JSON.stringify(data)
            };
            fetch(url, fetchOptions)
                .then(response => {
                    if (response.ok) {
                        fieldValueFields.forEach(field => {
                            field.value = '';
                        });
                    } else {
                        alert('Error')
                    }
                    return  response.json()
                })
                .then(data => console.log(data))
                .catch(e => console.error(e));
        }
    };

    removeBoxShadow(e) {
        e.target.style.boxShadow = "0px 0px 0px 0px";
        e.target.style.border = "1px solid rgba(183,183,183,1)";
    };

    generateField(field) {
        let isRequired = !!(field.required || (field.userRequired && field.userRequired === 'yes'));
        let fieldType = field && field.fieldType ? field.fieldType : '';
        let gridColumn = '1/-1';
        if (field && field.width && field.width.split('_')[0] === '50%') {
            gridColumn = field.width.split('_')[1] === 'Left' ? '1/2' : '2/-1';
        }
        let { backgroundImage , backgroundPosition, ...inputStyles } = this.props.formStylesInputSelect;
        let formStylesDiv = {...this.props.formStylesDiv, gridColumn };
        let activeStyle = this.props.selectedStyle || 'labels';
        let placeholderText = activeStyle !== 'style1' ? field.label : null;
        let searchFor = field.searchFor || false;
        switch(fieldType) {
            case 'input':
                return (<span key={field.key} style={formStylesDiv}>
                   {activeStyle === 'labels' &&  <label
                       htmlFor={`${field.key}-${field.entity}-input-field`}
                       style={this.props.formStylesLabel}>{field.label}
                   </label>}
                     <input id={`${field.key}-${field.entity}-input-field`}
                            onClick={e => this.removeBoxShadow(e)}
                           required={isRequired}
                           data-required={isRequired}
                           data-key={field.key}
                           data-label={field.label}
                           data-entity={field.entity}
                           data-search={searchFor}
                           data-form="value"
                           placeholder={placeholderText}
                           type={field.type}
                           style={inputStyles}
                    />
                </span>);
            case 'select':
                if (field.choices) {
                    let options = field.choices.map(choice => (
                        <option value={choice.value} key={choice.value}>{choice.label}</option>));
                    return (<span key={field.key} style={formStylesDiv}>
                       {activeStyle === 'labels' && <label
                           htmlFor={`${field.key}-${field.entity}-select-field`}
                           style={this.props.formStylesLabel}>
                           {field.label}</label> }
                        <select id={`${field.key}-${field.entity}-select-field`}
                                onClick={e => this.removeBoxShadow(e)}
                                data-required={isRequired}
                                data-key={field.key}
                                data-label={field.label}
                                data-entity={field.entity}
                                data-search={searchFor}
                                data-form="value"
                                style={this.props.formStylesInputSelect}
                        >${options}</select>
                    </span>);
                } else {
                    return (<span key={field.key} style={formStylesDiv}>
                       {activeStyle === 'labels' && <label
                           htmlFor={`${field.key}-${field.entity}-input-field`}
                           style={this.props.formStylesLabel}>
                           {field.label}</label> }
                        <input id={`${field.key}-${field.entity}-input-field`}
                               onClick={e => this.removeBoxShadow(e)}
                               required={isRequired}
                               data-required={isRequired}
                               data-key={field.key}
                               data-label={field.label}
                               data-entity={field.entity}
                               data-search={searchFor}
                               data-form="value"
                               placeholder={placeholderText}
                               type={field.type}
                               style={inputStyles}
                        />
                    </span>);
                }
            case 'checkboxes':
                let choices = field.choices || [{label: 'test1', value: 'test1'},{label: 'test2', value: 'test2'}];
                let checkBoxOptions = choices.map((choice,i) => {
                    let value = choice.value ||'';
                    return (<div key={`${choice.label}`} style={this.props.formCheckboxElementContainer}>
                         <label style={this.props.formCheckboxElementLabelStyles}
                            htmlFor={`${choice.label}${choice.value}`}>
                            {choice.label}</label>
                        <input type="checkbox"
                               id={`${choice.label}${choice.value}`}
                               name={choice.label}
                               data-key={field.key}
                               data-label={field.label}
                               data-entity={field.entity}
                               data-search={searchFor}
                               value={value}/>
                    </div>);
                });
                return (<span key={`${field.label}${field.key}checkboxes`}  style={formStylesDiv}>
                    { activeStyle === 'labels' && <div style={this.props.formStylesLabel}>{field.label}</div> }
                    <div style={this.props.formCheckboxContainerStyles}
                         data-form="value"
                         data-fieldtype="checkbox"
                         data-required={isRequired}
                         data-key={field.key}
                         data-label={field.label}
                         data-entity={field.entity}
                         data-search={searchFor}
                    >
                        {checkBoxOptions}
                    </div>
                </span>);
            case 'div':
                return(
                      <div style={{...this.props.formStylesTextBlock, gridColumn}}>{field.label}</div>
                  );
            case 'textarea':
                return(<span key={field.key} style={formStylesDiv}>
                    {activeStyle === 'labels' &&
                    <label htmlFor={`${field.key}-${field.entity}-textarea-field`}
                           style={this.props.formStylesLabel}>
                        {field.label}</label>
                    }
                    <textarea id={`${field.key}-${field.entity}-textarea-field`}
                              required={isRequired}
                              data-required={isRequired}
                              data-key={field.key}
                              data-label={field.label}
                              data-entity={field.entity}
                              data-search={searchFor}
                              data-form="value"
                              placeholder={placeholderText}
                              style={inputStyles}
                    />
                </span>)
        }
    }


    render() {
        let props = this.props;
        let generatedField = '';
        if (props && props.field) {
            generatedField  = this.generateField(props.field);
        }
        return (<>{generatedField}</>)
    }
}

const mapStateToProps = (state) => ({
    formFields: state.values.formFields,
    formContainerStyles: state.container,
    formCheckboxContainerStyles: state.checkboxContainer,
    formCheckboxElementContainer: state.checkboxElementContainer,
    formCheckboxElementLabelStyles: state.checkboxElementLabel,
    formStyles: state.form,
    formStylesDiv: state.field,
    formStylesInputSelect: state.inputselect,
    formStylesLabel: state.label,
    formStylesSubmitButton: state.submit,
    formStylesSubmitContainer: state.submitContainer,
    formStylesTextBlock: state.textBlock,
    clientId: state.values.creds.clientIdValue,
    secret: state.values.creds.secretValue,
    relationsList: state.values.relationsList,
    selectedStyle:state.values.selectedStyle,
});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(FormGeneratedField);
