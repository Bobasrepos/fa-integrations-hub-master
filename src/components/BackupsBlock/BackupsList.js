import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import styled from 'styled-components';
import BackupElement from './BackupElement'
import {getAllBackupsList} from "../../actions/actions";
import {getHashedId, getSavedVersionsDocs} from "../../utils";

let ListContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
    flex-direction: column;
    width: 100%;
    height: 100%;
    max-height: 100%;
    min-width: 100%;
    overflow-y: auto;
    font-size: 18px;
    padding: 10px;
    border: 1px solid RGBA(95, 108, 114, .3);
    box-shadow: 0px 0px 10px 2px rgba(0,0,0,0.1);
    border-radius: 0 10px 10px 0;
    margin-bottom: 0;
    color: #3D4C60;
`;



let BackupsList = props => {

    let refreshBackups = async (e) => {
        let clientId = props.creds.clientIdValue;
        if (!props.allBackups) {
           return getSavedVersionsDocs(clientId).then(backups => {
                props.getAllBackupsList(backups);
            })
            .catch(e => console.error(e.message));
        }
    };

    useEffect(() => {
        refreshBackups().then(() => null).catch(e => console.error(e));
    }, []);



    let backupsListRender = [];
    if (props.allBackups) {
        backupsListRender = props.allBackups.map(backup => {
            let id = Object.keys(backup)[0];
            return(<BackupElement key={id} backup={backup} />)
        });
    }


    return (<ListContainer>
            {backupsListRender}
    </ListContainer>)
};

const mapStateToProps = (state) => ({
    creds: state.values.creds,
    allBackups: state.values.allBackups
});

const mapDispatchToProps = (dispatch) => ({
    getAllBackupsList: (backups) => dispatch(getAllBackupsList(backups))
});

export default connect(mapStateToProps, mapDispatchToProps)(BackupsList);
