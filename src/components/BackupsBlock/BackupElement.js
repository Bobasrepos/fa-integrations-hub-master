import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import moment from "moment";
import styled from 'styled-components';
import db from '../../DB/db';
import {getHashedId , deleteDoc, downloadCode, copyToClipboard, getDoc} from "../../utils";
import {
    changeCheckBoxContainerStyle, changeCheckBoxElementContainerStyle, changeCheckBoxElementLabelStyle,
    changeContainerStyle,
    changeFieldStyle,
    changeFormStyle, changeInputSelectStyle,
    changeLabelStyle, changeSubmitContainerStyle, changeSubmitStyle, changeTextBlockStyle,
    loadEntityFields
} from "../../actions/actions";
import { getAllBackupsList, getFieldsFromBackup, removeBackupItem, defaultChanged } from "../../actions/actions";

let BackupContainer = styled.div`
     display: flex;
     justify-content: flex-start;
     align-items: center;
     flex-direction: column;
     width: 100%;
     height: auto;
     padding: 10px;
     font-weight: 400;
     border: 1px solid RGBA(95, 108, 114, .15);
     box-shadow: 0 0 7px 1px rgba(0,0,0,0.05);
     border-radius: 5px;
     margin-bottom: 10px;
`;

let InfoRow = styled.div`
     display: grid;
     justify-content: flex-start;
     align-items: flex-start;
     grid-template-columns: 9fr 4fr;
     width: 100%;
     height: auto;
     gap: 10px;
     font-size: .9em;
`;

let InfoLeftBlock = styled.div`
     display: flex;
     justify-content: flex-start;
     align-items: center;
     flex-direction: column;
     width: 100%;
     height: 100%;
     font-size: .85em;
`;

let InfoRightBlock = styled.div`
     display: flex;
     justify-content: flex-start;
     align-items: center;
     flex-direction: column;
     width: 100%;
     height: 100%;
     font-size: .85em;
`;

let InfoElement = styled.div`
   display: flex;
   justify-content: flex-start;
   align-items: center;
`;

let ActionRow = styled.div`
     display: grid;
     justify-content: center;
     align-items: center;
     grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
     gap: 15px;
     width: 100%;
     height: 40px;
     margin-top: 20px;
`;

let ActionButton = styled.div`
       display: flex;
       justify-content: center;
       align-items: center;
       padding: 5px 0;
       border: 1px solid RGBA(95, 108, 114, .15);
       box-shadow: inset 0px 0px 7px 1px rgba(0,0,0,0.03);
       border-radius: 5px;
       font-size: .75em;
       cursor: pointer;
       &:hover {
         background-color: rgba(8, 167, 243, .02);
       }
`;

let ValueText = styled.span`
        padding-left: 5px;
        font-weight: 700;
        font-size: .9em;
    `
let ValueLink = styled.a`
        padding-left: 5px;
        font-weight: 700;
        font-size: .9em;
    `
let ValueSelect = styled.select`
         display: flex;
         justify-content: center;
         align-items: center;
         width: 30px;
         padding-left: 3px;
         font-size: .8em;
         font-weight: 600;
         margin-left: 4px;
         background-color: white;
         border: 1px solid rgba(0,0,0,.1);
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        background-image: url(data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUiIHdpZHRoPSI3IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0yLjgyOCA0LjM2NEwwIDEuNTM2LjcwNy44MjhsMi44MjkgMi44MjlMNi4zNjQuODI4bC43MDcuNzA4TDMuNTM2IDUuMDd6IiBmaWxsPSIjQkJCRUJGIi8+PC9zdmc+);
        background-position: right 3px center;
        background-repeat: no-repeat;
    `

let BackupElement = props => {
    let [pluginDefault, setPlugin] = useState('No');
    let [iframeDefault, setIframe] = useState('No');

    let clientId = props.creds.clientIdValue;
    let id = getHashedId(clientId);
    let docId = Object.keys(props.backup)[0];
    let backupData = props.backup[docId];
    let date = moment(backupData.timestamp).format('lll');
    let htmlCode = backupData.formHtml;

    let iframeCode = backupData.iframeCode;
    let formFieldsAmount = backupData.formFields ? backupData.formFields.length : 0;


    useEffect(() => {
        let isIframeDefaultStore = backupData.isIframeDefault === 'Yes' ? 'Yes' : 'No';
        let isPlugInDefaultStore = backupData.isPlugInDefault === 'Yes' ? 'Yes' : 'No';
        if (pluginDefault !== isPlugInDefaultStore) {
            setPlugin(isPlugInDefaultStore)
        }
        if (isIframeDefaultStore !== iframeDefault) {
            setIframe(isIframeDefaultStore)
        }
    })

    let updateAllStyles = (newValuesToStorage) => {
        let element = Object.keys(newValuesToStorage)[0];
        switch (element) {
            case 'formStylesInputSelect':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeInputSelectStyle(newValuesToStorage[element]);
            case 'formStyles':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeFormStyle(newValuesToStorage[element]);
            case 'formContainerStyles':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeContainerStyle(newValuesToStorage[element]);
            case 'formStylesSubmitButton':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeSubmitStyle(newValuesToStorage[element]);
            case 'formStylesSubmitContainer':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeSubmitContainerStyle(newValuesToStorage[element]);
            case 'formFieldStyles':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeFieldStyle(newValuesToStorage[element]);
            case 'formStylesLabel':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeLabelStyle(newValuesToStorage[element]);
            case 'formStylesTextBlock':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeTextBlockStyle(newValuesToStorage[element]);
            case 'formStylesCheckboxContainer':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeCheckBoxContainerStyle(newValuesToStorage[element]);
            case 'formStylesCheckboxElementContainer':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeCheckBoxElementContainerStyle(newValuesToStorage[element]);
            case 'formStylesCheckboxElementLabel':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeCheckBoxElementLabelStyle(newValuesToStorage[element]);
            default:
                return;
        }
    };


    let importBackup = () => {
           backupData.stylesBackup.map(styleObj => {
              return updateAllStyles(styleObj);
          });
          props.getFieldsFromBackup(backupData);
    };

    let deleteBackup = () => {
        let docRef = `formBackups/${id}/savedVersions/${docId}`;
        let htmlDocRef = `formHtmlOnly/${docId}`;
        props.removeBackupItem(docId);
        deleteDoc(docRef);
        deleteDoc(htmlDocRef);
    };

    let copyIframeCode = async () => {
        let doc = await getDoc(`formHtmlOnly/${docId}`);
        return copyToClipboard(doc.iframeCode);
    };

    let copyUrl = () => {
       let url =  `https://fa-forms-app.web.app/${docId}`;
        return copyToClipboard(url);
    };

    let onDefaultChange = (e, type) => {
        let dataCopy = {...backupData};
        if (type === 'plugin') {
            dataCopy.isPlugInDefault = 'Yes';
        }
        if (type === 'iframe') {
            dataCopy.isIframeDefault = 'Yes';
        }
        props.defaultChanged({id: docId, data:dataCopy, type});
        let { width, height } = backupData.iframeData;
        let iframe = `<iframe width="${width}" frameBorder="0" height="${height}" src="https://fa-forms-app.web.app/${docId}"></iframe>`
        let docRef = db.doc(`formBackups/${id}/savedVersions/${docId}`);
        let colRef = db.collection(`formBackups/${id}/savedVersions`);
        docRef.get().then(doc => {
        if (doc.exists) {
            if (type === 'plugin') {
                docRef.update({ isPlugInDefault: 'Yes' }).then(r => r).catch(e => console.error(e.message))
                let htmlDocRef = db.doc(`formHtmlOnly/${id}`);
              htmlDocRef.get().then(doc => {
                  if (doc.exists) {
                      htmlDocRef.update({formHtml: backupData.formHtml, iframe , formId:docId }).then(r => r).catch(e => console.error(e.message))
                  } else {
                      htmlDocRef.set({formHtml: backupData.formHtml , iframe, formId:docId}).then(r => r).catch(e => console.error(e.message))
                  }
              }).catch(e => console.error(e.message))
                colRef.get().then(snapShot => {
                    snapShot.forEach(doc => {
                        if (doc.id !== docId) {
                            db.doc(doc.ref.path).update({isPlugInDefault: 'No'}).then(r => r).catch(e => console.error(e.message))
                        }
                    })
                })
            }
            if (type === 'iframe') {
                   docRef.update({ isIframeDefault: 'Yes' }).then(r => r).catch(e => console.error(e.message));
                    let iframeDocRef = db.doc(`formIframeOnly/${id}`);
                        iframeDocRef.get().then(iframeDoc => {
                            if (iframeDoc.exists) {
                                iframeDocRef.update({iframe, formId:docId}).then(r => r).catch(e => console.error(e.message));
                            } else {
                                let timestamp = Date.now();
                                iframeDocRef.set({timestamp,iframe, urlId: id, formId:docId }).then(r => r).catch(e => console.error(e.message));
                            }
                        })
                        colRef.get().then(snapShot => {
                            snapShot.forEach(doc => {
                                if (doc.id !== docId) {
                                    db.doc(doc.ref.path).update({isIframeDefault: 'No'}).then(r => r).catch(e => console.error(e.message))
                                }
                            })
                        });
                }
         }
        });
    };




    return (<BackupContainer>
                <InfoRow>
                    <InfoLeftBlock>
                        <InfoElement>Created At: <ValueText>{date}</ValueText></InfoElement>
                        <InfoElement>Document Id: <ValueText>{docId}</ValueText></InfoElement>
                        <InfoElement>Form Url: <ValueLink
                            href={`https://fa-forms-app.web.app/${docId}`}
                            target="_blank"
                        >{`https://fa-forms-app.web.app/${docId}`}</ValueLink></InfoElement>
                    </InfoLeftBlock>
                    <InfoRightBlock>
                        <InfoElement># of fields: <ValueText>{formFieldsAmount}</ValueText></InfoElement>
                        <InfoElement>Default for Plugin: <ValueSelect value={pluginDefault} onChange={(e) => onDefaultChange(e, 'plugin')}>
                                <option value="No">No</option>
                                <option value="Yes">Yes</option>
                            </ValueSelect>
                        </InfoElement>
                        <InfoElement>Default for Iframe: <ValueSelect value={iframeDefault} onChange={(e) => onDefaultChange(e, 'iframe')}>
                                <option value="No">No</option>
                                <option value="Yes">Yes</option>
                            </ValueSelect>
                        </InfoElement>
                    </InfoRightBlock>
                </InfoRow>
                <ActionRow>
                    <ActionButton onClick={() => importBackup()}>Import</ActionButton>
                    <ActionButton onClick={() => downloadCode(htmlCode, docId)}>Download</ActionButton>
                    <ActionButton onClick={() => copyToClipboard(htmlCode)}>Copy Html</ActionButton>
                    <ActionButton onClick={() => copyIframeCode(iframeCode)}>Copy Iframe</ActionButton>
                    <ActionButton onClick={() => copyUrl()}>Copy Url</ActionButton>
                    <ActionButton onClick={() => deleteBackup()}>Delete</ActionButton>
                </ActionRow>
    </BackupContainer>)
};

const mapStateToProps = (state) => ({
    creds: state.values.creds,
});

const mapDispatchToProps = (dispatch) => ({
    loadEntityFields: (entityInfo) => dispatch(loadEntityFields(entityInfo)),
    getAllBackupsList: (backups) => dispatch(getAllBackupsList(backups)),
    getFieldsFromBackup: (backup) => dispatch(getFieldsFromBackup(backup)),
    removeBackupItem: (backup) => dispatch(removeBackupItem(backup)),
    defaultChanged: (backupInfo) => dispatch(defaultChanged(backupInfo)),
    changeFormStyle: (styleObj) => dispatch(changeFormStyle(styleObj)),
    changeContainerStyle: (styleObj) => dispatch(changeContainerStyle(styleObj)),
    changeLabelStyle: (styleObj) => dispatch(changeLabelStyle(styleObj)),
    changeFieldStyle: (styleObj) => dispatch(changeFieldStyle(styleObj)),
    changeInputSelectStyle: (styleObj) => dispatch(changeInputSelectStyle(styleObj)),
    changeSubmitStyle: (styleObj) => dispatch(changeSubmitStyle(styleObj)),
    changeTextBlockStyle: (styleObj) => dispatch(changeTextBlockStyle(styleObj)),
    changeSubmitContainerStyle: (styleObj) => dispatch(changeSubmitContainerStyle(styleObj)),
    changeCheckBoxContainerStyle: (styleObj) => dispatch(changeCheckBoxContainerStyle(styleObj)),
    changeCheckBoxElementContainerStyle: (styleObj) => dispatch(changeCheckBoxElementContainerStyle(styleObj)),
    changeCheckBoxElementLabelStyle: (styleObj) => dispatch(changeCheckBoxElementLabelStyle(styleObj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BackupElement);
