import React, { Component } from 'react';
import { connect } from 'react-redux';
import FieldsList from './AppsBlock/FieldsList';
import FormInnerHtmlEditor from './FormBlock/FormInnerHtmlEditor';
import FormStylesEditor from './StylesBlock/FormStylesEditor';
import BackupsList from './BackupsBlock/BackupsList';


let MainEditor = props => {

        let dataIsActive = props && props.activeEditor && props.activeEditor === 'data';
        let stylesIsActive = props && props.activeEditor && props.activeEditor === 'styles';
        let codeIsActive = props && props.activeEditor && props.activeEditor === 'code';
        let backupIsActive = props && props.activeEditor && props.activeEditor === 'backups';

        return (
             <div className="data-style-code-container">
                { dataIsActive && <FieldsList /> }
                { stylesIsActive && <FormStylesEditor /> }
                { codeIsActive && <FormInnerHtmlEditor /> }
                { backupIsActive && <BackupsList /> }
            </div>
        )
};

const mapStateToProps = (state) => ({
    activeEditor : state.values.activeEditor,
});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(MainEditor);

