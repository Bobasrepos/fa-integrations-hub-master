import React, { Component } from 'react';
import { connect } from 'react-redux';
import SelectPosition from './SelectPosition';
import SelectTextOptions from './SelectTextOptions';
import FormContainerBackgroundColor from '../ColorPickers/FormContainerBackroungColor';
import FormBackgroundColor from '../ColorPickers/FormBackgroundColor';
import FormFieldBackgroundColor from '../ColorPickers/FormFieldBackgroundColor';
import FormBorderColor from '../ColorPickers/FormBorderColor';
import FormFieldsBorderColor from '../ColorPickers/FormFieldsBorderColor';
import FormSubmitBackgroundColor from '../ColorPickers/FormSubmitBackgroundColor';
import FormLabelColor from '../ColorPickers/FormLabelColor';
import FormFieldsColor from "../ColorPickers/FormFieldsColor";
import FormSubmitColor from "../ColorPickers/FormSubmitColor";
import FormTextBlockColor from "../ColorPickers/FormTextBlockColor";
import FormCheckboxLabelsColor from "../ColorPickers/FormCheckboxLabelsColor";
import Slider from './Slider';
import {
    changeFormStyle,
    changeContainerStyle,
    changeLabelStyle,
    changeFieldStyle,
    changeInputSelectStyle,
    changeSubmitStyle,
    loadSelectedStyle,
    changeTextBlockStyle,
    changeCheckBoxContainerStyle,
    changeCheckBoxElementContainerStyle,
    changeCheckBoxElementLabelStyle
} from "../../actions/actions";
import { uploadFile } from '../../utils';
import {
    formStyles,
    formStylesInputSelect,
    formStylesLabel,
    formStylesSubmitButton
} from "../../reducers/formStylesObjects";
import {storageRef} from "../../DB/db";
import styled from "styled-components";

let backgroundImgName = localStorage.getItem('backgroundImgName') ? localStorage.getItem('backgroundImgName') : 'Choose Image File';
let inputFileStyle = backgroundImgName === 'Choose Image File' ? 'rgb(0,0,0,0.4)' : 'RGBA(61, 76, 96, 1)';

let InputFileContainer = styled.div`
         position: relative;
         display: flex;
         justify-content: center;
         align-items: center;
         width: 100%;
         max-width: 100%;
         min-height: 24px;
         max-height: 24px;
         padding-left: 4px;
         padding-right: 4px;
         line-height: 24px;
         white-space: nowrap;
         min-width: 100%;
        font-size: .7em;
        border: 1px solid rgb(0,0,0,0.3);
        color: ${inputFileStyle};
        z-index: 1;
        border-radius: 2px;
        cursor: pointer;
    `

let InputFileName = styled.span`
         width: 100%;
         min-height: 24px;
         min-width: 100%;
            position: absolute;
            z-index: 1;
         display: flex;
         justify-content: center;
         align-items: center;
         cursor: pointer;
         position: absolute;
    `

let InputFile = styled.input`
         width: 100%;
         min-height: 24px;
         min-width: 100%;
        display: none;
        font-size: .6em;
        position: absolute;
        z-index: 5;
        cursor: pointer;
    `

let ValueSelect = styled.select`
         display: flex;
         justify-content: center;
         align-items: center;
         font-size: .8em;
         padding: 4px;
        background-color: white;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        background-image: url(data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUiIHdpZHRoPSI3IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0yLjgyOCA0LjM2NEwwIDEuNTM2LjcwNy44MjhsMi44MjkgMi44MjlMNi4zNjQuODI4bC43MDcuNzA4TDMuNTM2IDUuMDd6IiBmaWxsPSIjQkJCRUJGIi8+PC9zdmc+);
        background-position: right 10px center;
        background-repeat: no-repeat;
        border-radius: 2px;
        width: 100%;
        color: RGBA(61, 76, 96, 1);
        font-family: "Roboto", "Helvetica", "Arial", sans-serif;
    `

class FormStylesEditor extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    onFontChange(e) {
            let font = e.target.value.split('_')[0];
            let sub = e.target.value.split('_')[1];
             let fontValue = `${font},'Roboto',${sub}`;
            let styleObj = { property: 'fontFamily', value: fontValue };
              this.props.changeFieldStyle(styleObj);
              this.props.changeLabelStyle(styleObj);
              this.props.changeInputSelectStyle(styleObj);
              this.props.changeFormStyle(styleObj);
              this.props.changeContainerStyle(styleObj);
              this.props.changeSubmitStyle(styleObj);
              this.props.changeTextBlockStyle(styleObj);
              this.props.changeCheckBoxContainerStyle(styleObj);
              this.props.changeCheckBoxElementContainerStyle(styleObj);
              this.props.changeCheckBoxElementLabelStyle(styleObj);
          return null;
    }

    onStyleChange(e) {
        this.props.loadSelectedStyle(e.target.dataset.name)
    };

     uploadFile(e) {
         if (e.target &&e.target.files[0]) {
             let file = e.target.files[0];
             let timestamp = new Date().getTime();
             let storageFile = storageRef.ref(`images/${timestamp}${file.name.replace(' ', '')}`);
             let task = storageFile.put(file);
             return task.on("state_changed",
                 function progress(snapshot) {
                     console.log((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                 },
                 function error(err) {
                     alert('Error')
                     console.log("error", err);
                 },
                 (() => {
                     task.snapshot.ref.getMetadata().then(data => {
                         console.log(data);
                     });
                     task.snapshot.ref.getDownloadURL()
                         .then(downloadURL => {
                             let styleObj = {property: 'backgroundImage', value: `url(${downloadURL})`};
                             let newValuesToStorage = {...this.props.formContainerStyles};
                             newValuesToStorage['backgroundImage'] = `url(${downloadURL})`;
                             localStorage.setItem('formContainerStyles', JSON.stringify(newValuesToStorage));
                             let inputContainer = document.querySelector('#hidden-file-input-name');
                             inputContainer.style.color = 'RGBA(61, 76, 96, 1)';
                             let strLength = file.name.length;
                             let fileName = strLength > 23 ? `${file.name.substring(0, 18)}...${file.name.substring(strLength - 3, strLength)}` : file.name;
                             inputContainer.innerText = fileName;
                             localStorage.setItem('backgroundImgName', fileName);
                             this.props.changeContainerStyle(styleObj);
                         });
                 }));
         }
    };



    render() {
        let fontsDropDown=[];
        if (this.props.googleFontList) {
            fontsDropDown = this.props.googleFontList.map(fontObj => {
                return (<option key={fontObj.font}
                                style={{fontFamily: fontObj.font}}
                                value={`${fontObj.font}_${fontObj.subFont}`}
                >
                    {fontObj.font}</option>)
            })
        }
        let page = [];
        let sliderFieldsArray = [];
        let label = [];
        let inputSelect = [];
        let form = [];
        let submit = [];
        let submitContainer = [];
        let textBlock = [];
        let checkboxLabels = [];
        if (this.props && this.props.sliderFieldsArray) {
            this.props.sliderFieldsArray.map((sliderFields,i) => {
                let htmlElement = (<section key={`${sliderFields.element}${i}`}> <Slider sliderFields={sliderFields} /> </section>);
                let options = [];
                let positionHtmlElement;
                if (sliderFields.isTextSelect) {
                    positionHtmlElement = (
                        <div key={`${sliderFields.label}-text-selector`}>
                            <div className="color-picker-label">{sliderFields.label}</div>
                            <SelectTextOptions options={sliderFields.options}
                                                  name={sliderFields.element}
                                                  property={sliderFields.property}
                                                  label={sliderFields.label}
                                                  styles={{...this.props[sliderFields.element]}}
                            />
                        </div>)
                } else {
                    if (sliderFields.element === 'formContainerStyles') {
                        if (sliderFields.isPosition) {
                            options = sliderFields.verticalPositionOptions || sliderFields.horizontalPositionOptions;
                            positionHtmlElement = (
                                <div key={`${sliderFields.label}container-position`}>
                                    <div className="color-picker-label">{sliderFields.label}</div>
                                    <SelectPosition options={options} sliderFields={sliderFields}/>
                                </div>)
                        } else {
                            positionHtmlElement = (<section key={`${sliderFields.element}${i}`}> <Slider sliderFields={sliderFields} /> </section>);
                        }
                    }
                }


                switch (sliderFields.element) {
                    case 'formContainerStyles':
                        page = [...page, positionHtmlElement];
                        break;
                    case 'formStyles':
                        form = [...form, htmlElement];
                        break;
                    case 'formStylesLabel':
                        label = [...label, htmlElement];
                        break;
                    case 'formStylesInputSelect':
                        inputSelect = [...inputSelect, htmlElement];
                        break;
                    case 'formStylesSubmitButton':
                        submit = [...submit, htmlElement];
                        break;
                    case 'formStylesSubmitContainer':
                        submitContainer = [...submitContainer, htmlElement];
                        break;
                    case 'formStylesTextBlock':
                        textBlock = [...textBlock, htmlElement];
                        break;
                    case 'formStylesCheckboxElementLabel':
                        checkboxLabels = [...checkboxLabels, htmlElement];
                        break;
                    default:
                        sliderFieldsArray = [...sliderFieldsArray, htmlElement]
                }
            })
        }
        let backgroundImgName = localStorage.getItem('backgroundImgName') ? localStorage.getItem('backgroundImgName') : 'Choose Image File';

        return (<>
            <div className="form-styles-editor-container">
                <div className="form-styles-editor-section">
                    <span className='section-label'>Block Container</span>
                    {page}
                    <div>
                        <div className="color-picker-label">Container Bg Color</div>
                        <FormContainerBackgroundColor />
                    </div>
                    <div>
                        <div className="color-picker-label">Container Bg Image</div>
                        <InputFileContainer id="input-file-container">
                            <InputFileName id="hidden-file-input-name">{backgroundImgName}</InputFileName>
                            <InputFile type="file" className="hidden-file-input" onChange={e => this.uploadFile(e)}/>
                        </InputFileContainer>
                    </div>
                </div>
                 <div className="form-styles-editor-section">
                     <span className='section-label'>Form Container</span>
                     {form}
                     <div>
                         <div className="color-picker-label">Form Bg Color</div>
                         <FormBackgroundColor />
                     </div>
                     <div>
                         <div className="color-picker-label">Form Border Color</div>
                         <FormBorderColor />
                     </div>
                     <div>
                         <div className="color-picker-label">Font</div>
                         <ValueSelect onChange={e => this.onFontChange(e)}>{fontsDropDown}</ValueSelect>
                     </div>
                 </div>

                <div className="form-styles-editor-section">
                     <span className='section-label'>Field Label</span>
                    {label}
                    <div>
                        <div className="color-picker-label">Form Field Label Color</div>
                        <FormLabelColor />
                    </div>
                </div>

                <div className="form-styles-editor-section">
                    <span className='section-label'>Form Fields</span>
                    {inputSelect}
                    <div>
                        <div className="color-picker-label">Form Field Bg Color</div>
                        <FormFieldBackgroundColor />
                    </div>
                    <div>
                        <div className="color-picker-label">Form Fields Border Color</div>
                        <FormFieldsBorderColor />
                    </div>
                    <div>
                        <div className="color-picker-label">Form Fields Color</div>
                        <FormFieldsColor />
                    </div>
                </div>

                <div className="form-styles-editor-section">
                    <span className='section-label'>Submit Button</span>
                    {submit}
                    {submitContainer}
                    <div>
                        <div className="color-picker-label">Submit Bg Color</div>
                        <FormSubmitBackgroundColor />
                    </div>
                    <div>
                        <div className="color-picker-label">Submit Color</div>
                        <FormSubmitColor />
                    </div>
                </div>

                <div className="form-styles-editor-section">
                    <span className='section-label'>Checkbox Labels</span>
                    {checkboxLabels}
                    <div>
                        <div className="color-picker-label">Checkbox Labels Color</div>
                        <FormCheckboxLabelsColor />
                    </div>
                </div>


                <div className="form-styles-editor-section">
                    <span className='section-label'>Text Block</span>
                    {textBlock}
                    <div>
                        <div className="color-picker-label">Text Block Color</div>
                        <FormTextBlockColor />
                    </div>
                </div>
            </div>
            </>
        )
    }
}

const mapStateToProps = (state) => ({
    selectionList: state.values.selectionList,
    formOuterHtml: state.values.formOuterHtml,
    sliderFieldsArray: state.values.sliderFieldsArray,
    formContainerStyles: state.container,
    formStyles: state.form,
    formStylesDiv: state.field,
    formStylesInputSelect: state.inputselect,
    formStylesLabel: state.label,
    formStylesSubmitButton: state.submit,
    fontsList:state.values.fontsList,
    googleFontList:state.values.googleFontList,
    selectedStyle:state.values.selectedStyle,
});

const mapDispatchToProps = (dispatch) => ({
    changeFormStyle: (styleObj) => dispatch(changeFormStyle(styleObj)),
    changeContainerStyle: (styleObj) => dispatch(changeContainerStyle(styleObj)),
    changeLabelStyle: (styleObj) => dispatch(changeLabelStyle(styleObj)),
    changeFieldStyle: (styleObj) => dispatch(changeFieldStyle(styleObj)),
    changeInputSelectStyle: (styleObj) => dispatch(changeInputSelectStyle(styleObj)),
    changeSubmitStyle: (styleObj) => dispatch(changeSubmitStyle(styleObj)),
    loadSelectedStyle: (styleInfo) => dispatch(loadSelectedStyle(styleInfo)),
    changeTextBlockStyle: (styleObj) => dispatch(changeTextBlockStyle(styleObj)),
    changeCheckBoxContainerStyle: (styleObj) => dispatch(changeCheckBoxContainerStyle(styleObj)),
    changeCheckBoxElementContainerStyle: (styleObj) => dispatch(changeCheckBoxElementContainerStyle(styleObj)),
    changeCheckBoxElementLabelStyle: (styleObj) => dispatch(changeCheckBoxElementLabelStyle(styleObj)),
});


export default connect(mapStateToProps, mapDispatchToProps)(FormStylesEditor);
