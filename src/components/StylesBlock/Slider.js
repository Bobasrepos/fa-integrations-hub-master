import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import {
    changeContainerStyle,
    changeFieldStyle,
    changeFormStyle,
    changeInputSelectStyle,
    changeLabelStyle,
    changeSubmitStyle,
    changeTextBlockStyle,
    changeSubmitContainerStyle,
    changeCheckBoxContainerStyle,
    changeCheckBoxElementContainerStyle,
    changeCheckBoxElementLabelStyle
} from "../../actions/actions";



const useStyles = makeStyles({
    root: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    colorPrimary: {
        color: '#3D4C60',
        fontWeight: 600,
        fontSize: '.7em'
    }
});


function SliderComponent(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [componentRendered, setRendered] = React.useState(false);

    let element = props.sliderFields ? props.sliderFields.element : '';
    let property = props.sliderFields ? props.sliderFields.property : '';
    let label = props.sliderFields ? props.sliderFields.label : '';
    let min = props.sliderFields ? props.sliderFields.min : 0;
    let max = props.sliderFields ? props.sliderFields.max : 100;
    let step = props.sliderFields ? props.sliderFields.step : 1;
    let extension = props.sliderFields ? props.sliderFields.extension : '';
    let marks = props.sliderFields ? props.sliderFields.marks : null;
    let storageChanges =  sessionStorage.getItem(`${element}${property}`);
    let defaultValue = storageChanges ? JSON.parse(storageChanges) : props.sliderFields.defaultValue;

          if (!componentRendered) {
              switch (element) {
                  case 'formStylesInputSelect':
                      let fieldFromState = props.formStylesInputSelect[property] ? props.formStylesInputSelect[property] : null;
                      if (fieldFromState || storageChanges) {
                          defaultValue = parseFloat(fieldFromState) !== 'NaN' ? parseFloat(fieldFromState) : defaultValue;
                      }
                      break;
                  case 'formStyles':
                      let formFromState = props.formStyles[property] ? props.formStyles[property] : null;
                      if (formFromState || storageChanges) {
                          defaultValue = parseFloat(formFromState) !== 'NaN' ? parseFloat(formFromState) : defaultValue;
                      }
                      break;
                  case 'formContainerStyles':
                      let containerFromState = props.formContainerStyles[property] ? props.formContainerStyles[property] : null;
                      if (containerFromState || storageChanges) {
                          defaultValue = parseFloat(containerFromState) !== 'NaN' ? parseFloat(containerFromState) : defaultValue;
                      }
                      break;
                  case 'formStylesSubmitContainer':
                      let submitContainerFromState = props.formStylesSubmitContainer[property] ? props.formStylesSubmitContainer[property] : null;
                      if (submitContainerFromState || storageChanges) {
                          defaultValue = parseFloat(submitContainerFromState) !== 'NaN' ? parseFloat(submitContainerFromState) : defaultValue;
                      }
                      break;
                  case 'formStylesSubmitButton':
                      let submitFromState = props.formStylesSubmitButton[property] ? props.formStylesSubmitButton[property] : null;
                      if (submitFromState || storageChanges) {
                          defaultValue = parseFloat(submitFromState) !== 'NaN' ? parseFloat(submitFromState) : defaultValue;
                      }
                      break;
                  case 'formStylesLabel':
                      let labelFromState = props.formStylesLabel[property] ? props.formStylesLabel[property] : null;
                      if (labelFromState) {
                          defaultValue = parseFloat(labelFromState) !== 'NaN' ? parseFloat(labelFromState) : defaultValue;
                      }
                      break;
                  case 'formStylesTextBlock':
                      let textBlockFromState = props.formStylesTextBlock[property] ? props.formStylesTextBlock[property] : null;
                      if (textBlockFromState) {
                          defaultValue = parseFloat(textBlockFromState) !== 'NaN' ? parseFloat(textBlockFromState) : defaultValue;
                      }
                      break;
                  default:
                      console.log(defaultValue);
              }
          }
            if (!defaultValue) {
                defaultValue = props.sliderFields ? props.sliderFields.defaultValue : 0;
            }

            useEffect(() => {
                setRendered(true)
            }, []);

    let valuetext = (value) =>  {
        return `${value}${extension}`;
    };

    let valueChanged = (event, newValue) => {
        if (newValue !== value) {
            let valueWithPx = `${newValue}${extension}`;
            setValue(newValue);
            let newValuesToStorage = {};
            let styleObj = { element , property , value: valueWithPx };
            switch (element) {
                case 'formStylesInputSelect':
                    newValuesToStorage = { ...props.formStylesInputSelect };
                    newValuesToStorage[property] = valueWithPx;
                    localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage));
                    return props.changeInputSelectStyle(styleObj);
                case 'formStyles':
                    newValuesToStorage = { ...props.formStyles };
                    newValuesToStorage[property] = valueWithPx;
                    localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage));
                    return props.changeFormStyle(styleObj);
                case 'formContainerStyles':
                    newValuesToStorage = { ...props.formContainerStyles };
                    newValuesToStorage[property] = valueWithPx;
                    localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage));
                    return props.changeContainerStyle(styleObj);
                case 'formStylesSubmitButton':
                    newValuesToStorage = { ...props.formStylesSubmitButton };
                    newValuesToStorage[property] = valueWithPx;
                    localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage));
                    return props.changeSubmitStyle(styleObj);
                case 'formStylesSubmitContainer':
                    newValuesToStorage = { ...props.formStylesSubmitContainer };
                    newValuesToStorage[property] = valueWithPx;
                    localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage));
                    return props.changeSubmitContainerStyle(styleObj);
                case 'formFieldStyles':
                    newValuesToStorage = { ...props.formStylesInputSelect };
                    newValuesToStorage[property] = valueWithPx;
                    localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage));
                    return props.changeFieldStyle(styleObj);
                case 'formStylesLabel':
                    newValuesToStorage = { ...props.formStylesLabel };
                    newValuesToStorage[property] = valueWithPx;
                    localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage));
                    return props.changeLabelStyle(styleObj);
                case 'formStylesTextBlock':
                    newValuesToStorage = { ...props.formStylesTextBlock };
                    newValuesToStorage[property] = valueWithPx;
                    localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage));
                    return props.changeTextBlockStyle(styleObj);
                case 'formStylesCheckboxContainer':
                    newValuesToStorage = { ...props.formStylesCheckboxContainer };
                    newValuesToStorage[property] = valueWithPx;
                    localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage));
                    return props.changeCheckBoxContainerStyle(styleObj);
                case 'formStylesCheckboxElementContainer':
                    newValuesToStorage = { ...props.formStylesCheckboxElementContainer };
                    newValuesToStorage[property] = valueWithPx;
                    localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage));
                    return props.changeCheckBoxElementContainerStyle(styleObj);
                case 'formStylesCheckboxElementLabel':
                    newValuesToStorage = { ...props.formStylesCheckboxElementLabel };
                    newValuesToStorage[property] = valueWithPx;
                    localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage));
                    return props.changeCheckBoxElementLabelStyle(styleObj);
                default:
                     return;
            }
        }
    };



    return (
        <div className={classes.root} color="primary">
            <Typography className={classes.colorPrimary} id="discrete-slider" gutterBottom>
                {label}
            </Typography>
            <Slider className={classes.colorPrimary}
                defaultValue={defaultValue}
                getAriaValueText={valuetext}
                aria-labelledby="discrete-slider"
                valueLabelDisplay="auto"
                step={step}
                marks={marks}
                min={min}
                max={max}
                onChange={valueChanged}
                color="primary"
            />
        </div>
    );
}

const mapStateToProps = (state) => ({
    formContainerStyles: state.container,
    formStyles: state.form,
    formStylesInputSelect: state.inputselect,
    formStylesLabel: state.label,
    formStylesSubmitButton: state.submit,
    formStylesSubmitContainer: state.submitContainer,
    formStylesTextBlock: state.textBlock,
    formStylesCheckboxContainer: state.checkboxContainer,
    formStylesCheckboxElementContainer: state.checkboxElementContainer,
    formStylesCheckboxElementLabel: state.checkboxElementLabel,
    fontsList:state.values.fontsList
});

const mapDispatchToProps = (dispatch) => ({
    changeFormStyle: (styleObj) => dispatch(changeFormStyle(styleObj)),
    changeContainerStyle: (styleObj) => dispatch(changeContainerStyle(styleObj)),
    changeLabelStyle: (styleObj) => dispatch(changeLabelStyle(styleObj)),
    changeFieldStyle: (styleObj) => dispatch(changeFieldStyle(styleObj)),
    changeInputSelectStyle: (styleObj) => dispatch(changeInputSelectStyle(styleObj)),
    changeSubmitStyle: (styleObj) => dispatch(changeSubmitStyle(styleObj)),
    changeTextBlockStyle: (styleObj) => dispatch(changeTextBlockStyle(styleObj)),
    changeSubmitContainerStyle: (styleObj) => dispatch(changeSubmitContainerStyle(styleObj)),
    changeCheckBoxContainerStyle: (styleObj) => dispatch(changeCheckBoxContainerStyle(styleObj)),
    changeCheckBoxElementContainerStyle: (styleObj) => dispatch(changeCheckBoxElementContainerStyle(styleObj)),
    changeCheckBoxElementLabelStyle: (styleObj) => dispatch(changeCheckBoxElementLabelStyle(styleObj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SliderComponent);

