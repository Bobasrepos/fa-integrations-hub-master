import React from "react";
import {connect} from "react-redux";
import styled from 'styled-components';
import {
    changeContainerStyle,
} from "../../actions/actions";

let ValueSelect = styled.select`
         display: flex;
         justify-content: center;
         align-items: center;
         font-size: .8em;
         padding: 4px 0;
        background-color: white;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        background-image: url(data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUiIHdpZHRoPSI3IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0yLjgyOCA0LjM2NEwwIDEuNTM2LjcwNy44MjhsMi44MjkgMi44MjlMNi4zNjQuODI4bC43MDcuNzA4TDMuNTM2IDUuMDd6IiBmaWxsPSIjQkJCRUJGIi8+PC9zdmc+);
        background-position: right 10px center;
        background-repeat: no-repeat;
        border-radius: 2px;
        width: 100%;
        color: RGBA(61, 76, 96, 1);
        font-family: "Roboto", "Helvetica", "Arial", sans-serif;
    `


let SelectTextOptions = props => {

    let onPositionChange = (e) => {
        let styleObj = { property: props.property, value: e.target.value };
        let newValuesToStorage = { ...props.styles };
        newValuesToStorage[props.property] = e.target.value;
        localStorage.setItem(props.name, JSON.stringify(newValuesToStorage));
        props.changeContainerStyle(styleObj);
    };

    let positionOptions = [];
     if (props.options)
        positionOptions = props.options.map(option => {
            return (<option value={option.value}>{option.label}</option>)
        })


    return (<ValueSelect value={props.styles[props.property]} onChange={e => onPositionChange(e)} className='font-selector'>
        {positionOptions}
    </ValueSelect>)
};


const mapStateToProps = (state) => ({
    formContainerStyles: state.container,
});

const mapDispatchToProps = (dispatch) => ({
    changeContainerStyle: (styleObj) => dispatch(changeContainerStyle(styleObj)),
});


export default connect(mapStateToProps, mapDispatchToProps)(SelectTextOptions);
