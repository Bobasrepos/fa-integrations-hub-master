import React from "react";
import {connect} from "react-redux";
import styled from 'styled-components';
import { OptionsContainer, Option } from './styles';
import { loadEntityFields, loadSettingsRelations, loadFields } from "../../actions/actions";
import {getFaFields} from "../../utils";


let AppsOptions = props => {

    let loadEntityFields = (e) => {
        e.preventDefault();
        let text = e.currentTarget.dataset.text;
        let index = e.currentTarget.dataset.index;
        props.loadEntityFields({text, index});
    };

    let loadSettingsRelationsClicked = (e) => {
        e.preventDefault();
        props.loadSettingsRelations(e.target.dataset.name);
    };

    let refreshData = (e) => {
        let clientId = props.creds.clientIdValue;
        let clientSecret = props.creds.secretValue;
        getFaFields(clientId, clientSecret).then(res => {
                if (res.status === 200) {
                    props.getFieldsFromFa(res.data);
                } else {
                    console.log('We couldn\'t get the fields list from FA');
                }
            })
            .catch(e => console.error(e))
    };


    let dataIsActive = props && props.activeEditor && props.activeEditor === 'data';

    let entitiesMenu = [];
    if (props.entitiesMenu) {
        entitiesMenu = props.entitiesMenu.map(entity => (<Option
            data-index={entity.index}
            data-text={entity.text}
            key={entity.index}
            onClick={e => loadEntityFields(e)}
            style={props.activeData && dataIsActive && entity.text === props.activeData ? {color: 'white', fontWeight: 700} : null}
        >{entity.text}</Option>))
    }

    return(<OptionsContainer>
                 {entitiesMenu}
                <Option
                    key='entityRelations'
                    onClick={e => loadSettingsRelationsClicked(e)}
                    data-name="relations"
                    style={props.activeData && dataIsActive && props.activeData === "relations"  ? {color: 'white', fontWeight: 700} : null}
                >Relations</Option>
                <Option
                    key='entityEditing'
                    onClick={e => loadSettingsRelationsClicked(e)}
                    data-name="editing"
                    style={props.activeData && dataIsActive && props.activeData === "editing" ? {color: 'white', fontWeight: 700} : null}
                >Settings</Option>
               <Option onClick={e => refreshData(e)}>Refresh</Option>
    </OptionsContainer>)
};

const mapStateToProps = (state) => ({
    entitiesMenu: state.values.entitiesMenu,
    activeData: state.values.activeData,
    activeEditor: state.values.activeEditor,
    creds: state.values.creds,
});

const mapDispatchToProps = (dispatch) => ({
    loadEntityFields: (entityInfo) => dispatch(loadEntityFields(entityInfo)),
    loadSettingsRelations: (data) => dispatch(loadSettingsRelations(data)),
    getFieldsFromFa: (fields) => dispatch(loadFields(fields))

});

export default connect(mapStateToProps, mapDispatchToProps)(AppsOptions);
