import React from 'react';
import styled from 'styled-components';
import AppsOptions from './AppsOptions';
import StylesOptions from './StylesOptions';
import ExportOptions from './ExportOptions';
import {
    editorChange,
    loadFields,
    loadEntityFields,
    loadSettingsRelations,
    toggleEdit
} from "../../actions/actions";
import {connect} from "react-redux";

let MenuContainer = styled.div`
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            height: 100%;
            width: 100%;
            background-color: RGB(63, 76, 95);
            border-radius:10px 0  0 10px;
            color: white;
`;

let MenuElementContainer = styled.div`
                display: flex;
                justify-content: center;
                align-items: flex-start;
                flex-direction: column;
                width: 70%;
                height: auto;
                cursor: pointer;
                position: relative;
                margin:15px 0;
`;

let MenuElementName = styled.div`
                    display: flex;
                    width: 100%;
                    height: auto;
                    justify-content: flex-start;
                    align-items: center;
                    color: RGBA(255, 255, 255,.6);
                    font-weight: 400;
                    line-height: 2em;
                    &:hover {
                         color: white;
                    }
`;

let MenuNameActive = styled.div`
                    display: flex;
                    width: 100%;
                    height: auto;
                    justify-content: flex-start;
                    align-items: center;
                    color: white;
                    font-weight: 700;
                    line-height: 2em;
`;




let Menu = props => {

    let handleMenuClink = (optionClicked) => {
        props.editorChange(optionClicked);
        sessionStorage.setItem('activeEditor', optionClicked);
    };


    let dataIsActive = props && props.activeEditor && props.activeEditor === 'data';
    let stylesIsActive = props && props.activeEditor && props.activeEditor === 'styles';
    let codeIsActive = props && props.activeEditor && props.activeEditor === 'code';
    let menuActiveHtml = (<MenuNameActive onClick={() => handleMenuClink('data')}>APPS</MenuNameActive>);
    let menuHtml = (<MenuElementName onClick={() => handleMenuClink('data')}>APPS</MenuElementName>);
    let appsName= dataIsActive ? menuActiveHtml : menuHtml;

    let stylesNameActive = (<MenuNameActive  onClick={() => handleMenuClink('styles')}>STYLES</MenuNameActive>);
    let stylesNameHtml = (<MenuElementName  onClick={() => handleMenuClink('styles')}>STYLES</MenuElementName>);

    let stylesName = stylesIsActive ? stylesNameActive : stylesNameHtml;

    let exportNameActive = (<MenuNameActive  onClick={() => handleMenuClink('code')}>EXPORT</MenuNameActive>);
    let exportNameHtml = (<MenuElementName  onClick={() => handleMenuClink('code')}>EXPORT</MenuElementName>);

    let exportName = codeIsActive ? exportNameActive: exportNameHtml;

    return(
        <MenuContainer>
            <MenuElementContainer>
                {appsName}
                <AppsOptions />
            </MenuElementContainer>
            <MenuElementContainer>
                {stylesName}
                <StylesOptions />
            </MenuElementContainer>
            <MenuElementContainer>
                {exportName}
                <ExportOptions />
            </MenuElementContainer>
        </MenuContainer>
    )
};


const mapStateToProps = (state) => ({
    fieldsFromFa: state.values.fieldsFromFa,
    editedFields: state.values.editedFields,
    selectionList: state.values.selectionList,
    activeEditor : state.values.activeEditor,
    formOuterHtml: state.values.formOuterHtml,
    formFields: state.values.formFields,
    entitiesMenu: state.values.entitiesMenu,
    activeData: state.values.activeData,
    uid: state.values.user.uid,
    creds: state.values.creds,
    formContainerStyles: state.container,
    formStyles: state.form,
    formStylesInputSelect: state.inputselect,
    formStylesLabel: state.label,
    formStylesSubmitButton: state.submit,
    formStylesSubmitContainer: state.submitContainer,
    formStylesTextBlock: state.textBlock,
    formStylesCheckboxContainer: state.checkboxContainer,
    formStylesCheckboxElementContainer: state.checkboxElementContainer,
    formStylesCheckboxElementLabel: state.checkboxElementLabel,
});

const mapDispatchToProps = (dispatch) => ({
    editorChange: (name) => dispatch(editorChange(name)),
    toggleEdit: () => dispatch(toggleEdit()),
    getFieldsFromFa: (fields) => dispatch(loadFields(fields)),
    loadEntityFields: (entityInfo) => dispatch(loadEntityFields(entityInfo)),
    loadSettingsRelations: (data) => dispatch(loadSettingsRelations(data))

});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
