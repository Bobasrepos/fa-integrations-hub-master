import React from "react";
import {connect} from "react-redux";
import JSZip from 'jszip';
import styled from 'styled-components';
import { OptionsContainer, Option } from './styles';
import {
    editorChange,
    loadFields,
    loadEntityFields,
    loadSettingsRelations,
    toggleEdit,
    addBackupItem
} from "../../actions/actions";
import {getHashedId, getDoc, downloadCode, copyToClipboard, saveAs, exportToCsv} from "../../utils";
import db from "../../DB/db";


let ExportOptions = props => {

    let onIframeCopy = () => {
        let clientId = props.creds.clientIdValue;
        let id = getHashedId(clientId);
        let formHtml = props.formOuterHtml ? props.formOuterHtml : '';
        let htmlDocRef = db.collection("formHtmlOnly").doc(id);
        htmlDocRef.get().then(async function(doc) {
            if (doc.exists) {
                await htmlDocRef.update({ formHtml,urlId:id });
            } else {
                await htmlDocRef.set({ formHtml,  urlId:id } );
            }
        }).catch(function(error) {
            console.log("Error setting the document:", error);
        });
        let srcUrl = `https://fa-forms-app.web.app/if/${id}`;

        let formContainer = document.querySelector('.generated-form-container');
        let { height } = window.getComputedStyle(formContainer);
        let { maxWidth } = props.formStyles;
        let heightNum = Math.round(parseFloat(height));
        height = heightNum < 1000 ? `${heightNum}px` : '1000px';
        let iframeCode = `<iframe width="${maxWidth}" height="${height}" src="${srcUrl}"></iframe>`;
        copyToClipboard(iframeCode);
    };

    let onSaveClick = async (type) => {
        let formHtml = props.formOuterHtml ? props.formOuterHtml : '';
        let editedFields = props.editedFields ? props.editedFields : null;
        let formFields = props.formFields ? props.formFields : null;
        let selectionList = props.selectionList ? props.selectionList : null;
        let stylesBackup = [
            {formContainerStyles: props.formContainerStyles},
            {formStyles: props.formStyles},
            {formStylesInputSelect: props.formStylesInputSelect},
            {formStylesLabel: props.formStylesLabel},
            {formStylesSubmitButton: props.formStylesSubmitButton},
            {formStylesSubmitContainer: props.formStylesSubmitContainer},
            {formStylesTextBlock: props.formStylesTextBlock},
            {formStylesCheckboxContainer: props.formStylesCheckboxContainer},
            {formStylesCheckboxElementContainer: props.formStylesCheckboxElementContainer},
            {formStylesCheckboxElementLabel: props.formStylesCheckboxElementLabel}
        ];

        let clientId = props.creds.clientIdValue;
        let creds = props.creds;
        let id = getHashedId(clientId);
        let timestamp = Date.now();

        let formContainer = document.querySelector('.generated-form-container');
        let { height } = window.getComputedStyle(formContainer);
        let { maxWidth } = props.formStyles;
        let heightNum = Math.round(parseFloat(height));
        height = heightNum < 1000 ? `${heightNum}px` : '1000px';
        let isPlugInDefault = false;
        let defaultRef = db.doc(`defaultPluginHtml/${id}`);
        await defaultRef.get().then(doc => {
            if (doc.exists) {
                return doc;
            } else {
                isPlugInDefault = true;
                return defaultRef.set({formHtml,urlId:id}).then(r => r).catch(e => console.error(e));
            }
        }).catch(e => console.error(e));
        let backupData = {
            formHtml, urlId:id , editedFields, formFields, selectionList, stylesBackup, creds,timestamp,isPlugInDefault, iframeData: {height,width:maxWidth }
        };
         if (type === 'new') {
             let docRef = db.collection("formBackups").doc(id).collection('savedVersions');
             let docData = await docRef.add({ ...backupData });
             let data = await getDoc(docData.path);
             props.addBackupItem({[docData.id]: data});
             let srcUrl = `https://fa-forms-app.web.app/${docData.id}`;
             let iframeCode = `<iframe width="${maxWidth}" frameBorder="0" height="${height}" src="${srcUrl}"></iframe>`;
             let htmlDocRef = db.collection("formHtmlOnly").doc(docData.id);
             htmlDocRef.get().then(async function(doc) {
                 if (doc.exists) {
                     await htmlDocRef.update({ formHtml,urlId:id,timestamp, iframeCode, iframeSrc: srcUrl  });
                 } else {
                     await htmlDocRef.set({ formHtml,urlId:id,timestamp, iframeCode, iframeSrc: srcUrl } );
                 }
             }).catch(function(error) {
                 console.log("Error setting the document:", error);
             });
         }
         if (type === 'draft') {
             let docRef = db.collection("formBackups").doc(id);
             docRef.get().then(async function(doc) {
                 if (doc.exists) {
                     await docRef.update({ ...backupData });
                 } else {
                     await docRef.set({ ...backupData } );
                 }
             }).catch(function(error) {
                 console.log("Error setting the document:", error);
             });
         }

    };

    let loadBackups = () => {
       props.editorChange('backups');
    };

    let pageHtml = (html) => {
               return `<!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <title>Form</title>
                </head>
                <body style="padding: 0; margin: 0; width: 100vw; height: 100vh; box-sizing: border-box">
                    ${html}
                </body>
                </html>`;
    };

    let downloadSite = () => {
        let zip = new JSZip();
        let indexHtml = pageHtml(props.formOuterHtml);
        zip.file('index.html', indexHtml);
        if (props.allBackups && props.allBackups.length > 0) {
            props.allBackups.map(backup => {
                let docId = Object.keys(backup)[0];
                let finalHtml = pageHtml(backup[docId].formHtml);
                return zip.file(`${docId}.html`, finalHtml);
            });
        }

        zip.generateAsync({type:"blob"}).then(function (blob) { // 1) generate the zip file
                saveAs(blob, "getforge.zip");                          // 2) trigger the download
            }, function (err) {
               alert('Error')
            });
    };

    let downloadCsv = () => {
        let formField = props.formFields;
        let csvRows = [];
        formField.map(field => {
            let rowArr = [];
            rowArr.push(field.entity);
            rowArr.push(field.label);
            rowArr.push(field.key);
            rowArr.push(field.fieldType);
            rowArr.push(field.type);
            if (field.choices) {
                let choicesStr = '';
                field.choices.map(choice => {
                    choicesStr = choicesStr === '' ?  choicesStr : choicesStr + '||';
                     choicesStr = `${choicesStr}${choice.label}--${choice.value}`;
                    return choice;
                });
                rowArr.push(choicesStr);
            }
            csvRows.push(rowArr);
        });
        exportToCsv('formTemplate', csvRows)
    };

    let onGetQueries = () => {
         let formFields = props.formFields;
         console.log(formFields);
    };

    return(<OptionsContainer>
        <Option onClick={e => loadBackups(e)}>Backups List</Option>
        <Option onClick={() => downloadCode(props.formOuterHtml)}>Download Draft</Option>
        <Option onClick={() => downloadSite()}>Download Site</Option>
        <Option onClick={() => downloadCsv()}>Download Csv</Option>
        <Option onClick={() => copyToClipboard(props.formOuterHtml)}>Copy Code</Option>
        <Option onClick={() => onIframeCopy()}>Copy Iframe</Option>
        <Option onClick={() => onSaveClick('new')}>Save as New</Option>
        <Option onClick={() => onSaveClick('draft')}>Save as Draft</Option>
        <Option onClick={() => onGetQueries()}>Get Queries</Option>
    </OptionsContainer>)
};

const mapStateToProps = (state) => ({
    fieldsFromFa: state.values.fieldsFromFa,
    editedFields: state.values.editedFields,
    selectionList: state.values.selectionList,
    activeEditor : state.values.activeEditor,
    formOuterHtml: state.values.formOuterHtml,
    formFields: state.values.formFields,
    entitiesMenu: state.values.entitiesMenu,
    activeData: state.values.activeData,
    uid: state.values.user.uid,
    creds: state.values.creds,
    formContainerStyles: state.container,
    formStyles: state.form,
    formStylesInputSelect: state.inputselect,
    formStylesLabel: state.label,
    formStylesSubmitButton: state.submit,
    formStylesSubmitContainer: state.submitContainer,
    formStylesTextBlock: state.textBlock,
    formStylesCheckboxContainer: state.checkboxContainer,
    formStylesCheckboxElementContainer: state.checkboxElementContainer,
    formStylesCheckboxElementLabel: state.checkboxElementLabel,
    allBackups: state.values.allBackups
});

const mapDispatchToProps = (dispatch) => ({
    editorChange: (name) => dispatch(editorChange(name)),
    toggleEdit: () => dispatch(toggleEdit()),
    getFieldsFromFa: (fields) => dispatch(loadFields(fields)),
    loadEntityFields: (entityInfo) => dispatch(loadEntityFields(entityInfo)),
    loadSettingsRelations: (data) => dispatch(loadSettingsRelations(data)),
    addBackupItem: (backup) => dispatch(addBackupItem(backup)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ExportOptions);
