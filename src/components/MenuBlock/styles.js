import styled from 'styled-components';

let OptionsContainer = styled.div`
        display: flex;
        width: 100%;
        height: 100%;
        justify-content: flex-start;
        align-items: flex-start;
        flex-direction: column;
        padding-left: 15%;    
`;

let Option = styled.div`
        text-transform: capitalize;
        color: RGBA(255, 255, 255,.6);
        font-size: 1em;
        line-height: 1.7em;
        &:hover {
             color: white;
             font-weight: 700;
        }
`;



export { OptionsContainer, Option };
