import React from "react";
import {connect} from "react-redux";
import defaultStyles from '../../reducers/deafultStyles';
import { OptionsContainer, Option } from './styles';
import {
    changeCheckBoxContainerStyle, changeCheckBoxElementContainerStyle, changeCheckBoxElementLabelStyle,
    changeContainerStyle,
    changeFieldStyle,
    changeFormStyle, changeInputSelectStyle,
    changeLabelStyle, changeSubmitContainerStyle, changeSubmitStyle, changeTextBlockStyle,
    loadSelectedStyle
} from "../../actions/actions";

let StylesOptions = props => {

    let onStyleChange = (e) => {
        props.loadSelectedStyle(e.target.dataset.name)
    };

    let updateAllStyles = (newValuesToStorage) => {
        let element = Object.keys(newValuesToStorage)[0];
        switch (element) {
            case 'formStylesInputSelect':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeInputSelectStyle(newValuesToStorage[element]);
            case 'formStyles':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeFormStyle(newValuesToStorage[element]);
            case 'formContainerStyles':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeContainerStyle(newValuesToStorage[element]);
            case 'formStylesSubmitButton':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeSubmitStyle(newValuesToStorage[element]);
            case 'formStylesSubmitContainer':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeSubmitContainerStyle(newValuesToStorage[element]);
            case 'formFieldStyles':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeFieldStyle(newValuesToStorage[element]);
            case 'formStylesLabel':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeLabelStyle(newValuesToStorage[element]);
            case 'formStylesTextBlock':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeTextBlockStyle(newValuesToStorage[element]);
            case 'formStylesCheckboxContainer':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeCheckBoxContainerStyle(newValuesToStorage[element]);
            case 'formStylesCheckboxElementContainer':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeCheckBoxElementContainerStyle(newValuesToStorage[element]);
            case 'formStylesCheckboxElementLabel':
                localStorage.setItem(`${element}`,JSON.stringify(newValuesToStorage[element]));
                return props.changeCheckBoxElementLabelStyle(newValuesToStorage[element]);
            default:
                return;
        }
    };

    let refreshStyles = () => {
        defaultStyles.map(styleObj => {
            return updateAllStyles(styleObj);
        });
    };

    let stylesIsActive = props && props.activeEditor && props.activeEditor === 'styles';


    return(<OptionsContainer>
            <Option
                key='labels'
                onClick={e => onStyleChange(e)}
                data-name="labels"
                style={props.selectedStyle && stylesIsActive && props.selectedStyle === "labels" ? {color: 'white', fontWeight: 700} : null}
            >Labels</Option>
            <Option
                key='placeholders'
                onClick={e => onStyleChange(e)}
                data-name="placeholders"
                style={props.selectedStyle && stylesIsActive && props.selectedStyle === "placeholders" ? {color: 'white', fontWeight: 700} : null}
            >Placeholders</Option>
            <Option
                key='modern'
                onClick={e => onStyleChange(e)}
                data-name="modern"
                style={props.selectedStyle && stylesIsActive && props.selectedStyle === "modern" ? {color: 'white', fontWeight: 700} : null}
            >Modern</Option>
            <Option
                key='refresh'
                onClick={() => refreshStyles()}
                data-name="modern"
            >Refresh</Option>
    </OptionsContainer>)
};

const mapStateToProps = (state) => ({
    selectedStyle:state.values.selectedStyle,
    activeEditor:state.values.activeEditor,
});

const mapDispatchToProps = (dispatch) => ({
    loadSelectedStyle: (styleInfo) => dispatch(loadSelectedStyle(styleInfo)),
    changeFormStyle: (styleObj) => dispatch(changeFormStyle(styleObj)),
    changeContainerStyle: (styleObj) => dispatch(changeContainerStyle(styleObj)),
    changeLabelStyle: (styleObj) => dispatch(changeLabelStyle(styleObj)),
    changeFieldStyle: (styleObj) => dispatch(changeFieldStyle(styleObj)),
    changeInputSelectStyle: (styleObj) => dispatch(changeInputSelectStyle(styleObj)),
    changeSubmitStyle: (styleObj) => dispatch(changeSubmitStyle(styleObj)),
    changeTextBlockStyle: (styleObj) => dispatch(changeTextBlockStyle(styleObj)),
    changeSubmitContainerStyle: (styleObj) => dispatch(changeSubmitContainerStyle(styleObj)),
    changeCheckBoxContainerStyle: (styleObj) => dispatch(changeCheckBoxContainerStyle(styleObj)),
    changeCheckBoxElementContainerStyle: (styleObj) => dispatch(changeCheckBoxElementContainerStyle(styleObj)),
    changeCheckBoxElementLabelStyle: (styleObj) => dispatch(changeCheckBoxElementLabelStyle(styleObj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(StylesOptions);
