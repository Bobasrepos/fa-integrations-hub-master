let firebase = require("firebase/app");
// Add additional services that you want to use
require("firebase/auth");
require("firebase/database");
require("firebase/firestore");
require("firebase/storage");
require("firebase/messaging");
require("firebase/functions");
// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyA1NePM_MsSXHPBDMmQjQBnbxT-JWxRkb8",
    authDomain: "fa-integration-tools-hub.firebaseapp.com",
    databaseURL: "https://fa-integration-tools-hub.firebaseio.com",
    projectId: "fa-integration-tools-hub",
    storageBucket: "fa-integration-tools-hub.appspot.com",
    messagingSenderId: "362797053142",
    appId: "1:362797053142:web:04f983be14ec857e070577",
    measurementId: "G-RV2QPKFCX8"
};

let faIntegrationsToolsApp = firebase.initializeApp(firebaseConfig);

let db = faIntegrationsToolsApp.firestore();
export let storageRef = faIntegrationsToolsApp.storage();


let provider = new firebase.auth.GoogleAuthProvider();
provider.addScope('profile');
provider.addScope('email');


export { firebase, provider };
export default db;


