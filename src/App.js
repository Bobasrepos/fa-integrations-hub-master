import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firebase } from './DB/db';
import { startLoginRedirect  } from './auth';
import AdminPage from './pages/AdminPage';
import CredPage from "./pages/CredPage";
import { userInfo , loadPage } from './actions/actions'
import './App.scss';
import WebFont from 'webfontloader';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      invoices:[]
    };
  }

  componentWillMount() {
    if (!this.props.load) {
      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          let displayName = user.displayName;
          let email = user.email;
          let emailVerified = user.emailVerified;
          let photoURL = user.photoURL;
          let uid = user.uid;
          let phoneNumber = user.phoneNumber;
          let providerData = user.providerData;
          user.getIdToken().then(accessToken => {
            let userDetails = {
              displayName: displayName,
              email: email,
              emailVerified: emailVerified,
              phoneNumber: phoneNumber,
              photoURL: photoURL,
              uid: uid,
              accessToken: accessToken,
              providerData: providerData
            };
            this.props.userInfo(userDetails);
            this.props.loadPage();
            console.log(userDetails);
            let agentName = email.split('@')[0];
            let domain = email.split('@')[1];
            if(domain === 'freeagentsoftware.com') {
              console.log('isAgent')
            } else {
              console.log('isUser')
            }
          }).catch(err => console.error(err));
        } else {
          this.props.startLoginRedirect();
        }
      })
    }
  }

  componentDidMount() {
    WebFont.load({
      google: {
        families: ['Droid Sans', 'Droid Serif', 'Roboto']
      }
    });
  }

  handleInput(e) {
    this.setState({[e.target.name]:  e.target.value});
  }

  render() {
    return (
        <div className="container">
          { (this.props.isLoggedIn && !this.props.creds) && <CredPage /> }
          { (this.props.isLoggedIn && this.props.creds) && <AdminPage /> }
        </div>
    )
  }
}

const mapStateToProps = (state) => ({
  isLoggedIn: state.values.isLoggedIn,
  creds: state.values.creds
});

const mapDispatchToProps = (dispatch) => ({
  startLoginRedirect: () => dispatch(startLoginRedirect()),
  userInfo: (info) => dispatch(userInfo(info)),
  loadPage: () => dispatch(loadPage())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
